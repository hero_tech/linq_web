<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('app_users');
            $table->integer('loan_request_id')->unsigned();
            $table->foreign('loan_request_id')->references('id')->on('loan_requests');
            $table->integer('repayment_cycle_interest_id')->unsigned();
            $table->foreign('repayment_cycle_interest_id')->references('id')->on('repayment_cycle_interests');
            $table->double('interest_amount');
            $table->double('principal_amount');
            $table->boolean('loan_status')->comment("1: active, 2: cleared, 3: defaulted");//4: bad debt
            $table->date('start_at');
            $table->date('due_date');
            $table->date('clearance_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loans');
    }
}
