<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('region_id')->unsigned();
            $table->foreign('region_id')->references('id')->on('regions');
            $table->string('first_name');
            $table->string('second_name')->nullable();
            $table->string('surname');
            $table->string('id_number')->unique();
            $table->string('pin');
            $table->string('phone_number')->unique();
            $table->string('email')->nullable();
            $table->date('date_of_birth');
            $table->string('occupation');
            $table->boolean('status')->default(1)->comment("1: active, 2: blocked,3: flagged");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_users');
    }
}
