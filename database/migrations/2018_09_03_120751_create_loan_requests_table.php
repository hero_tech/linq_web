<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoanRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('app_users')->onDelete('cascade');
            $table->integer('repayment_cycle_interest_id')->unsigned();
            $table->foreign('repayment_cycle_interest_id')->references('id')->on('repayment_cycle_interests');
            $table->double('interest_amount');
            $table->double('principal_amount');
            $table->boolean('loan_request_status')->comment("1: pending, 2: accepted, 3: rejected");
            $table->string('conversation_id')->nullable();
            $table->string('originator_conversation_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_requests');
    }
}
