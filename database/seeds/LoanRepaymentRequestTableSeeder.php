<?php

use App\LoanRepaymentRequest;
use Illuminate\Database\Seeder;

class LoanRepaymentRequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $repayment_request = new LoanRepaymentRequest();
        $repayment_request->loan_id = 1;
        $repayment_request->amount = 600;
        $repayment_request->status = 2;
        $repayment_request->created_at = now();
        $repayment_request->updated_at = now();
        $repayment_request->save();

        $repayment_request1 = new LoanRepaymentRequest();
        $repayment_request1->loan_id = 2;
        $repayment_request1->amount = 800;
        $repayment_request1->status = 2;
        $repayment_request1->created_at = now();
        $repayment_request1->updated_at = now();
        $repayment_request1->save();

        $repayment_request2 = new LoanRepaymentRequest();
        $repayment_request2->loan_id = 1;
        $repayment_request2->amount = 200;
        $repayment_request2->status = 2;
        $repayment_request2->created_at = now();
        $repayment_request2->updated_at = now();
        $repayment_request2->save();
    }
}
