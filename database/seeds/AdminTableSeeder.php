<?php

use App\Admin;
use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // seeds super admin details to the Admins table
        $super_admin = Role::where('name','Super Admin')->first();
        $admin = new Admin();
        $admin->first_name = 'Admin';
        $admin->last_name = 'Admin';
        $admin->email = 'admin@gmail.com';
        $admin->password = bcrypt('password');
        $admin->created_at = now();
        $admin->updated_at = now();
        $admin->save();
//        $admin->roles()->attach($super_admin);
    }
}
