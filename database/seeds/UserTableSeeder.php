<?php

use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $super_admin = Role::where('name','Super Admin')->first();
        $admin = new User();
        $admin->first_name = 'Linq';
        $admin->last_name = 'Mobile';
        $admin->email = 'linq.mobi@gmail.com';
        $admin->password = bcrypt('1ynq$b7u2');
        $admin->created_at = now();
        $admin->updated_at = now();
        $admin->save();
        $admin->roles()->attach($super_admin);
    }
}
