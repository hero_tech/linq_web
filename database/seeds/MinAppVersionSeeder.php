<?php

use App\MinimumVersion;
use Illuminate\Database\Seeder;

class MinAppVersionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $minimumversion = new MinimumVersion();
        $minimumversion->min_version = '1';
        $minimumversion->created_at = now();
        $minimumversion->updated_at = now();
        $minimumversion->save();
    }
}
