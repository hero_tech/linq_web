<?php

use App\Repayment;
use Illuminate\Database\Seeder;

class RepaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $repayment = new Repayment();
        $repayment->loan_id = 1;
        $repayment->mpesa_repayment_id = 1;
        $repayment->amount = 600;
        $repayment->balance = 601;
        $repayment->created_at = now();
        $repayment->updated_at = now();
        $repayment->save();

        $repayment1 = new Repayment();
        $repayment1->loan_id = 2;
        $repayment1->mpesa_repayment_id = 2;
        $repayment1->amount = 800;
        $repayment1->balance = 1500;
        $repayment1->created_at = now();
        $repayment1->updated_at = now();
        $repayment1->save();

        $repayment2 = new Repayment();
        $repayment2->loan_id = 1;
        $repayment2->mpesa_repayment_id = 3;
        $repayment2->amount = 200;
        $repayment2->balance = 401;
        $repayment2->created_at = now();
        $repayment2->updated_at = now();
        $repayment2->save();
//
//        $repayment3 = new Repayment();
//        $repayment3->loan_id = 2;
//        $repayment3->mpesa_repayment_id = 4;
//        $repayment3->amount = 500;
//        $repayment3->created_at = now();
//        $repayment3->updated_at = now();
//        $repayment3->save();
//
//        $repayment4 = new Repayment();
//        $repayment4->loan_id = 2;
//        $repayment4->mpesa_repayment_id = 5;
//        $repayment4->amount = 700;
//        $repayment4->created_at = now();
//        $repayment4->updated_at = now();
//        $repayment4->save();
    }
}
