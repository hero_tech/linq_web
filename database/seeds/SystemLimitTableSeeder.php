<?php

use App\SystemSetLimit;
use Illuminate\Database\Seeder;

class SystemLimitTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $limit = new SystemSetLimit();
        $limit->user_id=1;
        $limit->amount=1000;
        $limit->status = 1;
        $limit->save();

        $limit1 = new SystemSetLimit();
        $limit1->user_id=2;
        $limit1->amount=1000;
        $limit1->status = 1;
        $limit1->save();

        $limit2 = new SystemSetLimit();
        $limit2->user_id=3;
        $limit2->amount=1000;
        $limit2->status = 1;
        $limit2->save();
    }
}
