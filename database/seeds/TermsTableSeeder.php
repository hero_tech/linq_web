<?php

use App\Terms;
use Illuminate\Database\Seeder;

class TermsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $toc = new Terms();
        $toc->terms_conditions = "Add your terms and conditions";
        $toc->save();
    }
}
