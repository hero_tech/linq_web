<?php

use App\Faq;
use Illuminate\Database\Seeder;

class FaqTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $faq = new Faq();
        $faq->question = 'Laravel already makes it easy to perform?';
        $faq->content = 'authentication via traditional login forms, but what about APIs? APIs typically use tokens to authenticate users and do not maintain session state between requests. Laravel makes API authentication a breeze using Laravel Passport, which provides a full OAuth2 server implementation for your Laravel application in a matter of minutes.';
        $faq->created_at = now();
        $faq->updated_at = now();
        $faq->save();

        $faq = new Faq();
        $faq->question = 'Already makes it easy to perform?';
        $faq->content = 'The Passport service provider registers its own database migration directory with the framework, so you should migrate your database after registering the provider. The Passport migrations will create the tables your application needs to store clients and access tokens.';
        $faq->created_at = now();
        $faq->updated_at = now();
        $faq->save();

        $faq = new Faq();
        $faq->question = 'makes it easy to perform?';
        $faq->content = 'APIs typically use tokens to authenticate users and do not maintain session state between requests. Laravel makes API authentication a breeze using Laravel Passport, which provides a full OAuth2 server implementation for your Laravel application in a matter of minutes.';
        $faq->created_at = now();
        $faq->updated_at = now();
        $faq->save();

    }
}
