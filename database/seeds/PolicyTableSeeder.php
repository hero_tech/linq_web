<?php

use App\Policy;
use Illuminate\Database\Seeder;

class PolicyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $policy = new Policy();
        $policy->policy = "Add your policy";
        $policy->save();

    }
}
