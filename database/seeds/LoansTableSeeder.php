<?php

use App\Loan;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class LoansTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $loan = new  Loan();
        $loan->user_id=1;
        $loan->loan_request_id=1;
        $loan->repayment_cycle_interest_id = 1;
        $loan->interest_amount = 158;
        $loan->principal_amount = 1051;
        $loan->loan_status = 1;
        $loan->start_at = Carbon::parse('22-10-2018');
        $loan->due_date = Carbon::parse('22-11-2019');
        $loan->clearance_date = Carbon::parse('22-11-2018');
        $loan->created_at = now();
        $loan->updated_at = now();
        $loan->save();

        $loan1 = new  Loan();
        $loan1->user_id=2;
        $loan1->loan_request_id=2;
        $loan1->repayment_cycle_interest_id = 2;
        $loan1->interest_amount = 300;
        $loan1->principal_amount = 2000;
        $loan1->loan_status = 1;
        $loan1->start_at = Carbon::parse('22-10-2018');
        $loan1->due_date = Carbon::parse('06-11-2019');
        $loan1->clearance_date = Carbon::parse('22-11-2018');
        $loan1->created_at = now();
        $loan1->updated_at = now();
        $loan1->save();

    }
}
