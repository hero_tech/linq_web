<?php

use App\Reminder;
use Illuminate\Database\Seeder;

class ReminderTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $reminder = new Reminder();
        $reminder->loan_id = 1;
        $reminder->message = 'Kindly pay your loan';
        $reminder->created_at = now();
        $reminder->updated_at = now();
        $reminder->save();
    }
}
