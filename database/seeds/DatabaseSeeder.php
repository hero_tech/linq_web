<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        call the seeders
//        $this->call(AdminTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(TermsTableSeeder::class);
        $this->call(PolicyTableSeeder::class);
//        $this->call(CountryTableSeeder::class);
//        $this->call(RegionTableSeeder::class);
//        $this->call(AppUserTableSeeder::class);
//        $this->call(RepaymentCycleInterestSeeder::class);
//        $this->call(LoanRequestTableSeeder::class);
//        $this->call(LoansTableSeeder::class);
//        $this->call(SystemLimitTableSeeder::class);
//        $this->call(FaqTableSeeder::class);
//        $this->call(ReminderTableSeeder::class);
//        $this->call(MpesaRepaymentTableSeeder::class);
//        $this->call(RepaymentTableSeeder::class);
//        $this->call(MinAppVersionSeeder::class);
//        $this->call(LoanLimitSeeder::class);
//        $this->call(UserMetropolScoreTableSeeder::class);
//        $this->call(LoanRepaymentRequestTableSeeder::class);

    }
}
