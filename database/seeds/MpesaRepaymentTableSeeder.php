<?php

use App\AppUser;
use App\MpesaRepaymnt;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MpesaRepaymentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = DB::table('app_users')->join('loans','app_users.id','=','loans.user_id')
            ->select('app_users.*','loans.interest_amount as interest','loans.principal_amount as principal')
            ->where('loans.id',1)
            ->first();

        //
        $mpesa_repayment = new MpesaRepaymnt();
        $mpesa_repayment->amount = 600;
        $mpesa_repayment->identifier = 'C2B_CONFIRMATION';
        $mpesa_repayment->msisdn = $user->phone_number;
        $mpesa_repayment->transaction_id = 'MJT12JNFJDL';
        $mpesa_repayment->first_name = 'Herons';
        $mpesa_repayment->middle_name = 'Odhiambo';
        $mpesa_repayment->last_name = 'Otieno';
        $mpesa_repayment->bill_reference = $user->phone_number;
        $mpesa_repayment->balance = 4000000;
        $mpesa_repayment->time = Carbon::now();
        $mpesa_repayment->save();

        $mpesa_repayment1 = new MpesaRepaymnt();
        $mpesa_repayment1->amount = 800;
        $mpesa_repayment1->identifier = 'C2B_CONFIRMATION';
        $mpesa_repayment1->msisdn = $user->phone_number;
        $mpesa_repayment1->transaction_id = 'MJT12JNFJDL';
        $mpesa_repayment1->first_name = 'Herons';
        $mpesa_repayment1->middle_name = 'Odhiambo';
        $mpesa_repayment1->last_name = 'Otieno';
        $mpesa_repayment1->bill_reference = $user->phone_number;
        $mpesa_repayment1->balance = 4000000;
        $mpesa_repayment1->time = Carbon::now();
        $mpesa_repayment1->save();

        $mpesa_repayment4 = new MpesaRepaymnt();
        $mpesa_repayment4->amount = 200;
        $mpesa_repayment4->identifier = 'C2B_CONFIRMATION';
        $mpesa_repayment4->msisdn = $user->phone_number;
        $mpesa_repayment4->transaction_id = 'MJT12JNFJDL';
        $mpesa_repayment4->first_name = 'George';
        $mpesa_repayment4->middle_name = 'Odhiambo';
        $mpesa_repayment4->last_name = 'Otieno';
        $mpesa_repayment4->bill_reference = $user->phone_number;
        $mpesa_repayment4->balance = 4000000;
        $mpesa_repayment4->time = Carbon::now();
        $mpesa_repayment4->save();
//
//        $mpesa_repayment2 = new MpesaRepaymnt();
//        $mpesa_repayment2->amount = 500;
//        $mpesa_repayment2->identifier = 'C2B_CONFIRMATION';
//        $mpesa_repayment2->msisdn = $user->phone_number;
//        $mpesa_repayment2->transaction_id = 'MJT12JNFJDL';
//        $mpesa_repayment2->first_name = 'George';
//        $mpesa_repayment2->middle_name = 'Odhiambo';
//        $mpesa_repayment2->last_name = 'Otieno';
//        $mpesa_repayment2->bill_reference = $user->phone_number;
//        $mpesa_repayment2->balance = 4000000;
//        $mpesa_repayment2->time = Carbon::now();
//        $mpesa_repayment2->save();
//
//        $mpesa_repayment3 = new MpesaRepaymnt();
//        $mpesa_repayment3->amount = 700;
//        $mpesa_repayment3->identifier = 'C2B_CONFIRMATION';
//        $mpesa_repayment3->msisdn = $user->phone_number;
//        $mpesa_repayment3->transaction_id = 'MJT12JNFJDL';
//        $mpesa_repayment3->first_name = 'George';
//        $mpesa_repayment3->middle_name = 'Odhiambo';
//        $mpesa_repayment3->last_name = 'Otieno';
//        $mpesa_repayment3->bill_reference = $user->phone_number;
//        $mpesa_repayment3->balance = 4000000;
//        $mpesa_repayment3->time = Carbon::now();
//        $mpesa_repayment3->save();

    }
}
