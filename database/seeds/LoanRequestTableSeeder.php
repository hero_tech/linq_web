<?php

use App\LoanRequest;
use Illuminate\Database\Seeder;

class LoanRequestTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $loan_request = new  LoanRequest();
        $loan_request->user_id=1;
        $loan_request->repayment_cycle_interest_id = 1;
        $loan_request->interest_amount = 150;
        $loan_request->principal_amount = 1000;
        $loan_request->loan_request_status = 1;
        $loan_request->conversation_id = 'AG_20181108_000073e7ba4128a504c1';
        $loan_request->originator_conversation_id = '27278-15320144-1';
        $loan_request->created_at = now();
        $loan_request->updated_at = now();
        $loan_request->save();

        $loan_request1 = new  LoanRequest();
        $loan_request1->user_id=2;
        $loan_request1->repayment_cycle_interest_id = 1;
        $loan_request1->interest_amount = 300;
        $loan_request1->principal_amount = 2000;
        $loan_request1->loan_request_status = 1;
        $loan_request1->conversation_id = 'AG_20181108_000073e7ba4128a504c1';
        $loan_request1->originator_conversation_id = '27278-15320144-1';
        $loan_request1->created_at = now();
        $loan_request1->updated_at = now();
        $loan_request1->save();

    }
}
