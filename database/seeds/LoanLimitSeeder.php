<?php

use App\LoanLimit;
use Illuminate\Database\Seeder;

class LoanLimitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $loanLimit = new LoanLimit();
        $loanLimit->maximum_limit = 70000;
        $loanLimit->minimum_limit = 1000;
        $loanLimit->save();
    }
}
