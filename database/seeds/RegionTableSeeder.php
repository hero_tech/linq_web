<?php

use App\Region;
use Illuminate\Database\Seeder;

class RegionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $region = new Region();
        $region->name = 'Nairobi';
        $region->country_id = 1;
        $region->created_at = now();
        $region->updated_at = now();
        $region->save();

        $region = new Region();
        $region->name = 'Kisumu';
        $region->country_id = 1;
        $region->created_at = now();
        $region->updated_at = now();
        $region->save();

        $region = new Region();
        $region->name = 'Kiambu';
        $region->country_id = 1;
        $region->created_at = now();
        $region->updated_at = now();
        $region->save();

        $region = new Region();
        $region->name = 'Kampala';
        $region->country_id = 2;
        $region->created_at = now();
        $region->updated_at = now();
        $region->save();
    }
}
