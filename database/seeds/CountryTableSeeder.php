<?php

use App\Country;
use Illuminate\Database\Seeder;

class CountryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $region = new Country();
        $region->name = 'Kenya';
        $region->created_at = now();
        $region->updated_at = now();
        $region->save();

        $region = new Country();
        $region->name = 'Uganda';
        $region->created_at = now();
        $region->updated_at = now();
        $region->save();
    }
}
