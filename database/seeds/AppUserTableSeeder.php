<?php

use App\AppUser;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AppUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $appUser = new AppUser();
        $appUser->first_name = 'Herons';
        $appUser->second_name = 'Odhiambo';
        $appUser->surname = 'Otieno';
        $appUser->email = 'herons@gmail.com';
        $appUser->id_number = '23262321';
        $appUser->pin = Hash::make('1234');;
        $appUser->region_id= 1;
        $appUser->phone_number = '0708374149';
        $appUser->occupation = 'occupation';
        $appUser->date_of_birth = Carbon::parse('12-12-1932');
        $appUser->status =1;
        $appUser->created_at = now();
        $appUser->updated_at = now();
        $appUser->save();

        $appUser1 = new AppUser();
        $appUser1->first_name = 'George';
        $appUser1->second_name = 'Odhiambo';
        $appUser1->surname = 'Otieno';
        $appUser1->email = 'george@gmail.com';
        $appUser1->id_number = '25889544';
        $appUser1->pin = Hash::make('1234');;
        $appUser1->region_id= 1;
        $appUser1->phone_number = '0731940124';
        $appUser1->occupation = 'occupation';
        $appUser1->date_of_birth = Carbon::parse('12-12-1933');
        $appUser1->status =1;
        $appUser1->created_at = now();
        $appUser1->updated_at = now();
        $appUser1->save();

        $app_user2 = new AppUser();
        $app_user2->first_name = 'Kevin';
        $app_user2->second_name = 'Sindu';
        $app_user2->surname = 'Kale';
        $app_user2->email = 'k.sindu@gmail.com';
        $app_user2->id_number = '770000077';
        $app_user2->pin = Hash::make('1234');;
        $app_user2->region_id= 1;
        $app_user2->phone_number = '0711585352';
        $app_user2->occupation = 'occupation';
        $app_user2->date_of_birth = Carbon::parse('12-12-1936');
        $app_user2->status =1;
        $app_user2->created_at = now();
        $app_user2->updated_at = now();
        $app_user2->save();
    }
}
