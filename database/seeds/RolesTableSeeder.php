<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            'name'=>'Super Admin',
            'created_at'=>now(),
            'updated_at'=>now()
        ]);DB::table('roles')->insert([
            'name'=>'Normal Admin',
            'created_at'=>now(),
            'updated_at'=>now()
        ]);
    }
}
