<?php

use App\RepaymentCycleInterest;
use Illuminate\Database\Seeder;

class RepaymentCycleInterestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $cycle1 = new RepaymentCycleInterest();
        $cycle1->repayment_cycle='30 days';
        $cycle1->interest_rate = 0.15;
        $cycle1->created_at = now();
        $cycle1->updated_at = now();
        $cycle1->save();

        $cycle2 = new RepaymentCycleInterest();
        $cycle2->repayment_cycle='14 days';
        $cycle2->interest_rate = 0.10;
        $cycle2->created_at = now();
        $cycle2->updated_at = now();
        $cycle2->save();
    }
}
