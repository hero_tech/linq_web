<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
})->name('landing');

//Auth
Auth::routes();
Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', [
        'uses' => 'HomeController@index',
        'middleware' => 'roles',
        'roles' => ['Super Admin','Normal Admin']

    ])->name('super_admin_home');

    Route::get('/normal', function () {
        return view('normal');
    });
});

//Admin
Route::get('users/active', 'AdminController@app_users')->name('users');
Route::get('users', 'AdminController@active_users')->name('active_users');
Route::get('admins','AdminController@admins')->name('admins');
Route::get('admin/add','AdminController@add_admin_form')->name('add_admin_form');
Route::get('admin/{admin_id}/deactivate','AdminController@deactivate_admin')->name('deactivate_admin');
Route::get('admin/{admin_id}/activate','AdminController@activate_admin')->name('activate_admin');
Route::post('admin/add','AdminController@create_admin')->name('create_admin');
Route::get('user/{id}/details','AdminController@user_details')->name('user_details');
Route::get('user/{id}/profile','AdminController@user_profile')->name('user_profile');
Route::get('user/{id}/loans','AdminController@user_loans')->name('user_loans');
Route::get('user/{id}/repayments','AdminController@user_repayments')->name('user_repayments');
Route::get('users/blocked','AdminController@blocked_users')->name('blocked_users');
Route::get('users/flagged','AdminController@flagged_users')->name('flagged_users');
Route::get('users/{user_id}/reset/account','AdminController@flagged_user_reset')->name('reset_flagged_users');
Route::get('users/{user_id}/edit/profile','AdminController@edit_user_profile')->name('edit_user_profile');
Route::post('users/{user_id}/update/details','AdminController@flagged_user_update')->name('flagged_user_update');
Route::get('users/{user_id}/block','AdminController@block_user')->name('block_user');
Route::get('users/{user_id}/unblock','AdminController@unblock_user')->name('unblock_user');
Route::get('loan/request/{request_id}/accept','LoanController@accept_request')->name('admin_accept_loan_request');
Route::get('loan/request/{request_id}/reject','LoanController@reject_request')->name('admin_reject_loan_request');
Route::get('loan/requests','LoanController@loan_requests')->name('loan_requests');
Route::get('unverified/users','AdminVerificationController@unverified_users')->name('unverified_users');
Route::get('verify/user/{user_id}','AdminVerificationController@verify_user')->name('verify_user');
Route::get('reject/user/{user_id}','AdminVerificationController@reject_user')->name('reject_user');

//users with bad debt
Route::get('users/bad_debts','AdminController@app_users_with_bad_debt')->name('bad_debt_users');

//pass resets

//Route::get('admin/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
//Route::post('admin/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
//Route::get('admin/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
//Route::post('admin/reset', 'Auth\ResetPasswordController@reset');


//loans
Route::get('loans/all', 'LoanController@index')->name('loans');
Route::get('loans/active', 'LoanController@active_loans')->name('loans_active');
Route::get('loans/defaulted', 'LoanController@defaulted_loans')->name('loans_defaulted');
Route::get('loans/cleared', 'LoanController@cleared_loans')->name('loans_cleared');
Route::get('loan_limits', 'LoanController@loan_limits')->name('loan_limits');
Route::get('admin_set_loan_limits','LoanController@admin_set_limits')->name('admin_set_limits');
Route::get('system_set_loan_limits','LoanController@system_set_limits')->name('system_set_limits');
Route::get('create/loan_limit','LoanController@create_limit')->name('loan_limit_create_form');
Route::post('create/loan_limit','LoanController@store_limit')->name('loan_limit_store');
Route::get('set/user/{id}/limit','LoanController@set_loan_limit_form')->name('set_loan_limit_form');
Route::post('set/user/{id}/limit','LoanController@set_loan_limit')->name('set_loan_limit');
Route::get('loans/payments','LoanController@payments')->name('loan_payments');
Route::get('admin_set/loan_limits/{id}/reset','LoanController@reset_admin_set_limit')->name('loan_limit_reset');
Route::get('admin_set/loan_limits/{id}/activate','LoanController@activate_admin_set_limit')->name('loan_limit_activate');
Route::get('admin/set/limit/{id}/edit/form','LoanController@limit_update_form')->name('limit_update_form');
Route::post('admin/set/limit/{id}/edit/form','LoanController@limit_update')->name('limit_update');
Route::get('all/users','AdminController@select_users_table')->name('select_users_table');

//loan bad debt
Route::get('bad_debt/id/{id}/val/{val}','LoanController@set_or_unset_bad_debt')->name('bad_debt');

//countries
Route::get('countries',"AdminController@countries")->name('countries');
Route::get('create/country/form',"AdminController@create_contry")->name('country_form');
Route::post('create/country',"AdminController@store_country")->name('country_store');
Route::get('country/{country_id}/edit','AdminController@edit_country')->name('country_edit');
Route::post('country/{country_id}/update','AdminController@update_country')->name('country_update');

//regions
Route::get('country/{country_id}/details','AdminController@country_details')->name('country_details');
Route::get('create/region/{id}/form',"AdminController@create_region")->name('region_form');
Route::post('create/region',"AdminController@store_region")->name('region_store');
Route::get('edit/region/{id}/form','AdminController@edit_region')->name('region_edit');
Route::post('update/region/{id}','AdminController@update_region')->name('region_update');





//reminders
Route::get('reminders', 'AdminController@reminders')->name('reminders');

//repayments
Route::get('loan/{id}/repayments','AdminController@repayments')->name('repayments');


//Route::get('/confirmation','MpesaController@confirmation')->name('mpesa_confirmation');
//Route::get('/validation','MpesaController@validation')->name('mpesa_validation');

//Route::get('/admin/login/form',[
//    'uses'=>'AdminAuth/LoginController@showLoginForm'
//])->name('admin_login_form');
//Route::post('/admin/login',['uses'=>'AdminAuth/LoginController@login'])->name('admin_login');
//Route::post('/admin/logout',['uses'=>'AdminAuth/LoginController@logout'])->name('admin_logout');

Route::get('test','AdminController@test')->name('test');


/*
|--------------------------------------------------------------------------
| Faq Routes
|--------------------------------------------------------------------------
*/

Route::prefix('faq')->group(function (){
    Route::get('/list','FaqController@index')->name('faq-all');
    Route::get('/create','FaqController@create')->name('faq-create');
    Route::post('/store','FaqController@store')->name('faq-store');
    Route::get('/edit/{id}','FaqController@edit')->name('faq-edit');
    Route::get('/show/{id}', 'FaqController@show')->name('faq-show');
    Route::post('/update/{id}','FaqController@update')->name('faq-update');
    Route::get('/delete/{id}','FaqController@destroy')->name('faq-destroy');
});

/*
|--------------------------------------------------------------------------
| End Faq Routes
|--------------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------------
| Repayment Cycles Routes
|--------------------------------------------------------------------------
*/

Route::prefix('repaymentcycle')->group(function (){
    Route::get('/list','RepayCycleController@index')->name('repaymentcycles');
    Route::get('/create','RepayCycleController@create')->name('repaymentcycle-create');
    Route::post('/store','RepayCycleController@store')->name('repaymentcycle-store');
    Route::get('/edit/{id}','RepayCycleController@edit')->name('repaymentcycle-edit');
    Route::post('/update/{id}','RepayCycleController@update')->name('repaymentcycle-update');
    Route::get('/delete/{id}','RepayCycleController@destroy')->name('repaymentcycle-destroy');
});

Route::prefix('code')->group(function (){
    Route::get('/list','CodeController@index')->name('code');
    Route::get('/create','CodeController@create')->name('code-create');
    Route::post('/store','CodeController@store')->name('code-store');
});

/*
|--------------------------------------------------------------------------
| End Repayment Cycles Routes
|--------------------------------------------------------------------------
*/

/*
|--------------------------------------------------------------------------
| App Minimum Version Routes
|--------------------------------------------------------------------------
*/

Route::prefix('minimumappversion')->group(function (){
    Route::get('/list','MinimumAppVersionController@index')->name('minimumversions');
    Route::get('/create','MinimumAppVersionController@create')->name('minimumversion-create');
    Route::post('/store','MinimumAppVersionController@store')->name('minimumversion-store');
    Route::get('/edit/{id}','MinimumAppVersionController@edit')->name('minimumversion-edit');
    Route::post('/update/{id}','MinimumAppVersionController@update')->name('minimumversion-update');
    Route::get('/delete/{id}','MinimumAppVersionController@destroy')->name('minimumversion-destroy');
});

/*
|--------------------------------------------------------------------------
| End App Minimum Version Routes
|--------------------------------------------------------------------------
*/
/*
|--------------------------------------------------------------------------
| Region Routes
|--------------------------------------------------------------------------
*/

Route::get('/regions','AdminController@regions')->name('all_regions');

/*
|--------------------------------------------------------------------------
| End Region Routes
|--------------------------------------------------------------------------
*/
/*
|--------------------------------------------------------------------------
| Reports Routes
|--------------------------------------------------------------------------
*/
Route::get('reports','AdminController@reports')->name('reports');

Route::post('send/message','LoanController@contact_message')->name('create_message');

Route::get('enquiries','AdminController@enquiries')->name('enquiries');

Route::get('terms_and_conditions','LoanController@tocs')->name('tocs');
Route::get('update_privacy_policy','LoanController@update_privacy_policy_form')->name('update_privacy_policy_form');
Route::get('update_tac','LoanController@update_tac_form')->name('update_tac_form');
Route::get('view_tac','LoanController@view_tac')->name('view_tac');
Route::post('create_tac','LoanController@create_tac')->name('create_tac');
Route::post('create_policy','LoanController@create_policy')->name('create_policy');
Route::post('update_tac','LoanController@update_tac')->name('update_tac');
Route::get('privacy_policy','LoanController@privacy')->name('privacy');
Route::get('landing_terms','LandingController@view_tac')->name('view_tac');
Route::get('landing_policy','LandingController@view_policy')->name('view_policy');
Route::get('mpesa/verification','MpesaVerificationController@mpesa_verifications')->name('mpesa_verifications');

/**
 * -----------------------------------------------------------------------------------------
 * Metropol Score Reports
 * -----------------------------------------------------------------------------------------
 */

Route::get('metropol/scores','ScoreReportController@index')->name('score_reports');
