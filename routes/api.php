<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('for/minappversion', 'Api\MinAppVersionController@getMinAppVersion');
Route::post('for/get_region', 'Api\RegionController@getRegions');
Route::post('for/get_code', 'Api\RegionController@getReferralCode');
Route::post('for/verificationcode', 'Api\AppUserController@requestVerification');
Route::post('for/verifycode', 'Api\AppUserController@codeVerification');
Route::post('for/login', 'Api\AppUserController@login');
Route::post('for/register', 'Api\AppUserController@register');
Route::post('for/resetPin', 'Api\AppUserController@resetPin');
Route::post('for/requestpinReset', 'Api\AppUserController@requestPinReset');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('for/logout', 'Api\AppUserController@logout');
    Route::post('for/get_faqs', 'Api\FAQController@getFAQs');
    Route::post('for/repay_cycle', 'Api\RepaymentCycleController@getRepaymentCycles');
    Route::post('for/loanlimit','Api\LoanLimitController@getLoanLimit');
    Route::post('for/userloanbalance','Api\LoanBalanceController@getUserLoanBalance');
    Route::post('for/userloanbalance_updated','Api\LoanBalanceController@getUserLoanBalanceUpdated');
    Route::post('for/userloanshistory','Api\LoanHistoryController@getUserLoansHistory');
    Route::get('for/mpesaaccesstoken','Api\MetropolCheckMpesaPaymentController@getToken');
//    Route::post('for/loanrequest','Api\MetropolCheckMpesaPaymentController@b2cAction');
    Route::post('for/appuserupdate', 'Api\AppUserController@updateuserinfo');
    Route::post('for/pendingrequest', 'Api\LoanStatusController@checkPendingLoanRequest');
    Route::post('for/pendingpayment', 'Api\LoanStatusController@checkPendingLoanPayment');
    Route::post('for/appbasicdataonlaunch', 'Api\AppBasicDataOnLaunchController@getBasicData');
    Route::post('for/userloandetail','Api\LoanHistoryController@getUserLoanDetail');
    //Metropol
//    Route::post('metropol/score','Api\MetropolController@clientCrbScore');
//    Route::post('metropol/name','Api\MetropolController@clientCrbName');
    Route::post('for/applyforloan','Api\MetropolController@apply')->name('apply_for_loan');
    Route::post('for/stk_push', 'Api\MpesaController@stkPush');
    Route::post('for/verification/stk_push', 'Api\MpesaVerificationStkPushController@stkPush');
    Route::post('for/loanrequest','Api\MetropolCheckMpesaPaymentController@b2cAction');
});

//Route::post('for/verification/stk_push', 'Api\MpesaVerificationStkPushController@verification_stkPush');
//Route::post('for/verification/stk_push', 'Api\MpesaVerificationStkPushController@stkPush');


Route::post('for/mpesacallbackusrls','Api\MetropolCheckMpesaPaymentController@registerUrls');
Route::post('for/b2ctimeout','Api\MetropolCheckMpesaPaymentController@timeout');
Route::post('for/b2csuccess','Api\MetropolCheckMpesaPaymentController@success');


Route::post('metropol/score','Api\MetropolController@clientCrbScore');
Route::post('metropol/name','Api\MetropolController@clientCrbName');
//Route::post('for/applyforloan','Api\MetropolController@apply')->name('apply_for_loan');

//Route::post('for/stk_push', 'Api\MpesaController@stkPush');
Route::get('for/access_token','Api\MetropolCheckMpesaPaymentController@getToken');

Route::get('token', 'Api\MpesaController@getAccessToken');
Route::get('verification/token', 'Api\MpesaVerifiationController@getVerificationAccessToken');
Route::post('register/urls', 'Api\MpesaController@registerUrls');
Route::post('register/verification/urls', 'Api\MpesaVerifiationController@registerVerificationUrls');
Route::post('validation', 'Api\MpesaController@validation');
Route::post('verification/validation', 'Api\MpesaVerifiationController@verificationValidation');
Route::post('confirmation', 'Api\MpesaController@confirmation');
Route::post('verification/confirmation', 'Api\MpesaVerifiationController@verificationConfirmation');
Route::post('c2b/simulation', 'Api\MpesaController@c2bSimulator');
Route::post('callback','Api\MpesaController@callback')->name('stk_push_callback');
Route::post('verification/callback','Api\MpesaVerificationStkPushController@verification_callback')->name('stk_push_callback');
//Route::post('verification/callback','Api\MpesaVerifiationController@verificationCallback')->name('verification_stk_push_callback');

Route::post('metropol/token','Api\MetropolDataSubmission@getAccessToken')->name('metropol.token');
Route::post('metropol/submit/data','Api\MetropolDataSubmission@submitJSON')->name('metropol.submission');
Route::post('metropol/report/callback','Api\MetropolDataSubmission@callback')->name('metropol.callback');
//Route::post('metropol/test','Api\MetropolDataSubmission@myTest')->name('metropol.test');
