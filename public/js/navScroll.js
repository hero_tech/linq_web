$(window).ready(function () {

    $('header').addClass('load');
    $('nav').addClass('loaded');

    $(window).scroll(function () {
        if ($(window).scrollTop() > 60)
        {
            $('nav').addClass('slide-header');
        }
        else {
            $('nav').removeClass('slide-header');
        }
    });

    $(".menu-icon").on("click", function () {
        $("nav ul").toggleClass("showing");
    });

});

