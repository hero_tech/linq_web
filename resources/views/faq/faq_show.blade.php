@extends('layouts.dash_side_top')

@section('content')

    <div class="row">

        <br>

        <div class="card" style="margin-left: 15px;margin-right: 25px;">
            <div class="card-header">

                {{ $faq->question }}

                <div class="btn-group" style="float: right">
                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                        Actions
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu pull-right" role="menu">
                        <li><a href="{{ route('faq-edit', ['id' => $faq->id]) }}">Edit FAQ</a>
                        </li>
                        <li><a href="{{ route('faq-destroy', ['id' => $faq->id]) }}">Delete FAQ</a>
                        </li>
                    </ul>
                </div>

            </div>
            <div class="card-body">
                <blockquote class="blockquote mb-0">
                    <p>
                        {{ $faq->content }}
                    </p>
                </blockquote>
            </div>
        </div>

    </div>

@endsection
