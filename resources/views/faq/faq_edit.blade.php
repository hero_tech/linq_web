@extends('layouts.dash_side_top')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Edit FAQ</div>
            <form method="POST" action="{{ route('faq-update',['id'=>$faq->id]) }}" style="margin-left: 50px">
                @csrf

                <input type="text" name="question" value="{{ $faq->question }}" placeholder="question" required autofocus>

                <br>

                <textarea id="answer" rows = 5 cols = 24 type="text" name="content" placeholder="answer" required autofocus style="margin-top: 10px">{{ $faq->content }}</textarea>

                <br>

                <button type="submit" class="btn btn-primary">Update</button>

            </form>
        </div>
    </div>

@endsection
