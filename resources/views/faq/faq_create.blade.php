@extends('layouts.dash_side_top')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Add FAQ</div>
            <form method="POST" action="{{ route('faq-store') }}" style="margin-left: 50px">
                @csrf

                <input type="text" name="question" value="{{ old('question') }}" placeholder="question" required autofocus>

                <br>

                <textarea id="answer" rows = 5 cols = 24 type="text" name="content" placeholder="answer" required autofocus style="margin-top: 10px">{{ old('content') }}</textarea>

                <br>

                <button type="submit" class="btn btn-primary">Create</button>

            </form>
        </div>
    </div>

@endsection
