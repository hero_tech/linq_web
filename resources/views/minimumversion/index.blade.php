@extends('layouts.dash_side_top')

@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Minimum App Version</h1>
        </div><!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars add-button">
                <a type="button" class="btn btn-custom" href="{{route('minimumversion-create')}}">Add Minimum Version</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>Minimum Version</th>
                                <th>Edit</th>
                                <th>Delete</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($minimumversions as $minimumversion)
                                <tr>
                                    <td>{{ $minimumversion->min_version }}</td>
                                    <td>
                                        <a href="{{route('minimumversion-edit',['id'=>$minimumversion->id])}}" class="btn btn-custom"><i class="fa fa-edit"></i> </a>
                                    </td>
                                    <td>
                                        <a href="{{route('minimumversion-destroy',['id'=>$minimumversion->id])}}" class="btn btn-custom"><i class="fa fa-trash-o"></i> </a>
                                    </td>
                                    <td>{{ Carbon\Carbon::parse($minimumversion->created_at)->format('d-m-Y') }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable();
        });
    </script>
@endsection
