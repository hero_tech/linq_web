@extends('layouts.dash_side_top')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Add new App Minimum Version</div>
            <form method="POST" action="{{ route('minimumversion-store') }}" style="margin-left: 50px">
                @csrf

                <input type="text" name="min_version" value="{{ old('min_version') }}" placeholder="minimum version" autofocus>

                <br>

                <button type="submit" class="btn btn-primary">Create</button>

            </form>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-4 justify-content-center">
            @if ($errors->any())
                <div class="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="color: #E55025">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

@endsection
