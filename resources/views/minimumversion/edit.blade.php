@extends('layouts.dash_side_top')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Edit App Minimum Version</div>
            <form method="POST" action="{{ route('minimumversion-update',['id'=>$minimumversion->id]) }}" style="margin-left: 50px">
                @csrf

                <input type="text" name="min_version" value="{{ $minimumversion->min_version }}" placeholder="minimum version" required autofocus>

                <br>

                <button type="submit" class="btn btn-primary">Update</button>

            </form>
        </div>
    </div>

@endsection
