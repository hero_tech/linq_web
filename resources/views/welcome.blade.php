@extends('layouts.landing.landing_super')
@section('header')
    @include('layouts.landing.landing_header')
@endsection
@section('content')
    <div class="cover-area">
        <div class="drive-area">
            <div class="row">
                <div class="col-lg-7 col-md-12 col-sm-12">
                    <div class="slogan">
                        <p style="text-transform: uppercase">Focused... Fast... Friendly...</p>
                        <h4>Access loans anywhere right on your phone.</h4>
                        <a class="btn btn-custom-blue" href="https://play.google.com/store/apps/details?id=co.ke.muva.linq" target="_blank">Download LinqMobile App</a>
                    </div>
                </div>
                <div class="col-lg-5 col-md-12 col-sm-12" style="">
                    <div class="image-slide">
                        <img style="float: left" src="{{asset('img/headerarrea.png')}}">
                    </div>

                </div>
            </div>
            <div id="about"></div>
        </div>
    </div>
    <div class="about-section">
        <div class="about-header">
            <i class="fa fa-arrow-circle-right"></i>About LinqMobile
        </div>
        <div class="about-content">

            <p>LINQ MOBILE LTD is a dependable, fast and flexible financial solution provider to many people who need to
                pay bills and attend to emergencies.Our leading lending model relies on your trustworthiness as
                security, a departure from the traditional reliance on tangible security for one to access credit.</p>

            <p>To access credit, download the LINQ MOBILE App from Play Store on your smartphone with a registered
                Safaricom line, follow a simple and short registration process on the app.
                After requesting for a loan, disbursement is done directly to your Safaricom Mpesa account.
                Repayment is easy thanks to the Mpesa STK push service which enables you to repay for your loan directly
                from the LINQ MOBILE App.
                Repayment period spans from 14 to 30 days depending on the repayment plan that you choose.
                Timely repayment of your loans increases your credit rating which will consequently increase your credit
                limit.
                For more information, you can get in touch with LINQ MOBILE team through our social media pages
                (Facebook and Twitter), SMS and Email (info@linq.mobi).</p>
            <div id="screenshots"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="image-slide">
                <div class="procedure">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active">
                            </li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                            <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                            {{--<li data-target="#carouselExampleIndicators" data-slide-to="3"></li>--}}
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <!-- Slide One - Set the background image for this slide in the line below -->
                            <div class="carousel-item item0 active">
                            </div>
                            <!-- Slide Two - Set the background image for this slide in the line below -->
                            <div class="carousel-item item1">
                            </div>
                            <!-- Slide Three - Set the background image for this slide in the line below -->
                            <div class="carousel-item item2">
                            </div>
                            {{--<div class="carousel-item item3">--}}
                            {{--</div>--}}
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                           data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                           data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--<div class="screenshots">--}}
    {{--<div class="row">--}}
    {{--<div class="col-lg-12">--}}

    {{--<div class="application">--}}
    {{--<div class="application-header">--}}
    {{--<i class="fa fa-arrow-circle-right"></i>Application Procedure--}}
    {{--</div>--}}
    {{--<div class="application-content">--}}

    {{--<ol>--}}
    {{--<li>Download the LINQ MOBILE App from Play Store on your smartphone.</li>--}}
    {{--<li>Complete the simple registration process on the App.</li>--}}
    {{--<li>Request for a loan and choose your repayment schedule.</li>--}}
    {{--<li>You will receive an SMS that your loan has been approved, processed and credited on your--}}
    {{--M-pesa account.--}}
    {{--</li>--}}
    {{--</ol>--}}

    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="col-lg-12">--}}
    {{--<img style="float: left" src="{{asset('img/lnding.png')}}">--}}
    {{--<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">--}}
    {{--<ol class="carousel-indicators">--}}
    {{--<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>--}}
    {{--<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>--}}
    {{--<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>--}}
    {{--</ol>--}}
    {{--<div class="carousel-inner" role="listbox">--}}
    {{--<!-- Slide One - Set the background image for this slide in the line below -->--}}
    {{--<div class="carousel-item item0 active">--}}
    {{--</div>--}}
    {{--<!-- Slide Two - Set the background image for this slide in the line below -->--}}
    {{--<div class="carousel-item item1">--}}
    {{--</div>--}}
    {{--<!-- Slide Three - Set the background image for this slide in the line below -->--}}
    {{--<div class="carousel-item item2">--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">--}}
    {{--<span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
    {{--<span class="sr-only">Previous</span>--}}
    {{--</a>--}}
    {{--<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">--}}
    {{--<span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
    {{--<span class="sr-only">Next</span>--}}
    {{--</a>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--</div>--}}

    {{--</div>--}}
    <div class="row">
        <div class="col-lg-6">
            <div class="eligibility">
                <div class="eligibility-header">
                    <i class="fa fa-arrow-circle-right"></i>Eligibility
                </div>
                <div class="eligibility-content">
                    <ul>
                        <li>An android phone.</li>
                        <li>Your National ID</li>
                        <li>A Safaricom M-pesa line</li>
                    </ul>
                    <p>No collateral, guarantors or referees required.<br>No paperwork or credit history required.</p>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="repayment">
                <div class="repayment-header">
                    <i class="fa fa-arrow-circle-right"></i>Repayment
                </div>
                <div class="repayment-content">
                    <p>Repayment is easy thanks to the Mpesa STK push service which enables you to repay for your loan
                        directly from the LINQ MOBILE App.</p>
                    <p>You can also repay your loan using paybill number 515657 and state the account number as your
                        phone number using your phone number or any other.</p>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('footer')
    @include('layouts.landing.landing_footer')
@endsection
@section('scripts')
    <script src="{{ asset('js/navScroll.js') }}" defer></script>
@endsection