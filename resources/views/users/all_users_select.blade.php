@extends('layouts.dash_side_top')
@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Select a user</h1>
        </div>
        <!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive table-customize">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Region</th>
                                <th>Phone</th>
                                <th>Current Loan Limit</th>
                                {{--<th>Profile</th>--}}
                                {{--<th>Loan Status</th>--}}
                                {{--<th>Set Limit</th>--}}
                            </tr>
                            </thead>
                            @foreach($app_users as $app_user)
                                <tr>
                                    <td><a href="{{route('set_loan_limit_form',['id'=>$app_user->id])}}">{{$app_user->first_name}} {{$app_user->second_name}} {{$app_user->surname}}</a></td>
                                    <td>{{$app_user->region}}</td>
                                    <td>{{$app_user->phone_number}}</td>
                                    <td>KSh. {{\App\Http\Traits\UniversalMethods::getLoanLimit($app_user->id)}}</td>
                                    {{--                                    <td><a href="{{route('user_profile',['id'=>$app_user->id])}}">View Profile</a></td>--}}
                                    {{--@if(\App\Http\Traits\UniversalMethods::hasActiveLoan($app_user->id))--}}
                                        {{--<td style="color: #1f6fb2">Active</td>--}}
                                        {{--<td><a href="{{route('set_loan_limit_form',['id'=>$app_user->id])}}" class="btn btn-custom">Set Limit</a> </td>--}}
                                    {{--@else--}}
                                        {{--<td style="color: #E55025">Inactive</td>--}}
                                    {{--@endif--}}
                                </tr>
                            @endforeach
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable();
        });
    </script>
@endsection
