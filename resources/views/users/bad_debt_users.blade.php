@extends('layouts.dash_side_top')
@section('styles')
    <link rel="stylesheet" type="text/css"
          href="{{URL::to('https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css')}}">
@endsection
@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Users</h1>
        </div>
        <!--End Page Header -->
    </div>
    <div class="row offset-1">
        <div class="col-lg-2">
            <div class="in-navs">
                <a href="{{route('unverified_users')}}"  class="btn {{url()->current()==route('unverified_users')? "active": ""}}"><i
                            class="fa fa-thumbs-up"></i>Unverified Users</a>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="in-navs">
                <a href="{{route('users')}}" class="btn {{url()->current()==route('users')? "active": ""}}"><i
                            class="fa fa-thumbs-up"></i>Active Users</a>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="in-navs">
                <a href="{{route('blocked_users')}}" class="btn {{url()->current()==route('blocked_users')? "active": ""}}"><i class="fa fa-thumbs-down"></i>Blocked Users</a>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="in-navs">
                <a href="{{route('flagged_users')}}" class="btn {{url()->current()==route('flagged_users')? "active": ""}}"><i class="fa fa-ban"></i>Flagged Accounts</a>
            </div>
        </div>
        <div class="col-lg-2">
            <div class="in-navs">
                <a href="{{route('bad_debt_users')}}" class="btn {{url()->current()==route('bad_debt_users')? "active": ""}}"><i class="fa fa-ban"></i>Bad Debts</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive table-customize">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Region</th>
                                <th>Phone</th>
                                <th>Loan Limit</th>
                                <th>Loan Status</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            @foreach($app_users as $app_user)
                                <tr>
                                    <td>
                                        <a href="{{route('user_profile',['id'=>$app_user->id])}}">{{$app_user->first_name}} {{$app_user->second_name}} {{$app_user->surname}}</a>
                                    </td>
                                    <td>{{$app_user->region}}</td>
                                    <td>{{$app_user->phone_number}}</td>
                                    <td>KSh. {{\App\Http\Traits\UniversalMethods::getLoanLimit($app_user->id)}}</td>
                                    {{--                                    <td><a href="{{route('user_profile',['id'=>$app_user->id])}}">View Profile</a></td>--}}
                                    @if(\App\Http\Traits\UniversalMethods::hasActiveLoan($app_user->id))
                                        <td style="color: #1f6fb2">Active</td>
                                    @else
                                        @if($app_user->loans->where('loan_status',4)->first() != null)
                                            <td style="color: #E55025">Has a Bad Debt</td>
                                        @else
                                            <td style="color: #012B43">Inactive</td>
                                        @endif
                                    @endif

                                    @if($app_user->loans->where('loan_status',4)->first()->loan_status == 4)
                                        <td><a href="{{ route('bad_debt',['id'=>$app_user->loans->where('loan_status',4)->first()->id,'val'=>1]) }}" class="btn btn-custom">Unset Bad Debt</a> </td>
                                    @elseif($app_user->loans->where('loan_status',4)->first()->loan_status == 2)
                                        <td><a href="{{ route('bad_debt',['id'=>$app_user->loans->where('loan_status',4)->first()->id,'val'=>4]) }}" class="btn btn-custom">Set as Bad Debt</a> </td>
                                    @endif
                                </tr>
                            @endforeach
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {{--    <script type="text/javascript" charset="utf8" src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js')}}}"></script>--}}
    {{--<script type="text/javascript" charset="utf8" src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js')}}"></script>--}}

    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js')}}"></script>

    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ]
            });
        });
    </script>
@endsection
