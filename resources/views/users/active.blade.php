@extends('layouts.dash_side_top')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Active Users</h1>
        </div>
        <!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Region</th>
                                <th>Phone</th>
                                <th>Loan Limit</th>
                                <th>Details</th>
                            </tr>
                            </thead>
                            @foreach($active_users as $app_user)
                                <tr>
                                    <td>{{$app_user->first_name}} {{$app_user->second_name}} {{$app_user->surname}}</td>
                                    <td>{{$app_user->region}}</td>
                                    <td>{{$app_user->phone_number}}</td>
                                    <td>KSh. {{\App\Http\Traits\UniversalMethods::getLoanLimit($app_user->id)}}</td>
                                    <td><a href="{{route('user_profile',['id'=>$app_user->id])}}">More Details</a></td>
                                </tr>
                            @endforeach
                            <tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $.noConflict();
        jQuery( document ).ready(function( $ ) {
            $('#example').DataTable();
        });
    </script>
@endsection
