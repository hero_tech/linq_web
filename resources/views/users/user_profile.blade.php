@extends('layouts.user.details')
@section('names')
    {{$user->first_name  }} {{$user->second_name}}
@endsection
@section('details')
    <div class="row  user-header">
        <div class="col-lg-6">
            Profile
        </div>
        <div class="col-lg-6" style="text-align: right">
            @if(\App\Http\Traits\UniversalMethods::hasActiveLoan($user->id))
                <b style="color: #E55025">Active</b>
            @else
                @if($user->loans()->where('loan_status',4)->first() != null)
                    <td style="color: #E55025">Has a Bad Debt</td>
                @else
                    <td style="color: #012B43">Inactive</td>
                @endif
            @endif
        </div>
    </div>

    <div class="row user-content">
        <div class="col-lg-6">

            <ul>
                <li><b>Basic Details</b></li>
                <li>ID Number: {{$user->id_number}}</li>
                <li>Occupation: {{$user->occupation}}</li>
                <li>D.O.B: {{$user->date_of_birth}}</li>
                <li>Region: {{$user->region_name}}</li>
                <li>Country: {{$user->country}}</li>
            </ul>

        </div>
        <div class="col-lg-6">

            <ul>
                <li><b>Contact Details</b></li>
                <li>Phone Number: {{$user->phone_number}}</li>
                <li>Email Address: {{$user->email}}</li>
            </ul>

        </div>
    </div>

@endsection
