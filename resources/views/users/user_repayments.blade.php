@extends('layouts.user.details')
@section('names')
    {{$user->first_name  }} {{$user->second_name}}
@endsection
@section('details')

    <div class="row  user-header">
        <div class="col-lg-12">
            Repayments
        </div>
    </div>
    <div class="table-responsive" style="margin-top: 10px;margin-bottom: 10px">
        <table class="table table-bordered table-hover" id="example">
            <thead>
            <tr>
                <th>Loan</th>
                <th>Loan Due</th>
                {{--<th>Cycle</th>--}}
                {{--<th>Due</th>--}}
                <th>Amount Paid</th>
                <th>Balance</th>
                <th>Date</th>
                {{--<th>Loan Limit</th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($repayments as $repayment)
                <tr>
                    <td>Ksh. {{$repayment->principal+$repayment->interest}}</td>
                    <td>{{$repayment->due_date}}</td>
                    <td>Ksh. {{$repayment->amount}}</td>
                    <td>
                        @if($repayment->balance == 0)
                            <b class="text-success">Cleared</b>
                        @else
                            Ksh. {{$repayment->balance}}
                        @endif
                    </td>
                    <td> {{date('d-m-y', strtotime($repayment->created_at))}}</td>
                    {{--<td>{{$loan->repayment_cycle}}</td>--}}
                    {{--<td>{{$loan->clearance_date}}</td>--}}
                    {{--<td>Ksh. 1000</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>

@endsection