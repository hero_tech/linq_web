@extends('layouts.user.details')
@section('names')
    {{$user->first_name  }} {{$user->second_name}}
@endsection
@section('details')
    <div class="row  user-header">
        <div class="col-lg-12">
            Loans
        </div>
    </div>


    <div class="table-responsive" style="margin-top: 10px;margin-bottom: 10px">
        <table class="table table-bordered table-hover" id="example">
            <thead>
            <tr>
                <th>Amount</th>
                <th>Interest</th>
                <th>Cycle</th>
                <th>Due</th>
                <th>Status</th>
                {{--<th>Loan Limit</th>--}}
            </tr>
            </thead>
            <tbody>
            @foreach($loans as $loan)
                <tr>
                    <td>Ksh. {{$loan->principal_amount}}</td>
                    <td>Ksh. {{$loan->interest_amount}}</td>
                    <td>{{$loan->repayment_cycle}}</td>
                    <td>{{$loan->due_date}}</td>
                    <td>@if($loan->loan_status == 1)
                            <b class="text-primary">Active</b>
                        @elseif($loan->loan_status == 2)
                            <b class="text-success">Cleared</b>
                        @elseif($loan->loan_status == 4)
                            <b class="text-danger">Bad Debt</b>
                        @else
                            <b class="text-danger">Defaulted</b>
                        @endif
                    </td>
                    {{--<td>Ksh. 1000</td>--}}
                </tr>
            @endforeach
            </tbody>
        </table>
@endsection
