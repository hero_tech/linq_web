@extends('layouts.dash_side_top')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Edit User Details to Unflag</div>
            <form method="POST" action="{{ route('flagged_user_update',['user_id'=>$user->id]) }}" style="margin-left: 50px">
            <form method="POST" action="#" style="margin-left: 50px">
                @csrf
                <input type="text" name="f_name" value="{{ $user->first_name }}" placeholder="First Name" required autofocus>
                <label class="my-label">First Name</label>
                <br>
                <input type="text" name="m_name" value="{{ $user->second_name }}" placeholder="Middle Name" required autofocus>
                <label class="my-label">Middle Name</label>
                <br>
                <input type="text" name="o_name" value="{{ $user->surname }}" placeholder="Other Name" required autofocus>
                <label class="my-label">  Other Name</label>
                <br>
                <input type="text" name="id" value="{{ $user->id_number }}" placeholder="ID Number" required autofocus>
                <label class="my-label">ID Number</label>
                <br>
                <input type="text" name="phone" value="{{ $user->phone_number }}" placeholder="Phone Number" required autofocus>
                <label class="my-label">Phone Number</label>
                <br>

                <button type="submit" class="btn btn-primary">Update</button>

            </form>
        </div>
    </div>

@endsection
