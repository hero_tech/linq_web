@extends('layouts.dash_side_top')
@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Loan Repayments</h1>
        </div>
        <!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive table-customize">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>User</th>
                                <th>Loan Amount</th>
                                <th>Amount Paid</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            {{--                            {{$dot = array_shift($balance)}}--}}
                            <tbody style="background-color: white">
                            @for($i=0;$i<count($repayments);$i++)
                                <tr>
                                    <td><a href="{{route('user_repayments',['id'=>$repayments[$i]['user']])}}">{{$repayments[$i]['first_name']}} {{$repayments[$i]['second_name']}} {{$repayments[$i]['surname']}}</a></td>
                                    <td>KSh. {{$repayments[$i]['principal'] + $repayments[$i]['interest']}}</td>
                                    <td>KSh. {{$repayments[$i]['amount']}}</td>
                                    <td>{{\Carbon\Carbon::parse($repayments[$i]['created_at'])->format('y-m-d')}}
                                    </td>
                                    {{--                                    <td><a href="{{route('user_details',['id'=>$reminder->id])}}">More Details</a></td>--}}
                                </tr>
                            @endfor
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable();
        });
    </script>
@endsection
