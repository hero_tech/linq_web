
@extends('layouts.dash_side_top')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Edit {{$region->name}}</div>
            <form method="POST" action="{{ route('region_update',['id'=>$region->id]) }}" style="margin-left: 50px">
                @csrf

                <div class="form-group row">
                    {{--<label for="country_name" class="col-sm-4 col-form-label text-md-right">Country Name</label>--}}

                    <div class="col-md-6">
                        <input id="name" type="text" name="name" value="{{$region->name}}" required autofocus>

                        {{--@if ($errors->has('email'))--}}
                        {{--<span class="invalid-feedback" role="alert">--}}
                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                        {{--</span>--}}
                        {{--@endif--}}
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-4 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>





    {{--<form action="{{route('country_store')}}" method="post">--}}
    {{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}
    {{--<div class="form-group">--}}
    {{--<label>Country Name</label>--}}
    {{--<input type="text" name="country_name" id="country_name">--}}
    {{--</div>--}}

    {{--<div class="form-group">--}}
    {{--<input type="submit" class="btn btn-primary" name="submit" id="submit" value="Create">--}}
    {{--</div>--}}

    {{--</form>--}}

@endsection



{{--@extends('layouts.dash_side_top')--}}
{{--@section('content')--}}
{{--<div class="container">--}}
{{--<div class="row justify-content-center">--}}
{{--<div class="col-md-8">--}}
{{--<div class="card">--}}
{{--<div class="card-header">Edit Country</div>--}}

{{--<div class="card-body">--}}
{{--<form method="POST" action="{{ route('country_update',['country_id'=>$country->id]) }}">--}}
{{--@csrf--}}

{{--<div class="form-group row">--}}
{{--<label for="country_name" class="col-sm-4 col-form-label text-md-right">Country Name</label>--}}

{{--<div class="col-md-6">--}}
{{--<input id="name" type="text" name="name" value="{{$country->name}}"  placeholder="Country Name" required autofocus>--}}

{{--@if ($errors->has('email'))--}}
{{--<span class="invalid-feedback" role="alert">--}}
{{--<strong>{{ $errors->first('email') }}</strong>--}}
{{--</span>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group row mb-0">--}}
{{--<div class="col-md-8 offset-md-4">--}}
{{--<button type="submit" class="btn btn-primary">--}}
{{--Update--}}
{{--</button>--}}
{{--</div>--}}
{{--</div>--}}
{{--</form>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}
{{--</div>--}}





{{--<form action="{{route('country_store')}}" method="post">--}}
{{--<input type="hidden" name="_token" value="{{csrf_token()}}">--}}
{{--<div class="form-group">--}}
{{--<label>Country Name</label>--}}
{{--<input type="text" name="country_name" id="country_name">--}}
{{--</div>--}}

{{--<div class="form-group">--}}
{{--<input type="submit" class="btn btn-primary" name="submit" id="submit" value="Create">--}}
{{--</div>--}}

{{--</form>--}}