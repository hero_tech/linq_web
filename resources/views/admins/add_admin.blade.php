@extends('layouts.dash_side_top')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-3"></div>
            <div class="col-lg-5">
                <div class="my-form justify-content-center">
                    {{--<img src="{{asset('img/profile.jpg')}}" class="contact">--}}
                    <div class="form-header">
                        Add Admin
                    </div>
                    <form action="{{route('create_admin')}}" method="post" style="margin-left: 50px">
                        @csrf
                        <input type="text" name="first_name" placeholder="First Name"><br>
                        <input type="text" name="last_name" placeholder="Last Name"><br>
                        <input type="text" name="email" placeholder="Email"><br>
                        <button type="submit" class="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
            <div class="col-lg-4"></div>
        </div>
    </div>
@endsection