@extends('layouts.dash_side_top')
@section('content')
    <div class="container">
        <div class="row">
            {{--<div class="col lg-12 headers">--}}
            {{--Dashboard--}}
            {{--</div>--}}
        </div>
        {{--<div class="row stat-bars">--}}
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="cleared-loans-stats">
{{--                    <a href="{{route('loan_requests')}}">--}}
                    <a href="{{route('loans_cleared')}}">
                        <div class="row">
                            <div class="col-lg-4 dash-fa-icon fa fa-money fa-3x">
                            </div>
                            <div class="col-lg-8 dash-text">
                                Cleared Loans<br>
                                <h5>{{$cleared}}</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="active-loans-stats">
                    <a href="{{route('loans_active')}}">
                        <div class="row">
                            <div class="col-lg-4 dash-fa-icon fa fa-money fa-3x">
                            </div>
                            <div class="col-lg-8 dash-text">
                                Active Loans<br>
                                <h5>{{$active}}</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="other-loans-stats">
                    <a href="{{route('loans_defaulted')}}">
                        <div class="row">
                            <div class="col-lg-4 dash-fa-icon fa fa-money fa-3x">
                            </div>
                            <div class="col-lg-8 dash-text">
                                Defaulted Loans<br>
                                <h5>{{$defaulted}}</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="defaulted-loans-stats">
                    <a href="{{route('users')}}">
                        <div class="row">
                            <div class="col-lg-4 dash-fa-icon fa fa-users fa-3x">
                            </div>
                            <div class="col-lg-8 dash-text">
                                Users<br>
                                <h5>{{$users}}</h5>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            {{--<div class="col-md-3 users-stats">Users</div>--}}
        </div>

        <div class="row" style="margin-top: 20px">
            <div class="col-lg-4">
                    <div id="loans-donut-chart" style="margin-bottom: 20px"></div>
                <?= $lava->render('DonutChart', 'Loans', 'loans-donut-chart')?>
            </div>
            <div class="col-lg-8">
                    <div class="col-lg-12">
                            <div id="loans-bar-chart"></div>
                            <?= $lava->render('BarChart', 'Loans', 'loans-bar-chart')?>
                    </div>
            </div>
        </div>
        <div class="row" style="margin-top: 10px;">
            <div class="col-lg-12">
                <div id="loans-column-chart" style="min-height: 300px;margin-bottom: 10px"></div>
                <?= $lava->render('ColumnChart', 'Loans', 'loans-column-chart')?>
            </div>
        </div>

    </div>
@endsection

{{--<div class="card">--}}
{{--<div class="card-header">Dashboard</div>--}}
{{--<div class="card-body">--}}
{{--@if (session('status'))--}}
{{--<div class="alert alert-success" role="alert">--}}
{{--{{ session('status') }}--}}
{{--</div>--}}
{{--@endif--}}
{{--</div>--}}
{{--</div>--}}
