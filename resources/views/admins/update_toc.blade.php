@extends('layouts.dash_side_top')
@section('styles')
    <script src="{{ asset('https://cdn.ckeditor.com/4.11.3/full/ckeditor.js') }}"></script>
@endsection
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-10 my-form justify-content-center">
            <div class="form-header">Add Terms and Conditions</div>
            <form method="POST" action="{{ route('create_tac') }}">
                @csrf

                @if($tac != null)
                    <textarea id="editor1" type="text" name="terms_conditions" required autofocus style="margin-top: 10px">{{$tac->terms_conditions }}</textarea>
                @else
                    <textarea id="editor1" type="text" name="terms_conditions" required autofocus style="margin-top: 10px"></textarea>
                @endif

                {{--<textarea name="editor1"></textarea>--}}

                <button type="submit" class="btn btn-primary">Save</button>

            </form>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        CKEDITOR.replace('terms_conditions');
    </script>
@endsection
