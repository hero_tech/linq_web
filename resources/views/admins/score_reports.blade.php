@extends('layouts.dash_side_top')

@section('styles')

    <link rel="stylesheet" type="text/css" href="{{URL::to('https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css')}}">

@endsection

@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Metropol Score Requests</h1>
        </div>
        <!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive table-customize">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>ID Number</th>
                                <th>Identity Type</th>
                                <th>Score</th>
                                <th>Date</th>
                                {{--<th>Loan Limit</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($reports as $request)
                                <tr>
                                    <td>{{$request->user->first_name}} {{$request->user->second_name}} {{$request->user->surname}}</td>
                                    <td>{{$request->identity_number}}</td>
                                    <td>{{$request->identity_type}}</td>
                                    <td>{{$request->credit_score}}</td>
                                    <td>{{date('d-M-y', strtotime($request->created_at))}}</td>
                                    {{--<td>Ksh. 1000</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {{--    <script type="text/javascript" charset="utf8" src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js')}}}"></script>--}}
    {{--<script type="text/javascript" charset="utf8" src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js')}}"></script>--}}

    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js')}}"></script>

    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ]
            });
        });
    </script>
@endsection
