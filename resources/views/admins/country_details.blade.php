@extends('layouts.dash_side_top')
@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Country: <b style="color:#E55025 ">{{$country->name}}</b></h1>
        </div>
    </div><!--End Page Header -->
    <div class="row" style="margin-left: 3px">
        <div class="col-lg-4"></div>
        <div class="col-lg-4 regions-area">
            <div class="row">
                <div class="col-lg-6">
                    <h5>Regions</h5>
                </div>
                <div class="col-lg-6" style="text-align: center">
                    <div class="stat-bars add-button">
                        <a type="button" class="btn btn-custom" href="{{route('region_form',['id'=>$country->id])}}">Add Region</a>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="dataTables-example">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Edit</th>
                            {{--<th>Edit</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($regions as $region)
                            <tr>
                                <td>{{$region->name}}</td>
                                <td> <a href="{{route('region_edit',['id'=>$region->id])}}"
                                        class="btn btn-custom"><i class="fa fa-edit"></i></a></td>
                                {{--<td>--}}
                                    {{--<a href="{{route('country_edit',['country_id'=>$country->id])}}"--}}
                                       {{--class="btn btn-primary">Edit</a>--}}
                                {{--</td>--}}
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-4"></div>
    </div>
@endsection