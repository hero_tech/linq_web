@extends('layouts.dash_side_top')
@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div id="users-by-regions" style="height: 300px;margin-top: 20px"></div>
            <?= $lava_chart->render('GeoChart', 'Population', 'users-by-regions') ?>
        </div>
        <div class="col-lg-6">
            <div id="borrowers-by-age" style="height: 300px;margin-top: 20px;margin-right: 10px"></div>
            <?= $lava_chart->render('PieChart', 'UserGroups', 'borrowers-by-age') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6">
            <div id="requests-by-cycles" style="height: 300px;margin-top: 20px;margin-bottom: 20px"></div>
            <?= $lava_chart->render('DonutChart','Loan Cycles','requests-by-cycles') ?>
        </div>
        <div class="col-lg-6">
            <div id="pending-users" style="height: 300px;margin-top: 20px;margin-bottom: 20px;margin-right: 10px"></div>
            <?= $lava_chart->render('PieChart','UserGroupsPending','pending-users') ?>
        </div>
    </div>
@endsection