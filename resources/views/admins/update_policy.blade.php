@extends('layouts.dash_side_top')
@section('styles')
    <script src="{{ asset('https://cdn.ckeditor.com/4.11.3/full/ckeditor.js') }}"></script>
@endsection
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-10 my-form justify-content-center">
            <div class="form-header">Add/Update Privacy Policy</div>
            <form method="POST" action="{{ route('create_policy') }}">
                @csrf
                @if($pol != null)
                    <textarea id="editor1" type="text" name="policy" required autofocus style="margin-top: 10px">{{$pol->policy }}</textarea>
                @else
                    <textarea id="editor1" type="text" name="policy" required autofocus style="margin-top: 10px"></textarea>
                @endif

                {{--<textarea name="editor1"></textarea>--}}

                <button type="submit" class="btn btn-primary">Save</button>

            </form>
        </div>
    </div>

@endsection
@section('scripts')
    <script>
        CKEDITOR.replace('policy');
    </script>
@endsection
