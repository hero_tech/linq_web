@extends('layouts.dash_side_top')

@section('styles')

    <link rel="stylesheet" type="text/css" href="{{URL::to('https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css')}}">

@endsection

@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Loan Requests</h1>
        </div>
        <!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive table-customize">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th style="display: none">id</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Amount</th>
                                <th>Interest</th>
                                <th>Cycle</th>
                                <th>Accept</th>
                                <th>Reject</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($loan_requests as $loan_request)
                                <tr>
                                    <td style="display: none">{{$loan_request->id}}</td>
                                    <td><a href="{{route('user_loans',['id'=>$loan_request->user_id])}}">{{$loan_request->user->first_name}} {{$loan_request->user->second_name}} {{$loan_request->user->surname}}</a></td>
                                    <td>{{$loan_request->user->phone_number}}</td>
                                    <td>Ksh. {{$loan_request->principal_amount}}</td>
                                    <td>Ksh. {{$loan_request->interest_amount}}</td>
                                    <td>{{$loan_request->cycle->repayment_cycle}}</td>
                                    <td><a href="{{route('admin_accept_loan_request',['request_id'=>$loan_request->id])}}" class="btn btn-custom">Accept</a> </td>
                                    <td><a href="{{route('admin_reject_loan_request',['request_id'=>$loan_request->id])}}" class="btn btn-custom">Reject</a> </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    {{--    <script type="text/javascript" charset="utf8" src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js')}}}"></script>--}}
    {{--<script type="text/javascript" charset="utf8" src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js')}}"></script>--}}

    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js')}}"></script>
    <script type="text/javascript" charset="utf8"
            src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js')}}"></script>

    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                    'excel'
                ],
                order: [ 0, "desc" ],
            });
        });
    </script>
@endsection
