@extends('layouts.dash_side_top')

@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Countries</h1>
        </div><!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars add-button">
                <a type="button" class="btn btn-custom" href="{{route('country_form')}}">Add Country</a>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive table-customize">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr style="background-color: white">
                                <th>Name</th>
                                <th>Edit</th>
                                <th>Add Region</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($countries as $country)
                                <tr style="background-color: white">
                                    <td><a href="{{route('country_details',['country_id'=>$country->id])}}">{{$country->name}}</a></td>
                                    <td>
                                        <a href="{{route('country_edit',['country_id'=>$country->id])}}"
                                           class="btn btn-custom"><i class="fa fa-edit"></i></a>
                                    </td>
                                    <td>
                                        <a href="{{route('country_details',['country_id'=>$country->id])}}"
                                           class="btn btn-custom"><i class="fa fa-plus"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable();
        });
    </script>
@endsection