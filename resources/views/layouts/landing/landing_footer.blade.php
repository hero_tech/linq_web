<footer class="footer-distributed">

    <div class="row" style="float: left">
        <div class="col-lg-3">
            <div class="footer-left">

                <a href="{{url('/')}}"><h3>Linq<span>Mobile</span></h3></a>
                {{--        <a href="#"><img src="{{asset('img/logo.png')}}"></a>--}}

                <p class="footer-links">
                @if(url()->current() == route('tocs'))
                    <a href="{{route('privacy')}}">Policy</a>
                @elseif(url()->current() == route('privacy'))
                    <a href="{{route('tocs')}}" class="scroll">T&Cs</a>
                    @else
                        <a href="{{route('view_policy')}}">Policy</a>
                @endif
                    ·
                    <a href="https://play.google.com/store/apps/details?id=co.ke.muva.linq" target="_blank">Download</a>
                    .
                    <a href="{{url('/')}}#about">About</a>
                </p>

                <div class="footer-icons">

                    <a href="https://www.facebook.com/linqmobile" target="_blank"><i class="fa fa-facebook"></i></a>
                    <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
{{--                    <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>--}}
                </div>

                <p class="footer-company-name">LinqMobile&copy;2018</p>

            </div>
        </div>
        <div class="col-lg-4">
            <div class="footer-center">
                <div class="center-head">
                    Contact Details
                </div>
                <div class="center-content">
                    <ul>
                        <li><i class="fa fa-envelope fa-1x"></i>info@linq.mobi </li>
                        <li><i class="fa fa-phone fa-1x"></i> </li>
                        <li><i class="fa fa-location-arrow fa-1x"></i>Nairobi, Kenya</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="footer-right" id="contact">

                <p>Leave us a message</p>
                <form action="{{route('create_message')}}" method="POST">
                    <input type="text" name="name" id="name-enq" placeholder="Name" />
                    <input type="text" name="phone" id="phone-enq" placeholder="Phone Number" />
                    <input type="text" name="email" id="mail-enq" placeholder="Email" />
                    <textarea name="message" placeholder="Message"></textarea>
                    <input  type="submit" class="btn btn-custom" value="Send" style="background-color: #012B43;color: #FFFFFF">
                    @csrf
                </form>
                @if ($errors->any())
                    <div class="alert">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li style="color: #E55025">{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

            </div>
        </div>
    </div>

</footer>
