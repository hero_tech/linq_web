<header>
    <nav id="nav">
        <div class="menu-icon">
            <i class="fa fa-bars fa-2x"></i>
        </div>
        <div class="logo">
            <a href="{{url('/')}}"><img src="{{asset('img/logo.png')}}"></a>
        </div>
        <div class="menu">
            <ul>
                <li><a href="#about">About</a></li>
                <li><a href="#screenshots">Screenshots</a></li>
                <li><a href="#contact">Contact Us</a></li>
                <li><a href="{{route('view_tac')}}" class="scroll">T&Cs</a></li>
                <li><a href="{{route('view_policy')}}" class="scroll">Policy</a></li>
            </ul>
        </div>
    </nav>
</header>

{{--<script type="text/javascript">--}}
    {{--$(document).ready()(function () {--}}
        {{--$(".menu-icon").on("click", function () {--}}
            {{--$("nav ul").toggleClass("showing");--}}
            {{--// $("nav ul").toggleClass("showing");--}}
        {{--});--}}
    {{--});--}}
{{--</script>--}}
