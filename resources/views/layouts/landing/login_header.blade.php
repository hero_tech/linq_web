<header>
    <nav id="nav">
        <div class="menu-icon">
            <i class="fa fa-bars fa-2x"></i>
        </div>
        <div class="logo">
            <a href="{{route('landing')}}"><img src="{{asset('img/logo.png')}}"></a>
        </div>
    </nav>
</header>

<script type="text/javascript">
    $(document).ready()(function () {
        $(".menu-icon").on("click", function () {

            $("nav ul").toggleClass("showing");
        });
    });
</script>
