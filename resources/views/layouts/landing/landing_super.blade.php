<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name') }}</title>

    <link rel="icon" href="{{asset('img/logoplain.png')}}">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->

    <link href="{{ asset('font-awesome/css/font-awesome.css')}}" rel="stylesheet"/>

    {{--<link href="https://fonts.googleapis.com/css?family=Lato|Montserrat|Open+Sans|Raleway" rel="stylesheet">--}}

    <link href="https://fonts.googleapis.com/css?family=Lato|Montserrat|Open+Sans|Raleway|Roboto" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/myCss.css') }}" rel="stylesheet">
    <link href="{{ asset('css/navStyles.css') }}" rel="stylesheet">
    <link href="{{ asset('css/simple-sidebar.css') }}" rel="stylesheet">
    <link href="{{ asset('css/landing_header.css') }}" rel="stylesheet">
    <link href="{{ asset('css/landing_footer.css') }}" rel="stylesheet">
    <link href="{{ asset('css/landing_content.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body class="body">
@yield('header')
{{--@include('layouts.dash_side')--}}
<main class="landing-content">
    @yield('content')
</main>
@yield('footer')

<!-- Scripts -->
<script src="{{asset('https://code.jquery.com/jquery-3.3.1.min.js')}}" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="{{ asset('js/myScript.js') }}" defer></script>
<script src="{{ asset('js/hideField.js') }}" defer></script>
{{--<script src="{{ asset('js/navScroll.js') }}" defer></script>--}}
@yield('scripts')
</body>
</html>
