<div id="wrapper">
    <!-- Sidebar -->
    <div class="sidebar-wrapper" id="sidebar-wrapper">
        <div class="user-area">
            <div class="profile-image">
                <img src="{{asset('img/profile.jpg')}}">
                <h5>{{\Illuminate\Support\Facades\Auth::user()->first_name}}</h5>
            </div>

        </div>
        <div class="sidenav">
            <a href="{{route('super_admin_home')}}" class="{{url()->current()==route('super_admin_home')? "active": ""}}"><i class="fa fa-dashboard"></i>Dashboard</a>
{{--            <a href="{{route('loan_requests')}}" class="{{url()->current()==route('loan_requests')? "active": ""}}"><i class="fa fa-money"></i>Loan Requests</a>--}}
            <a href="{{route('loans')}}" class="{{url()->current()==route('loans')? "active": ""}}"><i class="fa fa-money"></i>Loans</a>
            <a href="{{route('code')}}" class="{{url()->current()==route('code')? "active": ""}}"><i class="fa fa-money"></i>Referral Code</a>
            {{--<button  class="dropdown-btn"><i class="fa fa-money"></i>Loans--}}
                {{--<i class="fa fa-caret-down"></i>--}}
            {{--</button>--}}
            {{--<div class="dropdown-container">--}}
                {{--<a href="{{route('loans')}}"><i class="fa fa-dollar"></i>All Loans</a>--}}
                {{--<a href="{{route('loans_active')}}"><i class="fa fa-money"></i>Active Loans</a>--}}
                {{--<a href="{{route('loans_defaulted')}}"><i class="fa fa-dollar"></i>Defaulted Loans</a>--}}
                {{--<a href="{{route('loans_cleared')}}"><i class="fa fa-money"></i>Cleared Loans</a>--}}
            {{--</div>--}}
            {{--<a href="{{route('repayments')}}" class="{{url()->current()==route('repayments')? "active": ""}}"><i class="fa fa-thumbs-up"></i>Loan Repayments</a>--}}
            <button href="{{route('admin_set_limits')}}" class="dropdown-btn" ><i class="fa fa-dollar"></i>Loan Limits
                <i class="fa fa-caret-down"></i>
            </button>
            <div class="dropdown-container">
                <a href="{{route('admin_set_limits')}}"><i class="fa fa-dollar"></i>Admin Set Limits</a>
                <a href="{{route('system_set_limits')}}"><i class="fa fa-dollar"></i>System Set Limits</a>
            </div>
            <a href="{{route('loan_payments')}}" class="{{url()->current() == route('loan_payments') ? "active":""}}" ><i class="fa fa-thumbs-up"></i>Loan Payments</a>
            <li>
                <a href="{{route('reports')}}" class="{{url()->current() == route('reports')? "active":""}}"><i class="fa fa-book"></i>Reports</a>
                <a href="{{route('score_reports')}}" class="{{url()->current() == route('score_reports')? "active":""}}"><i class="fa fa-book"></i>Score Reports</a>
                <a href="{{route('mpesa_verifications')}}" class="{{url()->current() == route('mpesa_verifications')? "active":""}}"><i class="fa fa-book"></i>Mpesa Verifications</a>
                <a href="{{route('enquiries')}}" class="{{url()->current() == route('enquiries')? "active":""}}"><i class="fa fa-question-circle"></i>Enquiries</a>
            </li>
            @if(!Auth::user()->hasAnyRole('Normal Admin') )
                <li>
                    <a href="{{route('admins')}}" class="{{url()->current()==route('admins')? "active": ""}}"><i class="fa fa-user-md"></i>Admins</a>
                </li>
            @endif
            <a href="{{route('users')}}" class="{{url()->current()==route('users')? "active": ""}}"><i class="fa fa-users"></i>Users</a>
            <a href="{{route('reminders')}}" class="{{url()->current()==route('reminders')? "active": ""}}"><i class="fa fa-bell"></i>Reminders</a>
            <a href="{{route('countries')}}" class="{{url()->current()==route('countries')? "active": ""}}"><i class="fa fa-globe"></i>Countries</a>
            <a href="{{route('repaymentcycles')}}" class="{{url()->current()==route('repaymentcycles')? "active": ""}}"><i class="fa fa-circle"></i>Repayment Cycle</a>
            <a href="{{route('minimumversions')}}" class="{{url()->current()==route('minimumversions')? "active": ""}}"><i class="fa fa-level-up"></i>Minimum App Version</a>
            <a href="{{route('faq-all')}}" class="{{url()->current()==route('faq-all')? "active": ""}}"><i class="fa fa-question"></i>FAQs</a>
            <a href="{{route('update_tac_form')}}" class="{{url()->current()==route('update_tac_form')? "active": ""}}"><i class="fa fa-question"></i>T&C's</a>
            <a href="{{route('update_privacy_policy_form')}}" class="{{url()->current()==route('update_privacy_policy_form')? "active": ""}}"><i class="fa fa-question"></i>Privacy Policy</a>
            {{--<button href="#" class="dropdown-btn"><i class="fa fa-users"></i>Users--}}
                {{--<i class="fa fa-caret-down"></i>--}}
            {{--</button>--}}
            {{--<div class="dropdown-container">--}}
                {{--<a href="{{route('users')}}"  class="{{url()->current()==route('users')? "active": ""}}"><i class="fa fa-users"></i>All Users</a>--}}
                {{--<a href="{{route('active_users')}}"><i class="fa fa-users"></i>Active</a>--}}
            {{--</div>--}}
        </div>
    </div>
    <!-- /#sidebar-wrapper -->
    <!-- Page Content -->
{{--<div id="page-content-wrapper">--}}
{{--<div class="container-fluid">--}}
{{--<a href="#menu-toggle" onclick="myFunction()" class="btn btn-secondary" id="menu-toggle">Toggle Sidebar</a>--}}
{{--</div>--}}
{{--</div>--}}
<!-- /#page-content-wrapper -->

</div>
<script>
    var dropdown = document.getElementsByClassName("dropdown-btn");
    var i;

    for (i = 0; i < dropdown.length; i++) {
        dropdown[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var dropdownContent = this.nextElementSibling;
            if (dropdownContent.style.display === "block") {
                dropdownContent.style.display = "none";
            } else {
                dropdownContent.style.display = "block";
            }
        });
    }
</script>
<!-- /#wrapper -->
