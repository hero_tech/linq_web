@extends('layouts.dash_side_top')

@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="row" style="margin-left: 15px;margin-right: 10px   ">
        <div class="col-lg-3 user-navs">
            <ul>
                <li><b>@yield('names')</b></li>
                <li><a href="{{route('user_profile',['id'=>$user->id])}}"><i class="fa fa-user"></i>Profile</a></li>
                <li><a href="{{route('user_loans',['id'=>$user->id])}}"><i class="fa fa-dollar"></i>Loans</a></li>
                <li><a href="{{route('user_repayments',['id'=>$user->id])}}"><i class="fa fa-thumbs-up"></i>Repayments</a></li>
            </ul>
        </div>
        {{--<div class="col-lg-1"></div>--}}
        <div class="col-lg-8 user-details">
           @yield('details')
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable();
        });
    </script>
@endsection