@extends('layouts.dash_side_top')
@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Loan Payments</h1>
        </div>
        <!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>User</th>
                                <th>Amount</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            {{--                            {{$dot = array_shift($balance)}}--}}
                            <tbody style="background-color: white">
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{$payment->first_name}} {{$payment->middle_name}}</td>
                                    <td>KSh. {{$payment->amount}}</td>
                                    <td>{{date('d-m-y',strtotime($payment->created_at))}}</td>
                                    {{--                                    <td><a href="{{route('user_details',['id'=>$reminder->id])}}">More Details</a></td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable();
        });
    </script>
@endsection
