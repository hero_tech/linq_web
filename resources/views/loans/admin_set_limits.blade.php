@extends('layouts.dash_side_top')
@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@endsection
@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Admin Set Loan Limits</h1>
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            @if(session()->has('failure'))
                <b style="color: #E55025"> {{ session()->get('failure') }}</b>
            @endif
        </div>
        <!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(!Auth::user()->hasAnyRole('Normal Admin') )
                <div class="row stat-bars" style="margin-left: 1px">
                    <div class="col-lg-10" style="text-align: right;">
                    </div>
                    <div class="col-lg-2 add-button">
                        <a href="{{route('select_users_table')}}" class="btn btn-custom" >Set A Limit</a>
                    </div>
                </div>
            @endif
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>User</th>
                                <th>Phone</th>
                                <th>Amount</th>
                                <th>Date Set</th>
                                {{--<th>Edit</th>--}}
                                @if(!Auth::user()->hasAnyRole('Normal Admin') )
                                    <th>Deactivate</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($limits as $limit)
                                <tr>
                                    <td>{{$limit->first_name}} {{$limit->second_name}}</td>
                                    <td>{{$limit->phone}}</td>
                                    <td>KSh. {{$limit->amount}}</td>
                                    <td>{{date('d/m/y',strtotime($limit->created_at))}}</td>
                                    @if(!Auth::user()->hasAnyRole('Normal Admin') )
                                        @if($limit->status == 1)
                                            {{--<td style="color: #E55025">Active</td>--}}
                                            <td><a href="{{route('loan_limit_reset',['id'=>$limit->id])}}"
                                                   class="btn btn-custom">Deactivate</a></td>
                                        @else
                                            {{--<td><a href="{{route('limit_update_form',['id'=>$limit->id])}}"--}}
                                            {{--class="btn btn-custom"><i class="fa fa-edit"></i> </a></td>--}}
                                            <td><a href="{{route('loan_limit_activate',['id'=>$limit->id])}}"
                                                   class="btn btn-custom">Activate</a></td>
                                        @endif
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable();
        });
    </script>

    <script>
        function openSelect() {
            document.getElementById("myForm").style.display = "block";
        }

        function closeForm() {
            document.getElementById("myForm").style.display = "none";
        }
    </script>
@endsection
