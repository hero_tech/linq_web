@extends('layouts.dash_side_top')
@section('content')
    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Edit Admin Set Limit</div>
            <form method="POST" action="{{route('limit_update',['id'=>$limit->id])}}"
                  style="margin-left: 50px">
                @csrf

                <div class="form-group row">
                    {{--<label for="country_name" class="col-sm-4 col-form-label text-md-right">Country Name</label>--}}

                    <div class="col-md-6">
                        <input id="name" type="text" name="limit" value="{{$limit->amount}}" required autofocus>

                        {{--@if ($errors->has('email'))--}}
                        {{--<span class="invalid-feedback" role="alert">--}}
                        {{--<strong>{{ $errors->first('email') }}</strong>--}}
                        {{--</span>--}}
                        {{--@endif--}}
                    </div>
                </div>

                <div class="form-group row mb-0">
                    <div class="col-md-4 offset-md-4">
                        <button type="submit" class="btn btn-primary">
                            Update
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection