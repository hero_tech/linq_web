@extends('layouts.dash_side_top')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Set Limit For {{$user->first_name}} {{$user->surname}}</div>
            <form method="POST" action="{{route('set_loan_limit',['id'=>$user->id])}}" style="margin-left: 50px">
                @csrf

                {{--<input type="text" name="current_limit" value="{{ old('loan_limit') }}" placeholder="Current limit" required autofocus>--}}

                {{--<br>--}}

                <input type="text" name="new_limit" value="{{ old('loan_limit') }}" placeholder="Set New Limit" autofocus>

                <br>

                <button type="submit" class="btn btn-primary">Create</button>

            </form>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-4 justify-content-center">
            @if ($errors->any())
                <div class="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="color: #E55025">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-4 justify-content-center">
            @if(session()->has('success'))
                <div class="alert" style="color: #E55025">
                    {{ session()->get('success') }}
                </div>
            @endif
        </div>
    </div>

@endsection
