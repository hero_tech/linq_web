@extends('layouts.dash_side_top')

@section('styles')

    <link rel="stylesheet" type="text/css" href="{{URL::to('https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/css/buttons.dataTables.min.css')}}">

@endsection

@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Loans</h1>
        </div>
        <!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive table-customize">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Amount</th>
                                <th>Interest</th>
                                <th>Due</th>
                                <th>Status</th>
                                <th>Bad Debt</th>
                                <th>Repayments</th>
                                {{--<th>Loan Limit</th>--}}
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($loans as $loan)
                                <tr>
                                    <td><a href="{{route('user_loans',['id'=>$loan->user_id])}}">{{$loan->user_first_name}} {{$loan->user_second_name}} {{$loan->user_surname}}</a></td>
                                    <td>{{$loan->user_phone_number}}</td>
                                    <td>Ksh. {{$loan->principal_amount}}</td>
                                    <td>Ksh. {{$loan->interest_amount}}</td>
                                    @if($loan->loan_status == 1)
                                        <td>{{$loan->due_date}}</td>
                                        <td style="color: #1f6fb2">Active</td>
                                    @elseif($loan->loan_status == 2)
                                        <td>{{$loan->due_date}}</td>
                                        <td style="color: #2fa360">Cleared</td>
                                    @elseif($loan->loan_status == 4)
                                        <td>{{$loan->due_date}}</td>
                                        <td style="color: #E55025">Bad Debt</td>
                                    @else
                                        <td>{{$loan->due_date}}</td>
                                        <td style="color: #E55025">Defaulted</td>
                                    @endif
                                    @if($loan->loan_status == 4)
                                        <td><a href="{{ route('bad_debt',['id'=>$loan->id,'val'=>1]) }}" class="btn btn-custom">Unset Bad Debt</a> </td>
                                    @elseif($loan->loan_status == 2)
                                        <td><a href="" class="btn btn-custom disabled">Cleared</a> </td>
                                    @else
                                        <td><a href="{{ route('bad_debt',['id'=>$loan->id,'val'=>4]) }}" class="btn btn-custom">Set as Bad Debt</a> </td>
                                    @endif
                                    <td><a href="{{route('repayments',['id'=>$loan->id])}}" class="btn btn-custom">View Repayments</a> </td>
                                    {{--<td>Ksh. 1000</td>--}}
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
{{--    <script type="text/javascript" charset="utf8" src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js')}}}"></script>--}}
    {{--<script type="text/javascript" charset="utf8" src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js')}}"></script>--}}

<script type="text/javascript" charset="utf8"
        src="{{URL::to('https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js')}}"></script>
<script type="text/javascript" charset="utf8"
        src="{{URL::to('https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js')}}"></script>
<script type="text/javascript" charset="utf8"
        src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/dataTables.buttons.min.js')}}"></script>
<script type="text/javascript" charset="utf8"
        src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.flash.min.js')}}"></script>
<script type="text/javascript" charset="utf8"
        src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js')}}"></script>
<script type="text/javascript" charset="utf8"
        src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js')}}"></script>
<script type="text/javascript" charset="utf8"
        src="{{URL::to('https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js')}}"></script>
<script type="text/javascript" charset="utf8"
        src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.html5.min.js')}}"></script>
<script type="text/javascript" charset="utf8"
        src="{{URL::to('https://cdn.datatables.net/buttons/1.5.2/js/buttons.print.min.js')}}"></script>

    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable({
                dom: 'Bfrtip',
                buttons: [
                   'excel'
                ]
            });
        });
    </script>
@endsection
