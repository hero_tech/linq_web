@extends('layouts.dash_side_top')

@section('styles')
    <link rel="stylesheet" type="text/css"
          href="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="row">
        <!-- Page Header -->
        <div class="col-lg-12">
            <h1 class="headers">Referral Code</h1>
        </div><!--End Page Header -->
    </div>
    <div class="row">
        <div class="col-lg-12">
            @if(count(\App\RefferalCode::all() ) <= 0)
                <div class="stat-bars add-button">
                    <a type="button" class="btn btn-custom" href="{{route('code-create')}}">Add Referral Code</a>
                </div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="stat-bars">
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="example">
                            <thead>
                            <tr>
                                <th>Code</th>
                                <th>Last Updated</th>
                                <th>Edit</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($codes as $code)
                                <tr>
                                    <td>{{ $code->code }}</td>
                                    <td>{{ Carbon\Carbon::parse($code->updated_at)->format('d-m-Y') }}</td>
                                    <td>
                                        <a href="{{route('code-create')}}" class="btn btn-custom"><i
                                                class="fa fa-edit"></i> </a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="https://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>
    <script>
        $.noConflict();
        jQuery(document).ready(function ($) {
            $('#example').DataTable();
        });
    </script>
@endsection
