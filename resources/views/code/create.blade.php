@extends('layouts.dash_side_top')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            @if(count(\App\RefferalCode::all()) > 0)
                <div class="form-header">Edit Referral Code</div>
            @else
                <div class="form-header">Add Referral Code</div>
            @endif
            <form method="POST" action="{{ route('code-store') }}" style="margin-left: 50px">
                @csrf
                @if(count(\App\RefferalCode::all()) > 0)
                <input type="text" name="code" value="{{ $code->code == null ? old('code') : $code->code}}"
                       placeholder="code" autofocus>
                <br>
                    <button type="submit" class="btn btn-primary">Update</button>
                @else
                    <input type="text" name="code" value=""
                           placeholder="code" autofocus>
                    <br>
                    <button type="submit" class="btn btn-primary">Create</button>
                @endif

            </form>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-4 justify-content-center">
            @if ($errors->any())
                <div class="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="color: #E55025">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

@endsection
