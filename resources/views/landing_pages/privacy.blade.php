@extends('layouts.landing.landing_super')
@section('header')
    @include('layouts.landing.tocs_header')
@endsection
@section('content')
    <div class="tocs-cover-area">
        <div class="tocs-drive-area">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="heading">
                        PRIVACY POLICY
                    </div>
                </div>
            </div>
            <div id="about"></div>
        </div>
    </div>
{{--    <div class="row">--}}
{{--        <div class="col-lg-12">--}}
{{--            <div class="content-head">--}}
{{--                <h4 style="margin-left: 30px;margin-top: 10px;text-align: center"><b>Terms and Conditions for Use of Linq Mobile Limited Mobile Application “App” and Loan Services.</b></h4>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
    <div class="about-section">
        {!! $pol->policy !!}

    </div>
@endsection
@section('footer')
    @include('layouts.landing.landing_footer')
@endsection
@section('scripts')
    <script src="{{ asset('js/navScroll.js') }}" defer></script>
@endsection














{{--@extends('layouts.landing.landing_super')--}}
{{--@section('header')--}}
{{--    @include('layouts.landing.tocs_header')--}}
{{--@endsection--}}
{{--@section('content')--}}
{{--    <div class="tocs-cover-area">--}}
{{--        <div class="tocs-drive-area">--}}
{{--            <div class="row">--}}
{{--                <div class="col-lg-12 col-md-12 col-sm-12">--}}
{{--                    <div class="heading">--}}
{{--                        PRIVACY POLICY--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <div id="about"></div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="about-section">--}}
{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Privacy Policy for Linq Mobile Limited.--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}

{{--            <p>Linq Mobile (we, us) is committed to protecting and respecting your privacy or confidentiality. </p>--}}

{{--            <p>While using our Service, we may request you to provide us with certain personally identifiable--}}
{{--                information by filling a form on the Moblie App that can be used to contact, identify you and--}}
{{--                build your personal profile in our database to enable us to serve you better.</p>--}}

{{--            <p>Personally identifiable information may include, but is not limited to, your email address, name, phone number, location and occupation.</p>--}}

{{--            <p><b>Please read the following carefully to understand our views and practices regarding your personal data and how we will treat it. </b></p>--}}
{{--        </div>--}}
{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Scope of Policy.--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}

{{--            <p>This Privacy Policy together with our Terms of Use and Loan Account Agreement as set out <a href="{{route('tocs')}}">here</a> and any additional terms of use--}}
{{--                applies to your use of Linq Mobile App downloadable on our site OR hosted on the Google Play Store, once you have downloaded or streamed a copy--}}
{{--                of the App onto your mobile phone. </p>--}}

{{--            <p>This policy sets out the basis on which any personal data we collect from you, or that you provide to us, will be processed by us.</p>--}}

{{--            <p><b>By downloading the App, you confirm that you have read, understood and accept the terms of this Policy set out hereunder. You also consent--}}
{{--                    to the collection, use, storage, processing and disclosure of your personal information in the manner set out in this Policy.  </b></p>--}}
{{--        </div>--}}
{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Service Providers.--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}

{{--            <p>Due to the nature of the Services which we provide, we are required to work with a number of third parties (including credit reference--}}
{{--                agencies and mobile network providers) and we may receive information about you from them. </p>--}}

{{--            <p>We may also employ third party companies and individuals to facilitate our Service, to provide the Service on our behalf, to perform--}}
{{--                Service-related services and/or to assist us in analyzing how our Service is used.</p>--}}

{{--            <p>These third parties have access to your Personal Information only to perform specific tasks on our behalf and are obligated not--}}
{{--                    to disclose or use your information for any other purpose. </p>--}}
{{--        </div>--}}
{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Communications.--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}

{{--            <p>We may use your Personal Information to contact you with newsletters, marketing or promotional materials and other--}}
{{--                information that may be of interest to you. </p>--}}
{{--        </div>--}}
{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Compliance with Laws.--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}

{{--            <p>We will disclose your Personal Information where required to do so by law or if we believe that such action is necessary to--}}
{{--                comply with the law and the reasonable requests of law enforcement or to protect the security or integrity of our Service.</p>--}}
{{--        </div>--}}
{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Disclosure to Third Parties .--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}
{{--            <p><b>Linq Mobile Limited may disclose your information to third parties if:</b></p>--}}

{{--            <p>We are under a duty to disclose or share your personal data in order to comply with any legal or regulatory obligation or request; and/or </p>--}}
{{--            <p>We involved in a merger, acquisition or asset sale. In such cases, we will provide notice before your Personal Information is transferred and/or--}}
{{--                becomes subject to a different Privacy Policy. </p>--}}

{{--            <p>We need to enforce our Terms and Conditions and other agreements or to investigate potential breaches; report defaulters to any credit bureau.</p>--}}

{{--            <p>We need to publish statistics relating to the use of the App, in which case all information will be aggregated and made anonymous. </p>--}}
{{--        </div>--}}
{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Security .--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}
{{--            <p>The security of your Personal Information is important to us, and we strive to implement and maintain reasonable, commercially acceptable security--}}
{{--                procedures and practices appropriate to the nature of the information we store, in order to protect it from unauthorized access, destruction, use, modification, or disclosure.</p>--}}


{{--            <p>However, please be aware that no method of transmission over the internet, or method of electronic storage is 100% secure and we are unable to guarantee the absolute security of the--}}
{{--                Personal Information we have collected from you. </p>--}}
{{--        </div>--}}
{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Where We Store Your Personal Data .--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}
{{--            <p>Your information, including Personal Information, may be transferred to — and maintained on — computers located outside of your state, province, country or other governmental--}}
{{--                jurisdiction where the data protection laws may differ than those from your jurisdiction.</p>--}}
{{--            <p>If you are located outside United States and choose to provide information to us, please note that we transfer the information, including Personal Information, to United--}}
{{--                States and process it there. </p>--}}

{{--            <p>Your consent to this Privacy Policy followed by your submission of such information represents your agreement to that transfer.</p>--}}

{{--        </div>--}}

{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Children’s Privacy.--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}
{{--            <p>Our Service does not address anyone under the age of 18.</p>--}}
{{--            <p>Only persons of age 18 years or older have permission to access our Service. </p>--}}

{{--            <p>We do not knowingly collect personally identifiable information for persons below the age of 18. If you are a parent or guardian and you learn that your Children have--}}
{{--                provided us with Personal Information, please contact us. If we become aware that we have collected Personal Information from persons below the age of 18, we take steps--}}
{{--                to remove that information from our servers.</p>--}}

{{--        </div>--}}

{{--        <div class="about-header">--}}
{{--            <i class="fa fa-arrow-circle-right"></i>Changes to Privacy Policy.--}}
{{--        </div>--}}
{{--        <div class="about-content">--}}
{{--            <p>This Privacy Policy is effective as December 20, 2018 and will remain in effect except with respect to any changes in its provisions in the future, which will be in--}}
{{--                effect immediately after being posted on this page.</p>--}}
{{--            <p>We reserve the right to update or change our Privacy Policy at any time and you should check this Privacy Policy periodically. Your continued use of the Service after we--}}
{{--                post any modifications to the Privacy Policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified Privacy Policy.</p>--}}

{{--            <p>If we make any material changes to this Privacy Policy, we will notify you either through the email address you have provided us, or by placing a prominent notice on our website.</p>--}}
{{--            <p><b>Questions, comments and requests regarding this privacy policy are welcomed and should be addressed to <b style="color: #E55025">info@linq.mobi</b> </b></p>--}}

{{--        </div>--}}




{{--    </div>--}}
{{--@endsection--}}
{{--@section('footer')--}}
{{--    @include('layouts.landing.landing_footer')--}}
{{--@endsection--}}
{{--@section('scripts')--}}
{{--    <script src="{{ asset('js/navScroll.js') }}" defer></script>--}}
{{--@endsection--}}