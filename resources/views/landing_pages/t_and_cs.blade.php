@extends('layouts.landing.landing_super')
@section('header')
    @include('layouts.landing.tocs_header')
@endsection
@section('content')
    <div class="tocs-cover-area">
        <div class="tocs-drive-area">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <div class="heading">
                        TERMS OF USE
                    </div>
                </div>
            </div>
            <div id="about"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="content-head">
                <h4 style="margin-left: 30px;margin-top: 10px;text-align: center"><b>Terms and Conditions for Use of Linq Mobile Limited Mobile Application “App” and Loan Services.</b></h4>
            </div>
        </div>
    </div>
    <div class="about-section">
        {!! $tac->terms_conditions !!}

    </div>
@endsection
@section('footer')
    @include('layouts.landing.landing_footer')
@endsection
@section('scripts')
    <script src="{{ asset('js/navScroll.js') }}" defer></script>
@endsection