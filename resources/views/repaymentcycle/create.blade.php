@extends('layouts.dash_side_top')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Add Repayment Cycle</div>
            <form method="POST" action="{{ route('repaymentcycle-store') }}" style="margin-left: 50px">
                @csrf

                <input type="text" name="repayment_cycle" value="{{ old('repayment_cycle') }}" placeholder="repayment cycle" autofocus>

                <br>

                <input type="text" name="interest_rate" value="{{ old('interest_rate') }}" placeholder="interest rate(decimal e.g 0.1)" autofocus>

                <br>

                <button type="submit" class="btn btn-primary">Create</button>

            </form>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-lg-4 justify-content-center">
            @if ($errors->any())
                <div class="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li style="color: #E55025">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        </div>
    </div>

@endsection
