@extends('layouts.dash_side_top')
@section('content')

    <div class="row justify-content-center">
        <div class="col-md-4 my-form justify-content-center">
            <div class="form-header">Edit Repayment Cycle</div>
            <form method="POST" action="{{ route('repaymentcycle-update',['id'=>$repaymentcycle->id]) }}" style="margin-left: 50px">
                @csrf

                <input type="text" name="repayment_cycle" value="{{ $repaymentcycle->repayment_cycle }}" placeholder="repayment cycle" required autofocus>

                <br>

{{--                <input type="text" name="interest_rate" value="{{ $repaymentcycle->interest_rate }}" placeholder="interest rate" required autofocus>--}}

{{--                <br>--}}

                <button type="submit" class="btn btn-primary">Update</button>

            </form>
        </div>
    </div>

@endsection
