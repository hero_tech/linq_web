<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanIncrement extends Model
{
    //
    protected $fillable=['loan_id','amount'];
}
