<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVarificationCode extends Model
{
    protected $fillable =['phone_number','code'];
}
