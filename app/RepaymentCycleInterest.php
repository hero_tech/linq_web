<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RepaymentCycleInterest extends Model
{
    //
    protected $fillable=[

        'repayment_cycle','interest_rate'

    ];
}
