<?php

namespace App\Console\Commands;

use App\AppUser;
use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use App\Loan;
use App\Reminder;
use Illuminate\Console\Command;

class SendMessage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:message';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends a message to the user at a specified time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {


//        $p = UniversalMethods::formatPhoneNumber('0705441404');
//        $mesge = 'Hi, a new story has been added that needs to be moderated before it goes live';
//        SendSMSTrait::sendSMS($mesge, "254" . $p);

        $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
            ->where('loan_status', 1)
            ->get();

//        $phone_number = UniversalMethods::formatPhoneNumber($loans->pluck('phone'));
//        $message = 'Hello '.$loans->pluck('name').', Kindly pay your loan on time to increase your loan limit. Paybill: 515657. Ac No. '.$phone_number;
//        SendSMSTrait::sendSMS($message, "+254" . $phone_number);


//        foreach ($loans as $lon){
//            if ($lon->loan_status == 1){
//                //TODO send a message weekly
//                $p=UniversalMethods::formatPhoneNumber($lon->phone);
//                $mesge = 'Hello '.$lon->name.', Kindly pay your loan on time to increase your loan limit. Paybill: 515657. Ac No. '.$p;
//                SendSMSTrait::sendSMS($mesge,$p);
//            }else{
//                $p=UniversalMethods::formatPhoneNumber($lon->phone);
//                $mesge = 'Hello '.$lon->name.', You defaulted your loan on ,'.$lon->due_date.'Kindly pay today and you will be able to borrow another immediately'.'. Paybill: 515657. Ac No. '.$p;
//                SendSMSTrait::sendSMS($mesge,$p);
//            }
//
////            if (UniversalMethods::seventhDayDefaulted($lon->id)){
////                $new_interest = $lon->principal_amount*0.1 + $lon->interest_amount;
////                $lon->interest_amount = $new_interest;
////                $lon->save();
////            }
//        }

//        foreach ($loans as $loan) {
//                if ($loan->loan_status === 1) {
//                    $phone_number = UniversalMethods::formatPhoneNumber($loan->phone);
//                    $message = 'Hello '.$loan->name.', Kindly pay your loan on time to increase your loan limit. Paybill: 515657. Ac No. '.$phone_number;
//                    SendSMSTrait::sendSMS($message, "+254" . $phone_number);
//
//                } else if ($loan->loan_status === 3) {
//                    $phone_number = UniversalMethods::formatPhoneNumber($loan->phone);
//                    $message = 'Hello '.$loan->name.', You defaulted your loan on ,'.$loan->due_date.'.Kindly pay today and you will be able to borrow another immediately'.'. Paybill: 515657. Ac No. '.$phone_number;
//                    SendSMSTrait::sendSMS($message, "+254" . $phone_number);
//                    SendSMSTrait::create_reminder($loan->id, $message);
//                }else{
//                    echo 'Good User';
//                }
//        }

    }
}
