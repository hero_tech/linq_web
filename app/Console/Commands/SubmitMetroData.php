<?php

namespace App\Console\Commands;

use App\Http\Controllers\Api\MetropolDataSubmission;
use Illuminate\Console\Command;

class SubmitMetroData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'submit:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $metropol_submission = new MetropolDataSubmission();
        $metropol_submission->submitJSON(0);

        return "Data submitted to Metropol";
    }
}
