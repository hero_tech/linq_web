<?php

namespace App\Console\Commands;

use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use App\Loan;
use App\Reminder;
use App\Repayment;
use Carbon\Carbon;
use Illuminate\Console\Command;

class SendMessageToDefaulted extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'message:defaulted';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send message to all defaulted loan users within the two months of default';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
//        echo 'sending......defaulted';


        $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
            ->where('loan_status', 3)
            ->get();
        $p = [];
        for ($i = 0; $i < count($loans); $i++) {
            $period = UniversalMethods::checkIfTwoMonthsAfterDefault($loans[$i]->id);

            if (!$period){
                $rep = Repayment::where('loan_id', $loans[$i]->id)
                    ->orderby('id', 'desc')->first();

                if ($rep == null){
                    $balance = $loans[$i]->principal_amount + $loans[$i]->interest_amount;
                }else{
                    $balance = $rep->balance;
                }

                $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                $thisDate = Carbon::parse($loans[$i]->due_date)->addDay(1)->format('Y-m-d');
                $mesge = 'Hello ' . $loans[$i]->name . ', You defaulted your loan of KSh. '.$balance.' on ' . $thisDate . '. Kindly pay today and you will be able to borrow another immediately.Log into your Linq Mobile application to make the repayment.';
                SendSMSTrait::sendSMS($mesge, $p[$i]);
//                dd($loans[$i]->id);
                $reminder =  new Reminder();
                $reminder->loan_id = $loans[$i]->id;
                $reminder->message = $mesge;
                $reminder->save();
            }else{

            }


        }
    }
}
