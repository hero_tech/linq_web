<?php

namespace App\Console;

use App\AppUser;
use App\Http\Controllers\Api\MetropolDataSubmission;
use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use App\Loan;
use App\LoanIncrement;
use App\Reminder;
use App\Repayment;
use App\UpdateLoanIncrement;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
        'App\Console\Commands\SendMessage',
        'App\Console\Commands\SendMessageToDefaulted',
        'App\Console\Commands\SubmitMetroData',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

//        $schedule->call(function (){
//            $p = UniversalMethods::formatPhoneForAT('0705441404');
//            SendSMSTrait::sendSMS('Hello Herons',$p);
//        })->everyMinute();

        /**
         * Send a weekly message to people with active loans
         */
        $schedule->call(function () {
            $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
                ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
                ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
                ->where('loan_status', 1)
                ->get();


            $p = [];
            for ($i = 0; $i < count($loans); $i++) {

                $rep = Repayment::where('loan_id', $loans[$i]->id)
                    ->orderby('id', 'desc')->first();

                if ($rep == null){
                    $balance = $loans[$i]->principal_amount + $loans[$i]->interest_amount;
                }else{
                    $balance = $rep->balance;
                }

                if ($loans[$i]->cycle->repayment_cyle == "90 days"){
                    $monthly_payment = ceil(($loans[$i]->principal_amount + $loans[$i]->interest_amount)/3) ;
                    if ($balance >= ($monthly_payment * 2)){
                        $monthly_bal = $balance - ($monthly_payment * 2);
                        $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                        $mesge = 'Hello ' . $loans[$i]->name . ', Your loan of KSh. '.$balance.' is due on'.$loans[$i]->due_date.' First month payment balance is '.$monthly_bal.' Kindly pay your loan on time to increase your loan limit. Log into your Linq Mobile application to make the repayment.';
//                $mesge = 'Hello ' . $loans[$i]->name . ', Kindly pay your loan on time to increase your loan limit. Paybill: 515657. Ac No. ' . UniversalMethods::formatPhoneForAC($p[$i]);
                        SendSMSTrait::sendSMS($mesge, $p[$i]);

                        $reminder =  new Reminder();
                        $reminder->loan_id = $loans[$i]->id;
                        $reminder->message = $mesge;
                        $reminder->save();

                    }elseif ($balance <= $monthly_payment){
                        $monthly_bal = $balance;
                        $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                        $mesge = 'Hello ' . $loans[$i]->name . ', Your loan of KSh. '.$balance.' is due on'.$loans[$i]->due_date.' Third month payment balance is '.$monthly_bal.' Kindly pay your loan on time to increase your loan limit. Log into your Linq Mobile application to make the repayment.';
//                $mesge = 'Hello ' . $loans[$i]->name . ', Kindly pay your loan on time to increase your loan limit. Paybill: 515657. Ac No. ' . UniversalMethods::formatPhoneForAC($p[$i]);
                        SendSMSTrait::sendSMS($mesge, $p[$i]);

                        $reminder =  new Reminder();
                        $reminder->loan_id = $loans[$i]->id;
                        $reminder->message = $mesge;
                        $reminder->save();
                    }else{
                        $monthly_bal = $balance - $monthly_payment;
                        $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                        $mesge = 'Hello ' . $loans[$i]->name . ', Your loan of KSh. '.$balance.' is due on'.$loans[$i]->due_date.' Second month payment balance is '.$monthly_bal.' Kindly pay your loan on time to increase your loan limit. Log into your Linq Mobile application to make the repayment.';
//                $mesge = 'Hello ' . $loans[$i]->name . ', Kindly pay your loan on time to increase your loan limit. Paybill: 515657. Ac No. ' . UniversalMethods::formatPhoneForAC($p[$i]);
                        SendSMSTrait::sendSMS($mesge, $p[$i]);

                        $reminder =  new Reminder();
                        $reminder->loan_id = $loans[$i]->id;
                        $reminder->message = $mesge;
                        $reminder->save();
                    }

                }elseif ($loans[$i]->cycle->repayment_cyle == "60 days"){
                    $monthly_payment = ceil(($loans[$i]->principal_amount + $loans[$i]->interest_amount)/2) ;
                    if ($balance >= $monthly_payment){
                        $monthly_bal = $balance - $monthly_payment;
                        $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                        $mesge = 'Hello ' . $loans[$i]->name . ', Your loan of KSh. '.$balance.' is due on'.$loans[$i]->due_date.' First month payment balance is '.$monthly_bal.' Kindly pay your loan on time to increase your loan limit. Log into your Linq Mobile application to make the repayment.';
//                $mesge = 'Hello ' . $loans[$i]->name . ', Kindly pay your loan on time to increase your loan limit. Paybill: 515657. Ac No. ' . UniversalMethods::formatPhoneForAC($p[$i]);
                        SendSMSTrait::sendSMS($mesge, $p[$i]);

                        $reminder =  new Reminder();
                        $reminder->loan_id = $loans[$i]->id;
                        $reminder->message = $mesge;
                        $reminder->save();

                    }else{
                        $monthly_bal = $balance;
                        $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                        $mesge = 'Hello ' . $loans[$i]->name . ', Your loan of KSh. '.$balance.' is due on'.$loans[$i]->due_date.' Second month payment balance is '.$monthly_bal.' Kindly pay your loan on time to increase your loan limit. Log into your Linq Mobile application to make the repayment.';
//                $mesge = 'Hello ' . $loans[$i]->name . ', Kindly pay your loan on time to increase your loan limit. Paybill: 515657. Ac No. ' . UniversalMethods::formatPhoneForAC($p[$i]);
                        SendSMSTrait::sendSMS($mesge, $p[$i]);

                        $reminder =  new Reminder();
                        $reminder->loan_id = $loans[$i]->id;
                        $reminder->message = $mesge;
                        $reminder->save();
                    }

                }else{
                    $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                    $mesge = 'Hello ' . $loans[$i]->name . ', Your loan of KSh. '.$balance.' is due on'.$loans[$i]->due_date.' Kindly pay your loan on time to increase your loan limit. Log into your Linq Mobile application to make the repayment.';
//                $mesge = 'Hello ' . $loans[$i]->name . ', Kindly pay your loan on time to increase your loan limit. Paybill: 515657. Ac No. ' . UniversalMethods::formatPhoneForAC($p[$i]);
                    SendSMSTrait::sendSMS($mesge, $p[$i]);

                    $reminder =  new Reminder();
                    $reminder->loan_id = $loans[$i]->id;
                    $reminder->message = $mesge;
                    $reminder->save();
                }
            }
        })->weekly();

        /**
         * Increment monthly loans by 15% and send a message
         */

        $schedule->call(function (){
            $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
                ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
                ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
                ->where('loan_status', 1)
                ->get();
            foreach ($loans as $loan) {
                if ($loan->cycle->repayment_cycle == "90 days"){
                    $first_month = $loan->first_month_repayment;
                    $second_month = $loan->second_month_repayment;
                    $last_month = $loan->last_month_repayment;
                    $first_default = UniversalMethods::secondTrackDayDefaulted($first_month->id);
                    $second_default = UniversalMethods::secondTrackDayDefaulted($second_month->id);
                    $last_default = UniversalMethods::secondTrackDayDefaulted($last_month->id);
                    $p = UniversalMethods::formatPhoneForAT($loan->phone);
                    $monthly_payment = ceil(($loan->principal_amount + $loans->interest_amount)/3) ;

                    if ($first_default || $second_default || $last_default){
                        $current_loan = $loan->principal_amount + $loan->interest_amount;
                        $new_loan = ceil(ceil($current_loan/3) * 0.15) + $current_loan;

                        $new_interest = $new_loan - $loan->principal_amount;
//                    dd($new_interest);
                        $increment_amount = $new_interest - $loan->interest_amount;

                        $repayments = Repayment::where('loan_id', $loan->id)
                            ->orderby('id', 'desc')->get();

                        foreach ($repayments as $repayment){
                            $repayment->balance = $repayment->balance + $increment_amount;
                            $repayment->save();
                        }

                        $loan->interest_amount = number_format(floatval($new_interest), '0', '.', '');
                        $loan->save();

                        $rep = Repayment::where('loan_id', $loan->id)
                            ->orderby('id', 'desc')->first();

                        if ($rep == null){
                            $balance = $loan->principal_amount + $loan->interest_amount;
                        }else{
                            $balance = $rep->balance + number_format(floatval($increment_amount));
                        }

                        if ($first_default){
                            $monthly_bal = $balance - ($monthly_payment * 2);
                            $date = Carbon::parse($first_month->due_date)->addDay(1)->format('Y-m-d');
                            $mesge = 'Hello ' . $loan->name . ', You defaulted your monthly loan repayment on '
                                . $date . '. You therefore incur a 15% increment on your initial loan. Current loan balance is KSh. '.$balance.' First month balance is '.$monthly_bal.'. Log into your Linq Mobile application to make the repayment. ';
                        }elseif ($second_default){
                            $monthly_bal = $balance - $monthly_payment;
                            $date = Carbon::parse($second_month->due_date)->addDay(1)->format('Y-m-d');
                            $mesge = 'Hello ' . $loan->name . ', You defaulted your loan on '
                                . $date . '. You therefore incur a 15% increment on your initial loan. Current loan balance is KSh. '.$balance.' Second month balance is '.$monthly_bal.'. Log into your Linq Mobile application to make the repayment. ';
                        }elseif ($last_default){
                            $monthly_bal = $balance;
                            $date = Carbon::parse($last_month->due_date)->addDay(1)->format('Y-m-d');
                            $mesge = 'Hello ' . $loan->name . ', You defaulted your monthly loan repayment on '
                                . $date . '. You therefore incur a 15% increment on your initial loan. Current loan balance is KSh. '.$balance.' Third month balance is '.$monthly_bal.'. Log into your Linq Mobile application to make the repayment. ';
                        }
                        SendSMSTrait::sendSMS($mesge, $p);

                        $loan_increment = new LoanIncrement();
                        $loan_increment->loan_id = $loan->id;
                        $loan_increment->amount = number_format(floatval($increment_amount), '0', '.', '');
                        $loan_increment->save();

                        $reminder =  new Reminder();
                        $reminder->loan_id = $loan->id;
                        $reminder->message = $mesge;
                        $reminder->save();

                        $increment = new UpdateLoanIncrement();
                        $increment->loan_id = $loan->id;
                        $increment->increment_no = 1;
                        $increment->save();
                    }


                }
            }
        })->daily();

        /**
         * Send a message and increment the loan by 15% after first day of loan default
         */

        $schedule->call(function () {
            $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
                ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
                ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
                ->where('loan_status', 3)
                ->get();

            foreach ($loans as $loan) {
                $defaulted = UniversalMethods::seventhDayDefaulted($loan->id);

                $p = UniversalMethods::formatPhoneForAT($loan->phone);

                $loan_increment_updated = UpdateLoanIncrement::where('loan_id',$loan->id)->first();


                if ($defaulted) {

                    $current_loan = $loan->principal_amount + $loan->interest_amount;
                    $new_loan = ceil($current_loan * 1.15);
//                    dd($new_loan);

                    $new_interest = $new_loan - $loan->principal_amount;
//                    dd($new_interest);
                    $increment_amount = $new_interest - $loan->interest_amount;

                    $repayments = Repayment::where('loan_id', $loan->id)
                        ->orderby('id', 'desc')->get();
                    foreach ($repayments as $repayment){
                        $repayment->balance = $repayment->balance + $increment_amount;
                        $repayment->save();
                    }

                    $loan->interest_amount = number_format(floatval($new_interest), '0', '.', '');
                    $loan->save();

                    $rep = Repayment::where('loan_id', $loan->id)
                        ->orderby('id', 'desc')->first();

                    if ($rep == null){
                        $balance = $loan->principal_amount + $loan->interest_amount;
                    }else{
                        $balance = $rep->balance + number_format(floatval($increment_amount));
                    }

                    $date = Carbon::parse($loan->due_date)->addDay(1)->format('Y-m-d');
                    $mesge = 'Hello ' . $loan->name . ', You defaulted your loan on '
                        . $date . '. You therefore incur a 15% increment on your initial loan. Current loan balance is KSh. '.$balance.'. Log into your Linq Mobile application to make the repayment. ';
                    SendSMSTrait::sendSMS($mesge, $p);

                    $loan_increment = new LoanIncrement();
                    $loan_increment->loan_id = $loan->id;
                    $loan_increment->amount = number_format(floatval($increment_amount), '0', '.', '');
                    $loan_increment->save();

                    $reminder =  new Reminder();
                    $reminder->loan_id = $loan->id;
                    $reminder->message = $mesge;
                    $reminder->save();

                    $increment = new UpdateLoanIncrement();
                    $increment->loan_id = $loan->id;
                    $increment->increment_no = 1;
                    $increment->save();

                }else if($loan_increment_updated != null && $loan_increment_updated->increment_no <=5){
                    $last_increment_date = Carbon::parse($loan->last_increment->updated_at);
                    if ($last_increment_date->addDays(30) >= Carbon::parse(now()->format('y-m-d'))){
                        $last_loan_increment = LoanIncrement::findOrFail($loan->increment->id);
                        $current_loan = $loan->principal_amount + $loan->interest_amount;
                        $new_loan = ceil($current_loan * 1.15);
//                    dd($new_loan);

                        $new_interest = $new_loan - $loan->principal_amount;
//                    dd($new_interest);
                        $increment_amount = $new_interest - $loan->interest_amount;

                        $repayments = Repayment::where('loan_id', $loan->id)
                            ->orderby('id', 'desc')->get();
                        foreach ($repayments as $repayment){
                            $repayment->balance = $repayment->balance + $increment_amount;
                            $repayment->save();
                        }

                        $loan->interest_amount = number_format(floatval($new_interest), '0', '.', '');
                        $loan->save();

                        $rep = Repayment::where('loan_id', $loan->id)
                            ->orderby('id', 'desc')->first();

                        if ($rep == null){
                            $balance = $loan->principal_amount + $loan->interest_amount;
                        }else{
                            $balance = $rep->balance + number_format(floatval($increment_amount));
                        }

                        $date = Carbon::parse($loan->due_date)->addDay(1)->format('Y-m-d');
                        $mesge = 'Hello ' . $loan->name . ', You defaulted your loan on '
                            . $date . '. You therefore incur a 15% monthly increment on your initial loan. Current loan balance is KSh. '.$balance.'. Log into your Linq Mobile application to make the repayment. ';
                        SendSMSTrait::sendSMS($mesge, $p);

//                        $last_loan_increment->amount = number_format(floatval($increment_amount), '0', '.', '');

                        $loan_increment = new LoanIncrement();
                        $loan_increment->loan_id = $loan->id;
                        $loan_increment->amount = number_format(floatval($increment_amount), '0', '.', '');
                        $loan_increment->save();

                        $reminder =  new Reminder();
                        $reminder->loan_id = $loan->id;
                        $reminder->message = $mesge;
                        $reminder->save();

                        $increment_new = UpdateLoanIncrement::where('loan_id',$loan->id)->first();
                        $increment_new->increment_no += $increment_new->increment_no;
                        $increment_new->save();
                    }

                }
            }
        })->daily();

        /**
         * send message to those with defaulted loans
         */

//        $schedule->call(function () {
//            $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
//                ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
//                ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
//                ->where('loan_status', 3)
//                ->get();
//            $p = [];
//            for ($i = 0; $i < count($loans); $i++) {
//                $rep = Repayment::where('loan_id', $loans[$i]->id)
//                    ->orderby('id', 'desc')->first();
//
//                if ($rep == null){
//                    $balance = $loans[$i]->principal_amount + $loans[$i]->interest_amount;
//                }else{
//                    $balance = $rep->balance;
//                }
//
//                $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
//                $mesge = 'Hello ' . $loans[$i]->name . ', You defaulted your loan of KSh. '.$balance.' on ' . $loans[$i]->due_date . '. Kindly pay today and you will be able to borrow another immediately.Log into your Linq application to make the repayment.';
////                $mesge = 'Hello ' . $loans[$i]->name . ', You defaulted your loan on ' . $loans[$i]->due_date . '. Kindly pay today and you will be able to borrow another immediately. Paybill: 515657. Ac No. ' . UniversalMethods::formatPhoneForAC($p[$i]);
//                SendSMSTrait::sendSMS($mesge, $p[$i]);
////                dd($loans[$i]->id);
//                $reminder =  new Reminder();
//                $reminder->loan_id = $loans[$i]->id;
//                $reminder->message = $mesge;
//                $reminder->save();
//
//            }
//        })->cron('0 6,13 * * 1,1');

        /**
         * Mark loan as defaulted
         */

        $schedule->call(function () {
            $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
                ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
                ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
                ->where('loan_status', 1)
                ->get();

            for ($i = 0; $i < count($loans); $i++) {

                $rep = Repayment::where('loan_id', $loans[$i]->id)
                    ->orderby('id', 'desc')->first();

                if ($rep == null){
                    $balance = $loans[$i]->principal_amount + $loans[$i]->interest_amount;
                }else{
                    $balance = $rep->balance;
                }

                $is_defaulted = UniversalMethods::checkIfDayAfterDefaulted($loans[$i]->id);
                if ($is_defaulted){
                        $loans[$i]->loan_status = 3;
                        $loans[$i]->save();

                        $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                        $thisDate = Carbon::parse($loans[$i]->due_date)->addDay(1)->format('Y-m-d');
                        $mesge = 'Hello ' . $loans[$i]->name . ', You defaulted your loan of KSh. '.$balance.' on ' . $thisDate . '. Kindly pay today and you will be able to borrow another immediately.Log into your Linq Mobile application to make the repayment.';
                        SendSMSTrait::sendSMS($mesge, $p[$i]);


                }else{
                    if ($loans[$i]->cycle->repayment_cycle == "90 days") {
                        $first_month = $loans[$i]->first_month_payment;
                        $second_month = $loans[$i]->second_month_payment;
                        $last_month = $loans[$i]->third_month_payment;
                        $monthly_payment = ceil(($loans[$i]->principal_amount + $loans[$i]->interest_amount) / 3);
                        if (UniversalMethods::checkTrackIfDayAfterDefaulted($first_month->id)) {
                            $first_month->status = 3;
                            $first_month->save();
                            $theDate = Carbon::parse($first_month->due_date)->addDay(1)->format('Y-m-d');

                            $monthly_bal = $balance - ($monthly_payment * 2);

                            $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                            $mesge = 'Hello ' . $loans[$i]->name . ', You defaulted your first month loan repayment of KSh. ' . $monthly_bal . ' on ' . $theDate . '. Kindly pay today and you will be able to borrow another immediately.Log into your Linq Mobile application to make the repayment.';
                            SendSMSTrait::sendSMS($mesge, $p[$i]);
                        }

                        if (UniversalMethods::checkTrackIfDayAfterDefaulted($second_month->id)) {
                            $second_month->status = 3;
                            $second_month->save();
                            $theDate = Carbon::parse($second_month->due_date)->addDay(1)->format('Y-m-d');

                            $monthly_bal = $balance - $monthly_payment;

                            $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                            $mesge = 'Hello ' . $loans[$i]->name . ', You defaulted your second month loan repayment of KSh. ' . $monthly_bal . ' on ' . $theDate . '. Kindly pay today and you will be able to borrow another immediately.Log into your Linq Mobile application to make the repayment.';
                            SendSMSTrait::sendSMS($mesge, $p[$i]);
                        }

                        if (UniversalMethods::checkTrackIfDayAfterDefaulted($last_month->id)) {
                            $last_month->status = 3;
                            $last_month->save();
                            $theDate = Carbon::parse($last_month->due_date)->addDay(1)->format('Y-m-d');

                            $monthly_bal = $balance;

                            $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
                            $mesge = 'Hello ' . $loans[$i]->name . ', You defaulted your second month loan repayment of KSh. ' . $monthly_bal . ' on ' . $theDate . '. Kindly pay today and you will be able to borrow another immediately.Log into your Linq Mobile application to make the repayment.';
                            SendSMSTrait::sendSMS($mesge, $p[$i]);
                        }
                    }
                }

            }
        })->everyMinute();

        $schedule->call(function (){
           $metropol_submission = new MetropolDataSubmission();
           $metropol_submission->submitJSON(0);
        })->monthly();

//        $schedule->call(function (){
//
//            $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
//                ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
//                ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
//                ->where('loan_status', 3)
//                ->get();
//            $p = [];
//            for ($i = 0; $i < count($loans); $i++) {
//                $rep = Repayment::where('loan_id', $loans[$i]->id)
//                    ->orderby('id', 'desc')->first();
//
//                if ($rep == null){
//                    $balance = $loans[$i]->principal_amount + $loans[$i]->interest_amount;
//                }else{
//                    $balance = $rep->balance;
//                }
//
//                $p[$i] = UniversalMethods::formatPhoneForAT($loans[$i]->phone);
//                $mesge = 'Hello ' . $loans[$i]->name . ', You defaulted your loan of KSh. '.$balance.' on ' . $loans[$i]->due_date . '. Kindly pay today and you will be able to borrow another immediately.Log into your Linq application to make the repayment.';
//                SendSMSTrait::sendSMS($mesge, $p[$i]);
////                dd($loans[$i]->id);
//                $reminder =  new Reminder();
//                $reminder->loan_id = $loans[$i]->id;
//                $reminder->message = $mesge;
//                $reminder->save();
//
//            }
//
//        })->weekly();



//        $schedule->command('send:message')->monthly();
        $schedule->command('message:defaulted')->weekly()->wednesdays();
        $schedule->command('message:defaulted')->weekly()->sundays();
        $schedule->command('submit:data')->monthly();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
