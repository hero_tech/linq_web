<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MonthlyLoanTrack extends Model
{
    //

    protected $fillable =[
      'loan_id','amount','month','status','start_at','due_date','clearance_date'
    ];
}
