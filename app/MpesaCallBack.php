<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MpesaCallBack extends Model
{
    protected  $fillable=[
        'ip','caller','content'
    ];
}
