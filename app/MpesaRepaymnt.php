<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MpesaRepaymnt extends Model
{
    //
    protected  $fillable=[
        'identifier','amount','msisdn','transaction_id','first_name','middle_name','last_name','bill_reference','balance','time'
    ];
}
