<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetropolCrb extends Model
{
    //
    protected $fillable =[
        'user_id','identity_number','identity_type','credit_score'
    ];
}
