<?php
/**
 * Created by PhpStorm.
 * User: victorscott
 * Date: 9/24/18
 * Time: 6:51 PM
 */

namespace App\Http\Traits;


use App\AdminSetLimit;
use App\Loan;
use App\MetropolCrb;
use App\MetropolDetail;
use App\MonthlyLoanTrack;
use App\SystemSetLimit;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\DB;

trait UniversalMethods
{
    /**
     * @param $phone_number
     * @return string
     */
    public static function formatPhoneNumber($phone_number): string
    {
        if (starts_with($phone_number, "07")) {
            $phone_number = "" . substr($phone_number, 1);
        }
        return $phone_number;
    }

    public static function getValidationErrorsAsString($errorArray)
    {
        $errorArrayTemp = [];
        $error_strings = "";
        foreach ($errorArray as $index => $item) {
            $errStr = $item[0];
            array_push($errorArrayTemp, $errStr);
        }
        if (!empty($errorArrayTemp)) {
            $error_strings = implode('. ', $errorArrayTemp);
        }

        return $error_strings;
    }

    public static function validationErrorsToString($errArray)
    {
        $valArr = array();
        foreach ($errArray->toArray() as $key => $value) {
            $errStr = $value[0];
            array_push($valArr, $errStr);
        }
        if (!empty($valArr)) {
            $errStrFinal = implode(',', $valArr);
        }
        return $errStrFinal;
    }

    public static function getLoanLimit($user_id)
    {
        $admin_set_limit = DB::table('admin_set_limits')
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->orderBy('created_at', 'desc')
            ->first();
//        dd($admin_set_limit);
        $system_set_limit = DB::table('system_set_limits')
            ->where('user_id', $user_id)
            ->where('status', 1)
            ->first();
        if ($admin_set_limit !== null) {
            $loan_limit = $admin_set_limit->amount;
        } else {
            $loan_limit = $system_set_limit->amount;
        }
        return $loan_limit;
    }

    public static function getFormattedDate($timestamp)
    {
        return Carbon::parse($timestamp)->format('d-m-y');
    }

//    check if the user is applying for a loan for the first time
    public static function isFirstLoanApplication($user_id)
    {
//        $metropol_score = MetropolCrb::where('user_id', $user_id)
//            ->get()->toArray();
//        $metropol_details = MetropolDetail::where('user_id', $user_id)
//            ->get()->toArray();


        $loans = Loan::where('user_id', $user_id)
            ->get()->toArray();
        if (!empty($loans)) {
            return false;
        } else {
            return true;
        }
    }
    public static function metropolRecordExist($user_id)
    {
        $metropol = MetropolCrb::where('user_id', $user_id)
            ->get()->toArray();
        if (!empty($metropol)) {
            return false;
        } else {
            return true;
        }
    }

    public static function metropolDetailRecordExist($user_id)
    {
        $metropol = MetropolDetail::where('user_id', $user_id)
            ->get()->toArray();
        if (!empty($metropol)) {
            return false;
        } else {
            return true;
        }
    }


//    check if the user has an active performing or non-performing loan
    public static function hasActiveLoan($user_id)
    {
        $loan = Loan::where('user_id', $user_id)
            ->where(function ($q) {
                $q->where('loan_status', '=', 1)
                    ->orWhere('loan_status', '=', 3)
                    ->orWhere('loan_status', '=', 4);
            })->first();
        if ($loan == null) {
            return false;
        } else {
            return true;
        }
    }

//    check if the previous loan way defaulted
    public static function wasThePreviousLoanDefaulted($user_id)
    {
        $previousLoan = Loan::where('user_id', $user_id)
            ->where('loan_status', 2)
            ->orderby('created_at', 'desc')
            ->first();
        $due = Carbon::parse($previousLoan->due_date);
        $cleared = Carbon::parse($previousLoan->clearance_date);
        if ($cleared > $due) {
            return true;
        } else {
            return false;
        }
    }

//    check if the previous loan was defaulted for more than 2 months

    public static function wasPreviousLoanDefaultedByTwoMonths($user_id)
    {
        $previousLoan = Loan::where('user_id', $user_id)
            ->where('loan_status', 2)
            ->orderby('created_at', 'desc')
            ->first();
        $due = Carbon::parse($previousLoan->due_date);
        $cleared = Carbon::parse($previousLoan->clearance_date);
        if ($due->addDays(60) <= $cleared) {
            return true;
        } else {
            return false;
        }
    }

//    reset loanLimit

    public static function resetSystemLimit($user_id)
    {
        $limit = SystemSetLimit::where('user_id', $user_id)->first();
        $limit->amount = 1000;
        $limit->save();
        return $limit;

    }

//increase user limit by 1000

    public static function checkIfAlreadyIncremented($user_id)
    {

        $loan = Loan::where('user_id', $user_id)
            ->where('loan_status', 2)
            ->orderby('updated_at', 'desc')
            ->first();
        $limit = SystemSetLimit::where('user_id', $user_id)->first();

        $cleared = Carbon::parse($loan->updated_at);
        $updated = Carbon::parse($limit->updated_at);

        if ($updated > $cleared) {
            return true;
        } else {
            return false;
        }


    }

    public static function incrementSystemLimit($user_id)
    {
        $limit = SystemSetLimit::where('user_id', $user_id)->first();
        $limit->amount += 1000;
        $limit->incremented_at = Carbon::parse(now());
        $limit->save();

        return $limit;
    }

    public static function seventhDayDefaulted($loan_id)
    {
        $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
            ->where('loans.id', $loan_id)
            ->where('loan_status', 3)
            ->first();
//        dd($loans);

        if ($loans == null){
            return false;
        }else{
            $due = Carbon::parse($loans->due_date);
//            dd($due);
            $now = Carbon::parse(now()->format('y-m-d'));
//            dd($now);
            if ($due->addDays(2)->equalTo($now)) {
                return true;
            } else {
                return false;
            }
        }

    }

    public static function secondTrackDayDefaulted($track_id)
    {
        $track = MonthlyLoanTrack::where('id',$track_id)->first();

        if ($track == null){
            return false;
        }else{
            $due = Carbon::parse($track->due_date);
//            dd($due);
            $now = Carbon::parse(now()->format('y-m-d'));
//            dd($now);
            if ($due->addDays(2)->equalTo($now)) {
                return true;
            } else {
                return false;
            }
        }

    }


    public static function checkIfLastIncrementedIsMonth($user_id)
    {
        $limit = SystemSetLimit::where('user_id', $user_id)->first();
        $today = Carbon::parse(now());
        if (Carbon::parse($limit->incremented_at)->addDays('30') <=$today) {
            return true;
        } else {
            return false;
        }
    }

    public static function formatPhoneForAT($phone_number)
    {
        // Replace numbers starting with 07 or +254 with 254 due to what safaricom expects
        if (substr($phone_number, 0, strlen("07")) == "07") {
            $phone_number = "+2547".substr($phone_number, strlen("07"));
        } elseif (substr($phone_number,0, strlen("+2547")) == "+2547"){
            $phone_number = "+2547".substr($phone_number, strlen("+2547"));
        }
        return $phone_number;

    }
    public static function formatPhoneForAC($phone_number){
        if (substr($phone_number, 0, strlen("07")) == "07") {
            $phone_number = "07".substr($phone_number, strlen("07"));
        } elseif (substr($phone_number,0, strlen("+2547")) == "+2547"){
            $phone_number = "07".substr($phone_number, strlen("+2547"));
        }
        return $phone_number;
    }
    public static function checkIfDefaulted($loan_id){


        $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
            ->where('loans.id', $loan_id)
            ->where('loan_status', 1)
            ->first();
//        dd($loans);

        if ($loans == null){
            return false;
        }else{
            $due = Carbon::parse($loans->due_date);
//            dd($due);
            $now = Carbon::parse(now());
            if ($due<$now) {
                return true;
            } else {
                return false;
            }
        }

    }

    public static function checkIfDayAfterDefaulted($loan_id)
    {
        $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
            ->where('loans.id', $loan_id)
            ->where('loan_status', 1)
            ->first();

        if ($loans == null){
            return false;
        }else{
            $due = Carbon::parse($loans->due_date)->addDay(1);
            $now = Carbon::parse(now());
            if ($due<$now) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function checkTrackIfDayAfterDefaulted($track_id)
    {
        $track = MonthlyLoanTrack::where('id',$track_id)->first();

        if ($track == null){
            return false;
        }else{
            $due = Carbon::parse($track->due_date)->addDay(1);
            $now = Carbon::parse(now());
            if ($due<$now) {
                return true;
            } else {
                return false;
            }
        }
    }

    public static function checkIfTwoMonthsAfterDefault($loan_id){

        $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
            ->where('loans.id', $loan_id)
            ->where('loan_status', 3)
            ->first();

        if ($loans == null){
            return false;
        }else{
            $due = Carbon::parse($loans->due_date);
//            dd($due);
            $now = Carbon::parse(now()->format('y-m-d'));
//            dd($now);
//            dd($now > $due->addDays(60));
            if ($now > $due->addDays(60)) {
                return true;
            } else {
                return false;
            }
        }

    }
}
