<?php
/**
 * Created by PhpStorm.
 * User: phillip
 * Date: 2/9/18
 * Time: 3:20 PM
 */

namespace App\Http\Traits;


use App\Classes\AfricasTalkingGateway;
use App\Classes\AfricasTalkingGatewayException;
use App\Reminder;

trait SendSMSTrait
{
    public static function sendSMS($message, $recipients)
    {

        $username   = "LinqMobile";//enter your username
//        $apikey     = "c882e63522ed9b1305b78070ca0c38a0fa52d21056d6c01d35364bbed68d01f4";//"b6467432bcbf272938ceb7a6f1c57dbc015d4e447931e58eac3659210367bbe2";//enter your apikey
        $apikey     = "237c3739db43552933659e320cde6341577580541e19f43ed8e155e1fa00d9e3";//"b6467432bcbf272938ceb7a6f1c57dbc015d4e447931e58eac3659210367bbe2";//enter your apikey

        // Specify your AfricasTalking shortCode or sender id"LinqMobile"
        $from = "LinqMobile";

        // Specify your authentication credentials
       // $username   = "LinqMobile";//enter your username
       // $apikey     = "4f027863840d20dd61aa0d8cf48ca24d66f999ff1f103abe71c32266bc768823";//enter your apikey

            // Specify your AfricasTalking shortCode or sender id
//        $from = "";

            // Specify the numbers that you want to send to in a comma-separated list
            // Please ensure you include the country code (+254 for Kenya in this case)
//        $recipients = "+254711XXXYYY,+254733YYYZZZ";

            // And of course we want our recipients to know what we really do
//        $message    = "I'm a lumberjack and its ok, I sleep all night and I work all day";
            // Create a new instance of our awesome gateway class
        $gateway    = new AfricasTalkingGateway($username, $apikey);

        // Any gateway error will be captured by our custom Exception class below,
        // so wrap the call in a try-catch block
        try
        {
            // Thats it, hit send and we'll take care of the rest.
//            $results = $gateway->sendMessage($recipients, $message);
            $results = $gateway->sendMessage($recipients, $message,$from);

            foreach($results as $result) {
                // status is either "Success" or "error message"
//                echo " Number: " .$result->number;
//                echo " Status: " .$result->status;
//                echo " MessageId: " .$result->messageId;
//                echo " Cost: "   .$result->cost."\n";
                if ($result->status=="Success")
                    return true;
                else
                    return false;

            }

        } catch ( AfricasTalkingGatewayException $e )
        {
            echo "Encountered an error while sending: ".$e->getMessage();
            return $e->getMessage();
        }
    }

    public static function notifyUsers()
    {
        //logic to determine who to notify
        return 'hello';
    }

    public static function create_reminder($loan_id, $message){

        $reminder = new Reminder();
        $reminder->loan_id = $loan_id;
        $reminder->message = $message;
        $reminder->save();

        return $reminder;

    }


}
