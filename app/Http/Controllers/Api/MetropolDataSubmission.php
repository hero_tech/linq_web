<?php

namespace App\Http\Controllers\Api;

use App\Loan;
use App\MetropolSubmissionTracker;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MetropolDataSubmission extends Controller
{
//    public $public_key = "s5XPZjrzOviEVOjMzKlXfMEp9Ur9q1JM";
    public $public_key = "YDb3e90xGRZK5GUUsuzl8akMaw7KdWP7"; //live
//    public $private_key = "mYdQEse8X7QTM1yj";
    public $private_key = "BbJ9uztG1UyJuG2z"; //live

    public function getAccessToken()
    {

//        $url = 'https://api.metropol.co.ke:5558/api/v1/datasubmission/authenticate?grant_type=client_credentials';
        $url = 'https://api.metropol.co.ke:22228/api/v1/datasubmission/authenticate?grant_type=client_credentials'; //live

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $this->getHash(), 'cache-control: no-cache'));

        $curl_response = curl_exec($curl);

        $access_token = json_decode($curl_response);


        return $access_token->access_token;

    }

    public function submitJSON($last_record_submitted_id)
    {
//        $url = "https://api.metropol.co.ke:5558/api/v1/datasubmission/submit/data";
        $url = "https://api.metropol.co.ke:22228/api/v1/datasubmission/submit/data"; //live
        $curl = curl_init();

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getSubmissionHeaders());
        curl_setopt($curl, CURLOPT_POSTFIELDS, $this->getSubmissionPayload($last_record_submitted_id));

        $curl_response = curl_exec($curl);
//        dd($curl_response);

        $startfrom = $last_record_submitted_id;
        $endin = $startfrom + 10;

        /*$startfrom = 0;
        $endin = 2;*/

        $test = json_decode($curl_response);
//        dd($test);

        if ($test->success == true) {

            $maximumsubmitid = Loan::where(function ($q) {
                $q->where('loan_status', 1)
                    ->orWhere('loan_status', 2)
                    ->orWhere('loan_status', 3);
            })
                ->orderby('id', 'DESC')
                ->first();

//            dd($maximumsubmitid);

            $loans = Loan::where(function ($q) {
                $q->where('loan_status', 1)
                    ->orWhere('loan_status', 2)
                    ->orWhere('loan_status', 3);
            })
                ->where('id', '>', $startfrom)
                ->where('id', '<=', $endin)
                ->orderby('id', 'DESC')
                ->first();

            $metropoldatasubmissiontracker = new MetropolSubmissionTracker();
            $metropoldatasubmissiontracker['submission_date'] = Carbon::now()->format('d-m-Y');
            $metropoldatasubmissiontracker['last_record_id'] = $loans->id;
            $metropoldatasubmissiontracker->save();


            $checksubmittedidtocontinue = MetropolSubmissionTracker::where('submission_date', Carbon::now()->format('d-m-Y'))
                ->orderby('id', 'DESC')
                ->first();

            $diferenceindatatosubmit = $maximumsubmitid->id - $checksubmittedidtocontinue->last_record_id;

            if ($diferenceindatatosubmit > 0) {
                $this->submitJSON($checksubmittedidtocontinue->last_record_id + 1);
            }

        }

        return $curl_response;

    }

    public function getSubmissionHeaders()
    {
        $headers = [
            'Authorization:Bearer ' . $this->getAccessToken(),
            'Cache-Control:no-cache',
            'Content-Type:application/json'
        ];
        //dd($headers);

        return $headers;
    }

    public function getHash()
    {
        return $credentials = base64_encode($this->public_key . ":" . $this->private_key);
    }

    public function getSubmissionPayload($last_record_submitted_id)
    {

        $startfrom = $last_record_submitted_id;
        $endin = $startfrom + 40;

        /*$startfrom = 0;
        $endin = 2;*/

        $loans = Loan::where(function ($q) {
            $q->where('loan_status', 1)
                ->orWhere('loan_status', 2)
                ->orWhere('loan_status', 3);
        })
            ->where('id', '>', $startfrom)
            ->where('id', '<=', $endin)
            ->get();

        $datetoday = Carbon::now()->format('Ymd');
        $totaldata = array();

        foreach ($loans as $loan) {

            /**
             * APPLICABLE STATUSES
             * settled - H
             *
             */

            $acc_status = "";

            $balance = 0;
            $days_areas = 0;

            if ($loan->latest_repayment == null) {
                $latest_payment = 0;
                $payment_date = "";
            } else {
                $latest_payment = $loan->latest_repayment->amount;
                $payment_date = $loan->latest_repayment->created_at->format('Ymd');
            }

            //dd($loan->loan_status != 4);

            if ($loan->app_user["status"] == 2) {
                $acc_status = "A"; //closed
            } else if (($loan->app_user["status"] == 1)) {
                if ($loan->loan_status == 1) {
                    $balance = $loan->balance;
                    $acc_status = "F"; // active
                    $days_areas = 0;
                } elseif ($loan->loan_status == 2) {
                    $acc_status = "H"; //settled
                    $days_areas = 0;
                } elseif ($loan->loan_status == 3) {
                    $acc_status = "C"; //write-off
                    $balance = $loan->balance;
                    $days_areas = Carbon::now()->diffInDays(Carbon::parse($loan->due_date)->addDay(1));
                }
            } else {
                $acc_status = "K"; //suspended
            }

            if ($balance == 0) {
                $overdue_date = "";
            } else {
                $overdue_date = str_replace("-", "", $loan->due_date);
            }

            if ($days_areas >= 0 && $days_areas <= 30) {
                $prc = "A";
            } elseif ($days_areas > 30 && $days_areas <= 90) {
                $prc = "B";
            } elseif ($days_areas > 90 && $days_areas <= 180) {
                $prc = "C";
            } elseif ($days_areas > 180 && $days_areas <= 360) {
                $prc = "D";
            } else {
                $prc = "E";
            }

            $datatosubmit = [
                "account_number" => $loan->id,
                "account_product_type" => "I",
                "account_status" => $acc_status,
                "account_status_date" => $datetoday,
                "current_balance_kes" => strval($balance),
                "client_number" => $loan->app_user->phone_number,
                "currency_facility" => "KES",
                "current_balance" => strval($balance),
                "days_arrears" => $days_areas,
                "disbursement_date" => $loan->created_at->format('Ymd'),
                "dob" => str_replace("-", "", $loan->app_user->date_of_birth),
                "forename1" => $loan->app_user->first_name,
                "forename2" => $loan->app_user->second_name,
                "forename3" => $loan->app_user->surname,
                "gender" => $loan->app_user->gender,
                "installment_amount" => strval($loan->peincipal_amount + $loan->interest_amount),
                "installment_arrears" => strval($balance),
                "installment_due_date" => str_replace("-", "", $loan->due_date),
                "latest_payment_amount" => $latest_payment,
                "latest_payment_date" => $payment_date,
                "mobile_telephone_number" => $loan->app_user->phone_number,
                "nationality" => "KE",
                "original_amount" => strval($loan->peincipal_amount + $loan->interest_amount),
                "overdue_balance" => strval($balance),
                "overdue_date" => $overdue_date,
                "primary_id_doc_no" => $loan->app_user->id_number,
                "primary_id_doc_type" => "001",
                "prudential_risk_classification" => $prc,
                "repayment_period" => str_replace(" ", "", $loan->cycle->interest_rate * 100),
                "surname" => $loan->app_user->surname,
                "trading_as" => ""
            ];

            array_push($totaldata, $datatosubmit);

        }

        //dd($totaldata);

        $payload = array(
            'data_ref' => 'MF',
            'institution_ref' => 'M',
            'is_final_submission' => 'true',
            'submission_date' => $datetoday,
            'submission_type' => 'M',
            'template_version' => '4',
            'callback_url' => 'https://linq.mobi/api/metropol/report/callback',
            'data' => $totaldata
        );

        $result = json_encode($payload);
        //dd($result);

        return $result;
    }

//    public function myTest()
//    {
//
//        $this->submitJSON(0);
//
//        return "Data submitted to Metropol";
//    }

    public function callback(Request $request)
    {
//        dd($request);

        return $request->data[0]['status'];
    }

}
