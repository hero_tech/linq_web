<?php

namespace App\Http\Controllers\Api;

use App\MinimumVersion;
use App\Http\Controllers\Controller;

class MinAppVersionController extends Controller
{
    public function getMinAppVersion()
    {
        $minimumversion = MinimumVersion::orderBy('id', 'desc')->first();
        return response()->json($minimumversion);
    }
}
