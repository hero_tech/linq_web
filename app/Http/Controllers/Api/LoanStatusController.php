<?php

namespace App\Http\Controllers\Api;

use App\Loan;
use App\LoanRepaymentRequest;
use App\LoanRequest;
use App\StkPushPaymentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanStatusController extends Controller
{
    public function checkPendingLoanRequest(Request $request){

        $pendingrequest = LoanRequest::where('user_id',$request->appuser_id)
            ->where('loan_request_status',1)
            ->first();

        return response()->json($pendingrequest);

    }

    public function checkPendingLoanPayment(Request $request){

        $app_user_id = $request->appuser_id;

        $pendingpaymentstkpush = StkPushPaymentRequest::leftjoin('loans','stk_push_payment_requests.loan_id','=','loans.id')
            ->join('app_users','loans.user_id','=','app_users.id')
            ->where('app_users.id', '=',$app_user_id)
            ->where('stk_push_payment_requests.status', '=',1)
            ->select('stk_push_payment_requests.*')
            ->orderby('stk_push_payment_requests.id','desc')
            ->first();

        $pendingpaymentc2b = LoanRepaymentRequest::leftjoin('loans','loan_repayment_requests.loan_id','=','loans.id')
            ->join('app_users','loans.user_id','=','app_users.id')
            ->where('app_users.id', '=',$app_user_id)
            ->where('loan_repayment_requests.status', '=',1)
            ->select('loan_repayment_requests.*')
            ->orderby('loan_repayment_requests.id','desc')
            ->first();

        $pendingpayment = ([
            'stkpush'=> $pendingpaymentstkpush,
            'c2b'=> $pendingpaymentc2b,
        ]);

        return response()->json($pendingpayment);

    }

}
