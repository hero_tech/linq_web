<?php

namespace App\Http\Controllers\Api;

use App\Loan;
use App\LoanRepaymentRequest;
use App\LoanRequest;
use App\StkPushPaymentRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AppBasicDataOnLaunchController extends Controller
{

    public function getBasicData(Request $request)
    {
        $app_user_id = $request->appuser_id;

        $activeloan = Loan::select(\DB::raw('loans.*, SUM(repayments.amount) as repayment'))
            ->leftJoin('repayments', 'repayments.loan_id', '=', 'loans.id')
            ->Join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->where('app_users.id', '=',$app_user_id)
            ->where( function ($q){
                $q->where('loan_status',1)
                    ->orWhere('loan_status',3)
                    ->orWhere('loan_status',4);
            })
            ->groupBy('loans.id')
            ->orderby('loans.id','desc')
            ->first();

        $pendingrequest = LoanRequest::where('user_id',$app_user_id)
            ->where('loan_request_status',1)
            ->first();

        $pendingpaymentstkpush = StkPushPaymentRequest::leftjoin('loans','stk_push_payment_requests.loan_id','=','loans.id')
            ->join('app_users','loans.user_id','=','app_users.id')
            ->where('app_users.id', '=',$app_user_id)
            ->where('stk_push_payment_requests.status', '=',1)
            ->select('stk_push_payment_requests.*')
            ->orderby('stk_push_payment_requests.id','desc')
            ->first();

        $pendingpaymentc2b = LoanRepaymentRequest::leftjoin('loans','loan_repayment_requests.loan_id','=','loans.id')
            ->join('app_users','loans.user_id','=','app_users.id')
            ->where('app_users.id', '=',$app_user_id)
            ->where('loan_repayment_requests.status', '=',1)
            ->select('loan_repayment_requests.*')
            ->orderby('loan_repayment_requests.id','desc')
            ->first();

        if($pendingpaymentstkpush != null){
            $basicdata = ([
                'status_code'=> 4,
                'action'=> "processing_stk_repayment",
            ]);
        }elseif ($pendingpaymentc2b != null){
            $basicdata = ([
                'status_code'=> 5,
                'action'=> "processing_c2b_repayment",
            ]);
        }elseif ($pendingrequest != null){
            $basicdata = ([
                'status_code'=> 2,
                'action'=> "processing_loan_request",
            ]);
        }elseif ($activeloan != null){
            $basicdata = ([
                'status_code'=> 3,
                'action'=> "active_loan_pay_loan",
            ]);
        }elseif($pendingpaymentstkpush == null &&
            $pendingpaymentc2b == null &&
            $pendingrequest == null &&
            $activeloan == null){
            $basicdata = ([
                'status_code'=> 1,
                'action'=> "new_user_or_has_no_loan",
            ]);
        }else{
            $basicdata = ([
                'status_code'=> 7,
                'action'=> "unknown_status",
            ]);
        }

        return response()->json($basicdata);

    }

}
