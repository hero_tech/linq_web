<?php

namespace App\Http\Controllers\Api;

use App\AppUser;
use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use App\VerificationMpesaCall;
use App\VerificationMpesaPayment;
use App\VerificationStkPushRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MpesaVerificationStkPushController extends Controller
{
    public function stkPush(Request $request) //From AJAX call/android side, etc....start USSD pay request

    {
//        dd($request);
        $amount = 1;

        $user_id = $request->user_id;

        $user = AppUser::where('id', $user_id)
            ->first();
//        dd($user);

        $phone_paying = $user->phone_number;


        //remove 07 for those that come with it

        if ($this->startsWith($phone_paying, "07")) {

            $pos = strpos($phone_paying, "07");

            if ($pos !== false) {

                $phone_paying = substr_replace($phone_paying, "2547", $pos, 2);

            }


            //$phone_paying=str_replace("0","254",$phone_paying);

        }


        //Validate the inputs

        if ($amount === "" || $phone_paying === "" || $user_id === "" || !$this->startsWith($phone_paying, "2547") ||

            $amount < 1 || $amount > 70000 || $user_id < 1 || filter_var($user_id, FILTER_VALIDATE_INT) === false ||

            filter_var($amount, FILTER_VALIDATE_INT) === false) {

            return json_encode(
                [
                    "success" => 0,
                    "message" => "Wrong input"
                ]);

        }


        //Variables specific to this application

        $merchant_id = "515657"; //C2B Shortcode/Paybill
//        $merchant_id = "174379"; //C2B Shortcode/Paybill

        $callback_url = "https://linq.mobi/api/verification/callback"; //"https://linq.mobi/api/callback";

//        $passkey = "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919"; //Ask from Safaricom guys..
        $passkey = "b0072c3dcd2de0316cb621afb13b42540a00288f6ad2c624552c286a2d13d1a6"; //Ask from Safaricom guys..(live)

        $account_reference =  $phone_paying; //like account number while paying via paybill

        $transaction_description = 'Pay for User:' . $phone_paying;

      $user = AppUser::where('id',$user_id)->first();

//        return $loan;
        //LOG the Request. This is done just to keep a record of the online payment calls you have made

        $call = new VerificationMpesaCall();

        $caller = "MPESA_VERIFICATION_CALL";

        $ip = $request->ip();

        $content = "Phone: " . $phone_paying . " | Amount: " . $amount;

        $call->content = $content;

        $call->ip = $ip;

        $call->caller = $caller;

        $call->save();


        //Initiate PUSH

        $timestamp = date("YmdHis");

        $password = base64_encode($merchant_id . $passkey . $timestamp); //No more Hashing like before. this is a guideline from Saf

        $access_token = $this->getAccessToken();


        $curl = curl_init();

        $endpoint_url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

        curl_setopt($curl, CURLOPT_URL, $endpoint_url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $access_token)); //setting custom header


        $curl_post_data = array(

            'BusinessShortCode' => $merchant_id,

            'Password' => $password,

            'Timestamp' => $timestamp,

            'TransactionType' => 'CustomerPayBillOnline',

            'Amount' => $amount,

            'PartyA' => $phone_paying,

            'PartyB' => $merchant_id,

            'PhoneNumber' => $phone_paying,

            'CallBackURL' => $callback_url,

            'AccountReference' => $account_reference,

            'TransactionDesc' => $transaction_description

        );


        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);
//        dd($curl_response);

        $result = json_decode($curl_response);
//        dd($result);


//        return $curl_response;

        VerificationStkPushRequest::create([
            'user_id' => $user->id,
            'merchant_request_id' => $result->MerchantRequestID,
            'checkout_request_id' => $result->CheckoutRequestID,
            'amount' => $amount,
            'status' => 1
        ]);

        if (array_key_exists("errorCode", $result)) { //Error

            return json_encode(
                [
                    "success" => 0,
                    "message" => "Request Failed"
                ]);

        } else if ("ResponseCode" == 0) { //Success
            //create the pending request...

            return json_encode([
                "success" => 1,
                "message" => "Request Sent Successfully"
            ]);
//            return response()->json(['result'=> $result]);

        } else {

            return json_encode([
                "success" => 0,
                "message" => "Unknown Error, Please Retry"
            ]);

        }

    }

    public function verification_callback(Request $request)
    { //ONLY in STK push requests
//        $body = $request->Body;

//        $myresponse= response()->json(['body'=> $body]);

        $content = json_encode($request->getContent());

        $result_code = $request->Body['stkCallback']['ResultCode'];
        $mercahnt_id = $request->Body['stkCallback']['MerchantRequestID'];
        $checkout_id = $request->Body['stkCallback']['CheckoutRequestID'];

        $identifier = "MPESA_VERIFICATION_RESPONSE";

        $ip = $request->ip();

        $callback = new VerificationMpesaCall();

        $callback->ip = $ip;

        $callback->content = $content;

        $callback->caller = $identifier;

        $callback->save();

        $stk_repayment_request = VerificationStkPushRequest::where('merchant_request_id', $mercahnt_id)
            ->first();


        $user = AppUser::where('id',$stk_repayment_request->user_id)->first();

        if ($result_code == 0) {

            $stk_repayment_request->status = 2;
            $stk_repayment_request->save();
            $item = $request->Body['stkCallback']['CallbackMetadata']['Item'][0]['Value'];
//
            $balance = 0;
            $amount = $request->Body['stkCallback']['CallbackMetadata']['Item'][0]['Value'];
            $trans_id = $request->Body['stkCallback']['CallbackMetadata']['Item'][1]['Value'];
            $time = $request->Body['stkCallback']['CallbackMetadata']['Item'][3]['Value'];

            $mpesa_repayment = new VerificationMpesaPayment();
            $mpesa_repayment->identifier = 'STK_PUSH_VERIFICATION_REPAYMENT';
            $mpesa_repayment->amount = $amount;
            $mpesa_repayment->transaction_id = $trans_id;
            $mpesa_repayment->balance = $balance;
            $mpesa_repayment->msisdn = $user->phone_number;
            $mpesa_repayment->first_name = $user->first_name;
            if($user->second_name != null){
                $mpesa_repayment->middle_name = $user->second_name;
            }
            $mpesa_repayment->last_name = $user->surname;
            $mpesa_repayment->bill_reference = $user->phone_number;
            $mpesa_repayment->time = $time;
            $mpesa_repayment->save();

//                $phone_number = UniversalMethods::formatPhoneNumber($user->phone_number);
//                $fname = $user->first_name;
//
//                $message = "Hi, "
//                    . $fname. ". Your Mpesa verification amount of 1 KSh has been received";
//                SendSMSTrait::sendSMS($message,"+254".$phone_number);

                return json_encode(["success" => 1, "message" => "Kindly pay the expected balance"]);

        } else {

            $stk_repayment_request->status = 3;
            $stk_repayment_request->save();
        }

        return $request->Body['stkCallback']['ResultCode'];

    }



    public function getAccessToken()
    {

        //Variables specific to this application

//        $consumer_key = "t86V46LwTBzhBuqM2v8Wqlhof63uagx9"; //Get these two from DARAJA Platform
        $consumer_key = "9454GBUyZvVM2lLGPZhQjAOPCL9tYphP"; //Get these two from DARAJA Platform(live)

//        $consumer_secret = "DC47sjXXlP5MBATB";
        $consumer_secret = "M06TKXtUk6glMG1s";//live

        $credentials = base64_encode($consumer_key . ":" . $consumer_secret);

        //START CURL

        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
//        $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

//        $url = 'https://sendbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
//        $url = 'https://sendbo.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $credentials));

        curl_setopt($curl, CURLOPT_HEADER, false);//Make it not return headers...true retirns header

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);//MAGIC..

        $curl_response = curl_exec($curl);

//        dd($curl_response);

        $access_token = json_decode($curl_response);

        return $access_token->access_token;

    }


    function startsWith($haystack, $needle)

    {

        $length = strlen($needle);

        return (substr($haystack, 0, $length) === $needle);

    }
}
