<?php

namespace App\Http\Controllers\Api;

use App\AdminUserVerification;
use App\AppUser;
use App\Classes\Metropol;
use App\Http\Traits\UniversalMethods;
use App\Loan;
use App\MetropolCrb;
use App\MetropolDetail;
use App\MetropolScoreRequest;
use App\MpesaVerification;
use App\SystemSetLimit;
use Carbon\Carbon;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MetropolController extends Controller
{
    //
//    public $PublicKey = 'khDzpeuhuSmxnnsSVcFRBrSxtKxbBb';
    public $PublicKey = 'PBuMjReipMBXQsFSCkBboeHDbVxBeX';

//    public $PrivateKey = 'BKPcdJVQSAdfBubJusZIgPcVRJiHwshUYmbVCLHIrpdaiOrMFfItMmdtuHbi';
    public $PrivateKey = 'rcKAOGVXJZMuSMzUrWpVcMpBImxwhWENgMfWJhxpATAsPBDbRBfeXXoPKLUe';


    /**
     * Does all the checks at the point af loan application by an app user
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function apply(Request $request)
    {
        $user_id = $request->user_id;
        $user = AppUser::where('app_users.id', $user_id)->first();
//        dd($user);
        $firstApplicationChecker = UniversalMethods::isFirstLoanApplication($user_id);
//        dd($firstApplicationChecker);


        $metropoldetail = MetropolDetail::where('user_id', $user_id)->first();
        $metropolcrb = MetropolCrb::where('user_id', $user_id)->first();


        if ($metropoldetail == null) {
            if ($metropolcrb == null) {

                $limit = $this->clientCrbScore($request)->getContent();
                $names = $this->clientCrbName($request)->getContent();

//            dd($limit,$names);
                $loan_limit = json_decode($limit);
                $user_names = json_decode($names);

                if (strcasecmp($user->first_name, $user_names->first_name) == 0 && (strcasecmp($user->surname, $user_names->last_name) == 0
                        || strcasecmp($user->surname, $user_names->other_name) == 0 || strcasecmp($user->middle_name, $user_names->last_name) == 0 ||
                        strcasecmp($user->middle_name, $user_names->other_name) == 0)) {
                    if ($loan_limit->loan_limit == null) {
                        $loanlimit = 0;
                    } else {
                        $loanlimit = $loan_limit->loan_limit;
                    }

                    if ($user_names->other_names == null) {
                        $othernames = "NULL";
                    } else {
                        $othernames = $user_names->other_names;
                    }

                    $verify = MpesaVerification::where('user_id', $user_id)->where('verification_status', 1)->first();

                    if ($verify !== null) {

                        $admin_verification_status = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 2)->first();

                        if ($admin_verification_status !== null) {

                            return response()->json([
                                'first_name' => $user_names->first_name,
                                'last_name' => $user_names->last_name,
                                'other_name' => $othernames,
                                'dob' => $user_names->dob,
                                'loan_limit' => $loanlimit,
                                'code' => 1,
                                'message' => 'success',
                            ]);

                        } else {

                            $admin_unverified = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 1)->first();

                            if ($admin_unverified == null) {

                                $admin_verification = new AdminUserVerification();
                                $admin_verification->app_user_id = $user_id;
                                $admin_verification->user_id = 1;
                                $admin_verification->admin_verification_status = 1;
                                $admin_verification->save();

                            }

                            if ($loanlimit < 1000) {
                                return response()->json([
                                    'first_name' => $user_names->first_name,
                                    'last_name' => $user_names->last_name,
                                    'other_name' => $othernames,
                                    'dob' => $user_names->dob,
                                    'loan_limit' => $loanlimit,
                                    'code' => 1,
                                    'message' => 'success',
                                ]);
                            } else {

                                return response()->json([
                                    'first_name' => $user->first_name,
                                    'last_name' => $user->surname,
                                    'other_name' => $othernames,
                                    'dob' => $user->date_of_birth,
                                    'loan_limit' => 0,
                                    'code' => 8,
                                    'message' => 'not verified by admin',
                                ]);
                            }
                        }

                    } else {

                        if ($loanlimit < 1000) {
                            return response()->json([
                                'first_name' => $user_names->first_name,
                                'last_name' => $user_names->last_name,
                                'other_name' => $othernames,
                                'dob' => $user_names->dob,
                                'loan_limit' => $loanlimit,
                                'code' => 1,
                                'message' => 'success',
                            ]);
                        } else {
                            return response()->json([
                                'first_name' => $user->first_name,
                                'last_name' => $user->surname,
                                'other_name' => $othernames,
                                'dob' => $user->date_of_birth,
                                'loan_limit' => 0,
                                'code' => 7,
                                'message' => 'mpesa not verified',
                            ]);
                        }

                    }


                } else {

                    $user->status = 3;
                    $user->save();

                    if ($user->second_name == null) {
                        $othernames = "NULL";
                    } else {
                        $othernames = $user->second_name;
                    }

                    return response()->json([
                        'first_name' => $user->first_name,
                        'last_name' => $user->surname,
                        'other_name' => $othernames,
                        'dob' => $user->date_of_birth,
                        'loan_limit' => 0,
                        'code' => 0,
                        'message' => 'details do not match',
                    ]);
                }
            }
        } else {

            if ($firstApplicationChecker) {


                $limit = SystemSetLimit::where('user_id', $user_id)->first();
                $detail = MetropolDetail::where('user_id', $user_id)->first();

                if ($detail->other_name == null) {
                    $othernames = "NULL";
                } else {
                    $othernames = $detail->other_name;
                }

                $verify = MpesaVerification::where('user_id', $user_id)->where('verification_status', 1)->first();

                if ($verify !== null) {

                    $admin_verification_status = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 2)->first();

                    if ($admin_verification_status !== null) {

                        return response()->json([
                            'first_name' => $detail->first_name,
                            'last_name' => $detail->last_name,
                            'other_name' => $othernames,
                            'dob' => $detail->dob,
                            'loan_limit' => $limit->amount,
                            'code' => 1,
                            'message' => 'success',
                        ]);

                    } else {

                        $admin_unverified = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 1)->first();

                        if ($admin_unverified == null) {

                            $admin_verification = new AdminUserVerification();
                            $admin_verification->app_user_id = $user_id;
                            $admin_verification->user_id = 1;
                            $admin_verification->admin_verification_status = 1;
                            $admin_verification->save();

                        }

                        if ($limit->amount < 1000) {
                            return response()->json([
                                'first_name' => $detail->first_name,
                                'last_name' => $detail->last_name,
                                'other_name' => $othernames,
                                'dob' => $detail->dob,
                                'loan_limit' => $limit->amount,
                                'code' => 1,
                                'message' => 'success',
                            ]);
                        } else {

                            return response()->json([
                                'first_name' => $detail->first_name,
                                'last_name' => $detail->last_name,
                                'other_name' => $othernames,
                                'dob' => $detail->dob,
                                'loan_limit' => 0,
                                'code' => 8,
                                'message' => 'not verified by admin',
                            ]);
                        }
                    }


                } else {

                    if ($limit->amount < 1000) {
                        return response()->json([
                            'first_name' => $detail->first_name,
                            'last_name' => $detail->last_name,
                            'other_name' => $othernames,
                            'dob' => $detail->dob,
                            'loan_limit' => $limit->amount,
                            'code' => 1,
                            'message' => 'success',
                        ]);
                    } else {
                        return response()->json([
                            'first_name' => $detail->first_name,
                            'last_name' => $detail->last_name,
                            'other_name' => $othernames,
                            'dob' => $detail->dob,
                            'loan_limit' => 0,
                            'code' => 7,
                            'message' => 'mpesa not verified',
                        ]);
                    }

                }


            } else {
                $hasActiveLoan = UniversalMethods::hasActiveLoan($user_id);
                $metropoldetails = MetropolDetail::where('user_id', $user_id)->first();
                if ($hasActiveLoan == true) {
                    if ($metropoldetails->other_name == null) {
                        $othernames = "NULL";
                    } else {
                        $othernames = $metropoldetails->other_name;
                    }
                    return response()->json([
                        'first_name' => $metropoldetails->first_name,
                        'last_name' => $metropoldetails->last_name,
                        'other_name' => $othernames,
                        'dob' => $metropoldetails->dob,
                        'loan_limit' => 0,
                        'code' => 0,
                        'message' => 'The user has an active loan'
                    ]);
                } else {
                    $lastLoanDefaulted = UniversalMethods::wasThePreviousLoanDefaulted($user_id);
//                dd($lastLoanDefaulted);
                    if ($lastLoanDefaulted) {
                        $shouldBeDowngraded = UniversalMethods::wasPreviousLoanDefaultedByTwoMonths($user_id);
                        if ($shouldBeDowngraded) {
                            // reset loanLimit to 1000
                            $limit = UniversalMethods::resetSystemLimit($user_id);
                            if ($metropoldetails->other_name == null) {
                                $othernames = "NULL";
                            } else {
                                $othernames = $metropoldetails->other_name;
                            }


                            $verify = MpesaVerification::where('user_id', $user_id)->where('verification_status', 1)->first();

                            if ($verify !== null) {


                                $admin_verification_status = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 2)->first();

                                if ($admin_verification_status !== null) {

                                    return response()->json([
                                        'first_name' => $metropoldetails->first_name,
                                        'last_name' => $metropoldetails->last_name,
                                        'other_name' => $othernames,
                                        'dob' => $metropoldetails->dob,
                                        'lian_limit' => $limit->amount,
                                        'code' => 1,
                                        'message' => 'success',
                                    ]);

                                } else {

                                    $admin_unverified = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 1)->first();

                                    if ($admin_unverified == null) {

                                        $admin_verification = new AdminUserVerification();
                                        $admin_verification->app_user_id = $user_id;
                                        $admin_verification->user_id = 1;
                                        $admin_verification->admin_verification_status = 1;
                                        $admin_verification->save();
                                    }

                                    if ($limit->amount < 1000) {
                                        return response()->json([
                                            'first_name' => $metropoldetails->first_name,
                                            'last_name' => $metropoldetails->last_name,
                                            'other_name' => $othernames,
                                            'dob' => $metropoldetails->dob,
                                            'lian_limit' => $limit->amount,
                                            'code' => 1,
                                            'message' => 'success',
                                        ]);
                                    } else {

                                        return response()->json([
                                            'first_name' => $metropoldetails->first_name,
                                            'last_name' => $metropoldetails->last_name,
                                            'other_name' => $othernames,
                                            'dob' => $metropoldetails->dob,
                                            'loan_limit' => 0,
                                            'code' => 8,
                                            'message' => 'not verified by admin',
                                        ]);
                                    }
                                }

                            } else {

                                if ($limit->amount < 1000) {
                                    return response()->json([
                                        'first_name' => $metropoldetails->first_name,
                                        'last_name' => $metropoldetails->last_name,
                                        'other_name' => $othernames,
                                        'dob' => $metropoldetails->dob,
                                        'lian_limit' => $limit->amount,
                                        'code' => 1,
                                        'message' => 'success',
                                    ]);
                                } else {
                                    return response()->json([
                                        'first_name' => $metropoldetails->first_name,
                                        'last_name' => $metropoldetails->last_name,
                                        'other_name' => $othernames,
                                        'dob' => $metropoldetails->dob,
                                        'loan_limit' => 0,
                                        'code' => 7,
                                        'message' => 'mpesa not verified',
                                    ]);
                                }

                            }

                        } else {
                            // loanLimit remains the same
                            $limit = SystemSetLimit::where('user_id', $user_id)->first();
                            if ($metropoldetails->other_name == null) {
                                $othernames = "NULL";
                            } else {
                                $othernames = $metropoldetails->other_name;
                            }


                            $verify = MpesaVerification::where('user_id', $user_id)->where('verification_status', 1)->first();

                            if ($verify !== null) {

                                $admin_verification_status = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 2)->first();

                                if ($admin_verification_status !== null) {

                                    return response()->json([
                                        'first_name' => $metropoldetails->first_name,
                                        'last_name' => $metropoldetails->last_name,
                                        'other_name' => $othernames,
                                        'dob' => $metropoldetails->dob,
                                        'lian_limit' => $limit->amount,
                                        'code' => 1,
                                        'message' => 'success',
                                    ]);

                                } else {


                                    $admin_unverified = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 1)->first();

                                    if ($admin_unverified == null) {

                                        $admin_verification = new AdminUserVerification();
                                        $admin_verification->app_user_id = $user_id;
                                        $admin_verification->user_id = 1;
                                        $admin_verification->admin_verification_status = 1;
                                        $admin_verification->save();

                                    }

                                    if ($limit->amount < 1000) {
                                        return response()->json([
                                            'first_name' => $metropoldetails->first_name,
                                            'last_name' => $metropoldetails->last_name,
                                            'other_name' => $othernames,
                                            'dob' => $metropoldetails->dob,
                                            'lian_limit' => $limit->amount,
                                            'code' => 1,
                                            'message' => 'success',
                                        ]);
                                    } else {

                                        return response()->json([
                                            'first_name' => $metropoldetails->first_name,
                                            'last_name' => $metropoldetails->last_name,
                                            'other_name' => $othernames,
                                            'dob' => $metropoldetails->dob,
                                            'loan_limit' => 0,
                                            'code' => 8,
                                            'message' => 'not verified by admin',
                                        ]);
                                    }
                                }

                            } else {

                                if ($limit->amount < 1000) {
                                    return response()->json([
                                        'first_name' => $metropoldetails->first_name,
                                        'last_name' => $metropoldetails->last_name,
                                        'other_name' => $othernames,
                                        'dob' => $metropoldetails->dob,
                                        'lian_limit' => $limit->amount,
                                        'code' => 1,
                                        'message' => 'success',
                                    ]);
                                } else {
                                    return response()->json([
                                        'first_name' => $metropoldetails->first_name,
                                        'last_name' => $metropoldetails->last_name,
                                        'other_name' => $othernames,
                                        'dob' => $metropoldetails->dob,
                                        'loan_limit' => 0,
                                        'code' => 7,
                                        'message' => 'mpesa not verified',
                                    ]);
                                }

                            }

                        }
                    } else {
                        // increment user loanLimit by 1000
                        $is_loan_limit_updated = UniversalMethods::checkIfAlreadyIncremented($user_id);
                        if ($is_loan_limit_updated) {
                            $limit = SystemSetLimit::where('user_id', $user_id)->first();
                            if ($metropoldetails->other_name == null) {
                                $othernames = "NULL";
                            } else {
                                $othernames = $metropoldetails->other_name;
                            }

                            $verify = MpesaVerification::where('user_id', $user_id)->where('verification_status', 1)->first();

                            if ($verify !== null) {

                                $admin_verification_status = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 2)->first();

                                if ($admin_verification_status !== null) {

                                    return response()->json([
                                        'first_name' => $metropoldetails->first_name,
                                        'last_name' => $metropoldetails->last_name,
                                        'other_name' => $othernames,
                                        'dob' => $metropoldetails->dob,
                                        'lian_limit' => $limit->amount,
                                        'code' => 1,
                                        'message' => 'success',
                                    ]);

                                } else {

                                    $admin_unverified = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 1)->first();

                                    if ($admin_unverified == null) {

                                        $admin_verification = new AdminUserVerification();
                                        $admin_verification->app_user_id = $user_id;
                                        $admin_verification->user_id = 1;
                                        $admin_verification->admin_verification_status = 1;
                                        $admin_verification->save();

                                    }

                                    if ($limit->amount < 1000) {
                                        return response()->json([
                                            'first_name' => $metropoldetails->first_name,
                                            'last_name' => $metropoldetails->last_name,
                                            'other_name' => $othernames,
                                            'dob' => $metropoldetails->dob,
                                            'lian_limit' => $limit->amount,
                                            'code' => 1,
                                            'message' => 'success',
                                        ]);
                                    } else {

                                        return response()->json([
                                            'first_name' => $metropoldetails->first_name,
                                            'last_name' => $metropoldetails->last_name,
                                            'other_name' => $othernames,
                                            'dob' => $metropoldetails->dob,
                                            'loan_limit' => 0,
                                            'code' => 8,
                                            'message' => 'not verified by admin',
                                        ]);
                                    }
                                }

                            } else {

                                if ($limit->amount < 1000) {
                                    return response()->json([
                                        'first_name' => $metropoldetails->first_name,
                                        'last_name' => $metropoldetails->last_name,
                                        'other_name' => $othernames,
                                        'dob' => $metropoldetails->dob,
                                        'lian_limit' => $limit->amount,
                                        'code' => 1,
                                        'message' => 'success',
                                    ]);
                                } else {
                                    return response()->json([
                                        'first_name' => $metropoldetails->first_name,
                                        'last_name' => $metropoldetails->last_name,
                                        'other_name' => $othernames,
                                        'dob' => $metropoldetails->dob,
                                        'loan_limit' => 0,
                                        'code' => 7,
                                        'message' => 'mpesa not verified',
                                    ]);
                                }

                            }

                        } else {
                            $lmt = SystemSetLimit::where('user_id', $user_id)->first();
                            if ($lmt->incremented_at == null) {
                                $limit = UniversalMethods::incrementSystemLimit($user_id);
                                if ($metropoldetails->other_name == null) {
                                    $othernames = "NULL";
                                } else {
                                    $othernames = $metropoldetails->other_name;
                                }

                                $verify = MpesaVerification::where('user_id', $user_id)->where('verification_status', 1)->first();

                                if ($verify !== null) {

                                    $admin_verification_status = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 2)->first();

                                    if ($admin_verification_status !== null) {

                                        return response()->json([
                                            'first_name' => $metropoldetails->first_name,
                                            'last_name' => $metropoldetails->last_name,
                                            'other_name' => $othernames,
                                            'dob' => $metropoldetails->dob,
                                            'lian_limit' => $limit->amount,
                                            'code' => 1,
                                            'message' => 'success',
                                        ]);

                                    } else {

                                        $admin_unverified = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 1)->first();

                                        if ($admin_unverified == null) {

                                            $admin_verification = new AdminUserVerification();
                                            $admin_verification->app_user_id = $user_id;
                                            $admin_verification->user_id = 1;
                                            $admin_verification->admin_verification_status = 1;
                                            $admin_verification->save();

                                        }

                                        if ($limit->amount < 1000) {
                                            return response()->json([
                                                'first_name' => $metropoldetails->first_name,
                                                'last_name' => $metropoldetails->last_name,
                                                'other_name' => $othernames,
                                                'dob' => $metropoldetails->dob,
                                                'lian_limit' => $limit->amount,
                                                'code' => 1,
                                                'message' => 'success',
                                            ]);
                                        } else {

                                            return response()->json([
                                                'first_name' => $metropoldetails->first_name,
                                                'last_name' => $metropoldetails->last_name,
                                                'other_name' => $othernames,
                                                'dob' => $metropoldetails->dob,
                                                'loan_limit' => 0,
                                                'code' => 8,
                                                'message' => 'not verified by admin',
                                            ]);
                                        }
                                    }

                                } else {

                                    if ($limit->amount < 1000) {
                                        return response()->json([
                                            'first_name' => $metropoldetails->first_name,
                                            'last_name' => $metropoldetails->last_name,
                                            'other_name' => $othernames,
                                            'dob' => $metropoldetails->dob,
                                            'lian_limit' => $limit->amount,
                                            'code' => 1,
                                            'message' => 'success',
                                        ]);
                                    } else {
                                        return response()->json([
                                            'first_name' => $metropoldetails->first_name,
                                            'last_name' => $metropoldetails->last_name,
                                            'other_name' => $othernames,
                                            'dob' => $metropoldetails->dob,
                                            'loan_limit' => 0,
                                            'code' => 7,
                                            'message' => 'mpesa not verified',
                                        ]);
                                    }

                                }

                            } else {
                                $last_increment = UniversalMethods::checkIfLastIncrementedIsMonth($user_id);
                                if ($last_increment) {
                                    $limit = UniversalMethods::incrementSystemLimit($user_id);
                                    if ($metropoldetails->other_name == null) {
                                        $othernames = "NULL";
                                    } else {
                                        $othernames = $metropoldetails->other_name;
                                    }

                                    $verify = MpesaVerification::where('user_id', $user_id)->where('verification_status', 1)->first();

                                    if ($verify !== null) {


                                        $admin_verification_status = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 2)->first();

                                        if ($admin_verification_status !== null) {

                                            return response()->json([
                                                'first_name' => $metropoldetails->first_name,
                                                'last_name' => $metropoldetails->last_name,
                                                'other_name' => $othernames,
                                                'dob' => $metropoldetails->dob,
                                                'lian_limit' => $limit->amount,
                                                'code' => 1,
                                                'message' => 'success',
                                            ]);

                                        } else {

                                            $admin_unverified = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 1)->first();

                                            if ($admin_unverified == null) {

                                                $admin_verification = new AdminUserVerification();
                                                $admin_verification->app_user_id = $user_id;
                                                $admin_verification->user_id = 1;
                                                $admin_verification->admin_verification_status = 1;
                                                $admin_verification->save();

                                            }

                                            if ($limit->amount < 1000) {
                                                return response()->json([
                                                    'first_name' => $metropoldetails->first_name,
                                                    'last_name' => $metropoldetails->last_name,
                                                    'other_name' => $othernames,
                                                    'dob' => $metropoldetails->dob,
                                                    'lian_limit' => $limit->amount,
                                                    'code' => 1,
                                                    'message' => 'success',
                                                ]);
                                            } else {

                                                return response()->json([
                                                    'first_name' => $metropoldetails->first_name,
                                                    'last_name' => $metropoldetails->last_name,
                                                    'other_name' => $othernames,
                                                    'dob' => $metropoldetails->dob,
                                                    'loan_limit' => 0,
                                                    'code' => 8,
                                                    'message' => 'not verified by admin',
                                                ]);
                                            }
                                        }

                                    } else {

                                        if ($limit->amount < 1000) {
                                            return response()->json([
                                                'first_name' => $metropoldetails->first_name,
                                                'last_name' => $metropoldetails->last_name,
                                                'other_name' => $othernames,
                                                'dob' => $metropoldetails->dob,
                                                'lian_limit' => $limit->amount,
                                                'code' => 1,
                                                'message' => 'success',
                                            ]);
                                        } else {
                                            return response()->json([
                                                'first_name' => $metropoldetails->first_name,
                                                'last_name' => $metropoldetails->last_name,
                                                'other_name' => $othernames,
                                                'dob' => $metropoldetails->dob,
                                                'loan_limit' => 0,
                                                'code' => 7,
                                                'message' => 'mpesa not verified',
                                            ]);
                                        }

                                    }

                                } else {
                                    $limit = SystemSetLimit::where('user_id', $user_id)->first();
                                    if ($metropoldetails->other_name == null) {
                                        $othernames = "NULL";
                                    } else {
                                        $othernames = $metropoldetails->other_name;
                                    }

                                    $verify = MpesaVerification::where('user_id', $user_id)->where('verification_status', 1)->first();

                                    if ($verify !== null) {

                                        $admin_verification_status = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 2)->first();

                                        if ($admin_verification_status !== null) {

                                            return response()->json([
                                                'first_name' => $metropoldetails->first_name,
                                                'last_name' => $metropoldetails->last_name,
                                                'other_name' => $othernames,
                                                'dob' => $metropoldetails->dob,
                                                'lian_limit' => $limit->amount,
                                                'code' => 1,
                                                'message' => 'success',
                                            ]);

                                        } else {

                                            $admin_unverified = AdminUserVerification::where('app_user_id', $user_id)->where('admin_verification_status', 1)->first();

                                            if ($admin_unverified == null) {

                                                $admin_verification = new AdminUserVerification();
                                                $admin_verification->app_user_id = $user_id;
                                                $admin_verification->user_id = 1;
                                                $admin_verification->admin_verification_status = 1;
                                                $admin_verification->save();

                                            }

                                            if ($limit->amount < 1000) {
                                                return response()->json([
                                                    'first_name' => $metropoldetails->first_name,
                                                    'last_name' => $metropoldetails->last_name,
                                                    'other_name' => $othernames,
                                                    'dob' => $metropoldetails->dob,
                                                    'lian_limit' => $limit->amount,
                                                    'code' => 1,
                                                    'message' => 'success',
                                                ]);
                                            } else {

                                                return response()->json([
                                                    'first_name' => $metropoldetails->first_name,
                                                    'last_name' => $metropoldetails->last_name,
                                                    'other_name' => $othernames,
                                                    'dob' => $metropoldetails->dob,
                                                    'loan_limit' => 0,
                                                    'code' => 8,
                                                    'message' => 'not verified by admin',
                                                ]);
                                            }
                                        }

                                    } else {

                                        if ($limit->amount < 1000) {
                                            return response()->json([
                                                'first_name' => $metropoldetails->first_name,
                                                'last_name' => $metropoldetails->last_name,
                                                'other_name' => $othernames,
                                                'dob' => $metropoldetails->dob,
                                                'lian_limit' => $limit->amount,
                                                'code' => 1,
                                                'message' => 'success',
                                            ]);
                                        } else {
                                            return response()->json([
                                                'first_name' => $metropoldetails->first_name,
                                                'last_name' => $metropoldetails->last_name,
                                                'other_name' => $othernames,
                                                'dob' => $metropoldetails->dob,
                                                'loan_limit' => 0,
                                                'code' => 7,
                                                'message' => 'mpesa not verified',
                                            ]);
                                        }

                                    }

                                }
                            }
                        }
                    }
                }
            }

        }
    }

    function clientCrbName(Request $request)
    {
        $user = AppUser::where('id', $request->user_id)
            ->first();
        $id_number = $user->id_number;
        $metropol = new Metropol($this->PublicKey, $this->PrivateKey);
        $client_name = $metropol->identityVerification($id_number);
//        dd($client_name->gender);
        $first_name = $client_name->first_name;
        $last_name = $client_name->last_name;
        $other_names = $client_name->other_name;
        $dob = $client_name->dob;
        $identity_number = $client_name->identity_number;
        $identity_type = $client_name->identity_type;
        $user_id = $user->id;
        $gender = $client_name->gender;

        //        dd($client_name);

        $metropol_details = new MetropolDetail();
        $metropol_details->user_id = $user_id;
        $metropol_details->first_name = $first_name;
        $metropol_details->last_name = $last_name;
        $metropol_details->other_name = $other_names;
        $metropol_details->gender = $gender;
        $metropol_details->identity_number = $identity_number;
        $metropol_details->identity_type = $identity_type;
        $metropol_details->dob = $dob;
        $metropol_details->save();

        return response()->json([
            'first_name' => $first_name,
            'last_name' => $last_name,
            'other_names' => $other_names,
            'dob' => $dob
        ]);
    }

    function clientCrbScore(Request $request)
    {
        $user = AppUser::where('id', $request->user_id)
            ->first();

        $id_number = $user->id_number;
//        dd($id_number);


        $metropol = new Metropol($this->PublicKey, $this->PrivateKey);

        $client_crb_score = $metropol->consumerScore($id_number);


//        dd($client_crb_score);

        //TODO check if the response is either an array or an object

//        dd($client_crb_score);
        $score = $client_crb_score->credit_score;
//        dd($score);
        $identity_type = $client_crb_score->identity_type;

        $score_request = new MetropolScoreRequest();
        $score_request->user_id = $request->user_id;
        $score_request->identity_number = $id_number;
        $score_request->identity_type = $identity_type;
        $score_request->credit_score = $score;
        $score_request->save();

        $client_score = [
            'identity_number' => $id_number,
            'identity_type' => $identity_type,
            'credit_score' => $score
        ];

//        dd($client_score);
//        return $client_score;


        $client_metropol_crb = new MetropolCrb();

        $client_metropol_crb->user_id = $user->id;

        $client_metropol_crb->identity_number = $client_score['identity_number'];

        $client_metropol_crb->identity_type = $client_score['identity_type'];

        $client_metropol_crb->credit_score = number_format(floatval($client_score['credit_score']), '0', '.', '');

        $client_metropol_crb->save();
        $score = number_format(floatval($client_score['credit_score']), '0', '.', '');
        if ($score > 0 && $score < 400) {
            return response()->json([
                'code' => 0,
                'message' => 'Metropol score too low for a loan'
            ]);
        } else {

            $loan_limit = $this->setUserLimit($user->id, $score);

            return response()->json([
                'code' => 1,
                'loan_limit' => $loan_limit
            ]);
        }
    }

    public function setUserLimit($user_id, $score)
    {
        $system_set_limit = SystemSetLimit::where('user_id', $user_id)->first();
        if ($score === null) {
            $system_set_limit->amount = 0;
            $system_set_limit->save();
            return $system_set_limit;

        } else if ($score >= 0 && $score < 400) {
            $system_set_limit->amount = 0;
            $system_set_limit->save();
            return 0;
        } else if ($score >= 400 && $score < 501) {
            $system_set_limit->amount = 1000;
            $system_set_limit->save();
            return 1000;
        } else if ($score >= 501 && $score < 601) {
            $system_set_limit->amount = 1500;
            $system_set_limit->save();
            return 1500;
        } else if ($score >= 601 && $score < 701) {
            $system_set_limit->amount = 2000;
            $system_set_limit->save();
            return 2000;
        } else if ($score >= 701 && $score < 801) {
            $system_set_limit->amount = 2500;
            $system_set_limit->save();
            return 2500;
        } else if ($score >= 801 && $score < 901) {
            $system_set_limit->amount = 3000;
            $system_set_limit->save();
            return 3000;
        }
    }
}
