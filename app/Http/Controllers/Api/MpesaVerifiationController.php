<?php

namespace App\Http\Controllers\Api;

use App\AppUser;
use App\CashFlow;
use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use App\Loan;
use App\LoanRepaymentRequest;
use App\MpesaCall;
use App\MpesaCallBack;
use App\MpesaRepaymnt;
use App\MpesaVerification;
use App\PaybillBalance;
use App\Repayment;
use App\StkPushPaymentRequest;
use App\VerificationMpesaCall;
use App\VerificationMpesaPayment;
use App\VerificationStkPushRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;

class MpesaVerifiationController extends Controller
{
    //


    public function registerVerificationUrls()
    {
        //Variables specific to this application

//        $merchant_id = "515657"; //C2B Shortcode/Paybill
        $merchant_id = "600000"; //C2B Shortcode/Paybill

//        $confirmation_url = "https://linq.mobi/api/verification/confirmation";//"https://linq.mobi/api/confirmation"; //Always avoid IP..coz of ssl issues
        $confirmation_url = "https://09db6e78.ngrok.io/api/verification/confirmation";//"https://linq.mobi/api/confirmation"; //Always avoid IP..coz of ssl issues

//        $validation_url = "https://linq.mobi/api/verification/validation"; //"https://linq.mobi/api/validation";
        $validation_url = "https://09db6e78.ngrok.io/api/verification/validation"; //"https://linq.mobi/api/validation";

        $access_token = $this->getVerificationAccessToken();

        //START CURL

//        $url = 'https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl';
        $url = 'https://sandbox.safaricom.co.ke/mpesa/c2b/v1/registerurl';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $access_token)); //setting custom header


//          $curl_post_data = array(
//
//              'ShortCode' => 515657,
//
//              'ResponseType' => 'cancelled',
//
//              'ConfirmationURL' => 'https://23e87f38.ngrok.io/api/confirmation',
//
//              'ValidationURL' => 'https://23e87f38.ngrok.io/api/validation'
//
//          );

//          dd($curl_post_data);

        $curl_post_data = array(

            'ShortCode' => $merchant_id,

            'ResponseType' => 'completed',

            'ConfirmationURL' => $confirmation_url,

            'ValidationURL' => $validation_url

        );


        $data_string = json_encode($curl_post_data);


        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);


        $curl_response = curl_exec($curl);

        print_r($curl_response);

//        echo $curl_response;

    }


    //Lipa na M-Pesa Online Payment - Resource URL


    /**
     * @param Request $request (user id and the amount to be paid)
     * @return false|string
     */

    public function verification_stkPush(Request $request) //From AJAX call/android side, etc....start USSD pay request

    {
//        dd($request);
        $amount = $request->amount;

        $user_id = $request->user_id;

        $user = AppUser::where('id', $user_id)
            ->first();
//        dd($user);

        $phone_paying = $user->phone_number;


        //remove 07 for those that come with it

        if ($this->startsWith($phone_paying, "07")) {

            $pos = strpos($phone_paying, "07");

            if ($pos !== false) {

                $phone_paying = substr_replace($phone_paying, "2547", $pos, 2);

            }


            //$phone_paying=str_replace("0","254",$phone_paying);

        }


        //Validate the inputs

        if ($amount === "" || $phone_paying === "" || $user_id === "" || !$this->startsWith($phone_paying, "2547") ||

            $amount < 1 || $amount > 1 || $user_id < 1 || filter_var($user_id, FILTER_VALIDATE_INT) === false ||

            filter_var($amount, FILTER_VALIDATE_INT) === false) {

            return json_encode(
                [
                    "success" => 0,
                    "message" => "Wrong input"
                ]);

        }


        //Variables specific to this application

//        $merchant_id = "515657"; //C2B Shortcode/Paybill
        $merchant_id = "174379"; //C2B Shortcode/Paybill

//        $callback_url = "https://linq.mobi/api/verification/callback"; //"https://linq.mobi/api/callback";
        $callback_url = "https://09db6e78.ngrok.io/api/verification/callback"; //"https://linq.mobi/api/callback";

        $passkey = "bfb279f9aa9bdbcf158e97dd71a467cd2e0c893059b10f78e6b72ada1ed2c919"; //Ask from Safaricom guys..
//        $passkey = "b0072c3dcd2de0316cb621afb13b42540a00288f6ad2c624552c286a2d13d1a6"; //Ask from Safaricom guys..(live)

        $account_reference =  $phone_paying; //like account number while paying via paybill

        $transaction_description = 'Pay for User:' . $phone_paying;


//        return $loan;
        //LOG the Request. This is done just to keep a record of the online payment calls you have made

        $call = new VerificationMpesaCall();

        $caller = "MPESA_VERIFICATION";

        $ip = $request->ip();

        $content = "Phone: " . $phone_paying . " | Amount: " . $amount;

        $call->content = $content;

        $call->ip = $ip;

        $call->caller = $caller;

        $call->save();


        //Initiate PUSH

        $timestamp = date("YmdHis");

        $password = base64_encode($merchant_id . $passkey . $timestamp); //No more Hashing like before. this is a guideline from Saf

        $access_token = $this->getVerificationAccessToken();


        $curl = curl_init();

//        $endpoint_url = 'https://api.safaricom.co.ke/mpesa/stkpush/v1/processrequest';
        $endpoint_url = 'https://sandbox.safaricom.co.ke/mpesa/stkpush/v1/processrequest';

        curl_setopt($curl, CURLOPT_URL, $endpoint_url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $access_token)); //setting custom header


        $curl_post_data = array(

            'BusinessShortCode' => $merchant_id,

            'Password' => $password,

            'Timestamp' => $timestamp,

            'TransactionType' => 'CustomerPayBillOnline',

            'Amount' => $amount,

            'PartyA' => $phone_paying,

            'PartyB' => $merchant_id,

            'PhoneNumber' => $phone_paying,

            'CallBackURL' => $callback_url,

            'AccountReference' => $account_reference,

            'TransactionDesc' => $transaction_description

        );


        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);
//        dd($curl_response);

        $result = json_decode($curl_response);
//        dd($result);


//        return $curl_response;

        VerificationStkPushRequest::create([
            'user_id' => $user->id,
            'merchant_request_id' => $result->MerchantRequestID,
            'checkout_request_id' => $result->CheckoutRequestID,
            'amount' => $amount,
            'status' => 1
        ]);

        if (array_key_exists("errorCode", $result)) { //Error

            return json_encode(
                [
                    "success" => 0,
                    "message" => "Request Failed"
                ]);

        } else if ("ResponseCode" == 0) { //Success
            //create the pending request...

            return json_encode([
                "success" => 1,
                "message" => "Request Sent Successfully"
            ]);
//            return response()->json(['result'=> $result]);

        } else {

            return json_encode([
                "success" => 0,
                "message" => "Unknown Error, Please Retry"
            ]);

        }

    }


    /*

     * initial settings functions..callback/success/failure?

     * this method receives feedback from Safaricom after request

     * is successfully sent to user. But remember may cancel, low balance, pay etc

    */

    public function verificationCallback(Request $request)
    { //ONLY in STK push requests
//        $body = $request->Body;

//        $myresponse= response()->json(['body'=> $body]);

        $content = json_encode($request->getContent());

        $result_code = $request->Body['stkCallback']['ResultCode'];
        $mercahnt_id = $request->Body['stkCallback']['MerchantRequestID'];
        $checkout_id = $request->Body['stkCallback']['CheckoutRequestID'];

        $identifier = "C2B_CALLBACK";

        $ip = $request->ip();

        $callback = new VerificationMpesaCall();

        $callback->ip = $ip;

        $callback->content = $content;

        $callback->caller = $identifier;

        $callback->save();

        $stk_repayment_request = VerificationStkPushRequest::where('merchant_request_id', $mercahnt_id)
            ->first();

        $user = AppUser::where('id',$stk_repayment_request->user_id)->first();
        $mpesa_verification = MpesaVerification::where('user_id',$user->id)->first();

        if ($result_code == 0) {

            $stk_repayment_request->status = 2;
            $stk_repayment_request->save();
            $item = $request->Body['stkCallback']['CallbackMetadata']['Item'][0]['Value'];
//
            $balance = 0;
            $amount = $request->Body['stkCallback']['CallbackMetadata']['Item'][0]['Value'];
            $trans_id = $request->Body['stkCallback']['CallbackMetadata']['Item'][1]['Value'];
            $time = $request->Body['stkCallback']['CallbackMetadata']['Item'][3]['Value'];


            $mpesa_repayment = new VerificationMpesaPayment();
            $mpesa_repayment->identifier = 'STK_PUSH_VERIFICATION_REPAYMENT';
            $mpesa_repayment->amount = $amount;
            $mpesa_repayment->transaction_id = $trans_id;
            $mpesa_repayment->balance = $balance;
            $mpesa_repayment->msisdn = $user->phone_number;
            $mpesa_repayment->first_name = $user->first_name;
            if($user->second_name != null){
                $mpesa_repayment->middle_name = $user->second_name;
            }
            $mpesa_repayment->last_name = $user->surname;
            $mpesa_repayment->bill_reference = $user->phone_number;
            $mpesa_repayment->time = $time;
            $mpesa_repayment->save();

            if ($mpesa_verification !== null){
                $mpesa_verification->verification_status = 1;
                $mpesa_verification->save();
            }else{

                $mpesa_verification = new MpesaVerification();
                $mpesa_verification->first_name = $user->first_name;
                $mpesa_verification->last_name = $user->surname;
                if($user->second_name != null){
                    $mpesa_verification->middle_name = $user->second_name;
                }
                $mpesa_verification->user_id = $user->id;
                $mpesa_verification->verification_status = 1;
                $mpesa_verification->save();

            }


        } else {

            $stk_repayment_request->status = 3;
            $stk_repayment_request->save();
        }

//        return $request->Body['stkCallback']['ResultCode'];
        return $request;

    }


    /*

     * Validation

     * only accept payments of subscription amount multiples

    */

    public function verificationValidation(Request $request)
    {


        $identifier = "STK_PUSH_VERIFICATION_VALIDATION_RAW";

        $ip = $request->ip();

        $callback = new VerificationMpesaCall();

        $callback->ip = $ip;

        $callback->content = $request->getContent();

        $callback->caller = $identifier;

        $callback->save();


        $content = json_decode($request->getContent());

        //get VALIDATION Details..........

        $msisdn = $content->MSISDN;

        $transaction_amount = $content->TransAmount;

        $transaction_bill_ref_number = $content->BillRefNumber;//user_id

        $first_name = $content->FirstName;

        $middle_name = $content->MiddleName;

        $last_name = $content->LastName;

        $phone_paying ='';

        if ($this->startsWith($msisdn, "2547")) {

            $pos = strpos($msisdn, "2547");

            if ($pos !== false) {

                $phone_paying = substr_replace($msisdn, "07", $pos, 2);

            }

            //$phone_paying=str_replace("0","254",$phone_paying);

        }

        $user = AppUser::where('phone_number',$phone_paying)->first();

        $mpesa_verification = new MpesaVerification();
        $mpesa_verification->first_name = $first_name;
        $mpesa_verification->last_name = $last_name;
        $mpesa_verification->middle_name = $middle_name;
        $mpesa_verification->user_id = $user->id;
        $mpesa_verification->verification_status = 2;
        $mpesa_verification->save();


        //LOG THE DETAILS....

        $identifier = "STK_PUSH_VERIFICATION_VALIDATION_DETAILS";

        $detailed_content = "Names: " . $first_name . ", " . $middle_name . "," . $last_name . " | Phone: " . $msisdn . " | Amount: " . $transaction_amount;


        $callback = new VerificationMpesaCall();

        $ip = $request->ip();

        $callback->ip = $ip;

        $callback->content = $detailed_content;

        $callback->caller = $identifier;

        $callback->save();

        /**
         * check if the names match
         */

        if (strcasecmp($user->first_name, $first_name) == 0 && (strcasecmp($user->surname, $last_name) == 0
            || strcasecmp($user->surname, $middle_name) == 0 || strcasecmp($user->middle_name, $last_name) == 0 ||
            strcasecmp($user->middle_name, $middle_name) == 0)) {

            $result_code = "C2B00014";

            $result_description = "Validation success, the names are matching";

    }else {
            $result_code = "C2B00015";

            $result_description = "Validation failure, the names are not matching";
        }


        return $this->validationResponse($result_code, $result_description);

    }


    public function validationResponse($result_code, $result_description)
    {

        $result = json_encode(["ResultDesc" => $result_description, "ResultCode" => $result_code]);

        $response = new Response();

        $response->headers->set("Content-Type", "application/json; charset=utf-8");

        $response->setContent($result);

        return $response;

    }


    //Confirmation

    public function verificationConfirmation(Request $request)
    {

        $identifier = "C2B_CONFIRMATION";


        $callback = new MpesaCallBack();

        $ip = $request->ip();

        $callback->ip = $ip;

        $callback->content = $request->getContent();

        $callback->caller = $identifier;

        $callback->save();


        $content = json_decode($request->getContent());

//        dd($request);

//        $loan = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
//            ->where('app_users.phone_number', $request->BillRefNumber)
//            ->where('loans.loan_status', 1)
//            ->first();

        $loan = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->select('loans.*','app_users.phone_number as phone_number','app_users.id as user')
            ->where('app_users.phone_number', $request->BillRefNumber)
            ->where(function ($q){
                $q ->where('loans.loan_status', 1)
                    ->orWhere('loans.loan_status', 3)
                    ->orWhere('loans.loan_status',4);
            })
            ->first();

//        update loan repayments request status
        $loan_request = LoanRepaymentRequest::where('loan_id', $loan->id)
            ->where('amount', $request->TransAmount)
            ->where('status',1)
            ->orderby('id', 'desc')
            ->first();

        if ($loan_request == null){
            $lrequest = new LoanRepaymentRequest();
            $lrequest->loan_id = $loan->id;
            $lrequest->amount = $content->TransAmount;
            $lrequest->status = 2;
            $lrequest->save();
        }else{
            $loan_request->status = 2;
            $loan_request->save();
        }

//        dd($callback);

        //get Confirmation Details..........

        $msisdn = $content->MSISDN;

        $transaction_id = $content->TransID;

        $transaction_time = $content->TransTime;

        $org_account_balance = $content->OrgAccountBalance;

        $transaction_amount = $content->TransAmount;

        $transaction_bill_ref_number = $content->BillRefNumber;//user_id, eg "1"

        $first_name = $content->FirstName;

        $middle_name = $content->MiddleName;

        $last_name = $content->LastName;


        //Update Organisation balance table


        CashFlow::create([
            'identifier' => $identifier,
            'amount' => $transaction_amount,
            'paybill_balance' => $org_account_balance
        ]);

        $paybillbalance = new PayBillBalance();
        $paybillbalance->transaction_id = $transaction_id;
        $paybillbalance->transaction_amount = $transaction_amount;
        $paybillbalance->balance_amount = $org_account_balance;
        $paybillbalance->save();

        $paybillbalance->time = $transaction_time;

        /*register the payment by the user
          record the subscription
          1 month=30 days
       */

//        $payment = new Payment();
//
//        $payment->user_id = $loan->user_id;
//
//        $payment->amount = $transaction_amount;
//
//        $payment->phone_number = $msisdn;
//
//
//        $payment->transaction_id = $transaction_id;
//
//        $payment->first_name = $first_name;
//
//        $payment->middle_name = $middle_name . ", " . $last_name;
//
//        $payment->transaction_time = $transaction_time;
//
//        $payment->transaction_bill_ref_number = $transaction_bill_ref_number; //Order Number
//
//        $payment->save();


        $mpesa_repayment = new MpesaRepaymnt();
        $mpesa_repayment->identifier = $identifier;
        $mpesa_repayment->amount = $transaction_amount;
        $mpesa_repayment->msisdn = $msisdn;
        $mpesa_repayment->transaction_id = $transaction_id;
        $mpesa_repayment->first_name = $first_name;
        $mpesa_repayment->middle_name = $middle_name;
        $mpesa_repayment->last_name = $last_name;
        $mpesa_repayment->bill_reference = $transaction_bill_ref_number;
        $mpesa_repayment->balance = $org_account_balance;
        $mpesa_repayment->time = $transaction_time;
        $mpesa_repayment->save();

        $rep = Repayment::where('loan_id', $loan->id)
            ->orderby('id', 'desc')->first();
        $total_loan = $loan->principal_amount + $loan->interest_amount;

        if ($rep == null){
            $prev_balance = $total_loan;
        }else{
            $prev_balance = $rep->balance;
        }

        $result_description = "C2B Payment Transaction $transaction_id result received.";

        $result_code = "0";


        $result = json_encode(["ResultDesc" => $result_description, "ResultCode" => $result_code]);


        $response = new Response();

        $response->headers->set("Content-Type", "text/xml; charset=utf-8");

        $response->setContent($result);

        if ($rep === null) {
            $balance = $total_loan - $transaction_amount;
        } else {
            $balance = $prev_balance - $transaction_amount;
        }

        if ($balance < 0) {

            $phone_number = UniversalMethods::formatPhoneNumber($loan->phone_number);
            $fname = $loan->fname;

            $message = "Hi, "
                . $fname. " Your loan payment of "
                . $transaction_amount . " Ksh has been received, Your loan is now fully settled. Thank you for using LINQ MOBILE ";
            SendSMSTrait::sendSMS($message,"+254".$phone_number);

            return json_encode(["success" => 1, "message" => "Kindly pay the expected balance"]);

        } elseif ($balance == 0) {

            $payment = new Repayment();

            $payment->loan_id = $loan->id;

            $payment->mpesa_repayment_id = $mpesa_repayment->id;

            $payment->amount = $transaction_amount;

            $payment->balance = $balance;
            $payment->save();

            $loan->loan_status = 2;
            $loan->clearance_date = now();
            $loan->save();

            $phone_number = UniversalMethods::formatPhoneNumber($loan->phone_number);
            $fname = $loan->fname;

            $message = "Hi, "
                . $fname. " Your loan payment of "
                . $transaction_amount . " Ksh has been received, Your loan is now fully settled. Thank you for using LINQ MOBILE ";
            SendSMSTrait::sendSMS($message,"+254".$phone_number);

            return json_encode(["success" => 0, "Thank you for paying your loan. Your loan is now completely paid"]);
        } else {

            $payment = new Repayment();

            $payment->loan_id = $loan->id;

            $payment->mpesa_repayment_id = $mpesa_repayment->id;

            $payment->amount = $transaction_amount;
            $payment->balance = $balance;

            $payment->save();

            $phone_number = UniversalMethods::formatPhoneNumber($loan->phone_number);
            $fname = $loan->fname;

            $message = "Hi, "
                . $fname. " Your loan payment of "
                . $transaction_amount . " Ksh has been received. Your outstanding balance is "
                .$balance." KSh.Pay your loan in time to increase your loan limit";
            SendSMSTrait::sendSMS($message,"+254".$phone_number);

            return json_encode(["success" => 2, "message" => "Thank you for paying your loan"]);
        }

    }

    public function getVerificationAccessToken()
    {

        //Variables specific to this application

        $consumer_key = "t86V46LwTBzhBuqM2v8Wqlhof63uagx9"; //Get these two from DARAJA Platform
//        $consumer_key = "9454GBUyZvVM2lLGPZhQjAOPCL9tYphP"; //Get these two from DARAJA Platform(live)

        $consumer_secret = "DC47sjXXlP5MBATB";
//        $consumer_secret = "M06TKXtUk6glMG1s";//live

        $credentials = base64_encode($consumer_key . ":" . $consumer_secret);

        //START CURL

//        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
        $url = 'https://sandbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

//        $url = 'https://sendbox.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';
//        $url = 'https://sendbo.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $credentials));

        curl_setopt($curl, CURLOPT_HEADER, false);//Make it not return headers...true retirns header

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);//MAGIC..

        $curl_response = curl_exec($curl);

//        dd($curl_response);

        $access_token = json_decode($curl_response);

        return $access_token->access_token;

    }

    function startsWith($haystack, $needle)

    {

        $length = strlen($needle);

        return (substr($haystack, 0, $length) === $needle);

    }





}
