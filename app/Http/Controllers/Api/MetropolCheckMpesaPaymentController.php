<?php

namespace App\Http\Controllers\Api;

use App\AdminSetLimit;
use App\AppUser;
use App\CashFlow;
use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use App\Loan;
use App\LoanRequest;
use App\MetropolCrb;
use App\MonthlyLoanTrack;
use App\MpesaCallBack;
use App\Payment;
use App\RepaymentCycleInterest;
use App\UserMetropolScore;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MetropolCheckMpesaPaymentController extends Controller
{

    //Register URLS for validation and Confirmation...used only once as a setup..use postman to fire it up
    public function registerUrls()
    {

        //Variables specific to this application

//        $merchant_id = "600136"; //C2B Shortcode/Paybill 661912(live)
        $merchant_id = "661912"; //C2B Shortcode/Paybill 661912(live)

        $confirmation_url = "https://linq.mobi/api/confirmation"; //Always avoid IP..coz of ssl issues

        $validation_url = "https://linq.mobi/api/validation";

        $access_token = $this->getToken();
//        dd($access_token);

        //START CURL

        $url = 'https://api.safaricom.co.ke/mpesa/c2b/v1/registerurl';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $access_token)); //setting custom header

        $curl_post_data = array(

            'ShortCode' => $merchant_id,

            'ResponseType' => 'completed',

            'ConfirmationURL' => $confirmation_url,

            'ValidationURL' => $validation_url

        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($curl, CURLOPT_POST, true);

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);


        $curl_response = curl_exec($curl);
//        dd($curl_response);

        print_r($curl_response);

//        echo $curl_response;

    }

    //    simulating a B2C transaction
    public function b2cAction(Request $request)
    {
        $app_user = AppUser::where('id', intval($request->appuser_id))->first();
        $phone_to_pay = $app_user->phone_number;
        $user_id = $request->appuser_id;
        $amount = $request->amount;
        $repayment_cycle_id = $request->repayment_cycle_id;
        $metropolscore = MetropolCrb::where('user_id', $request->appuser_id)->first();

        //remove 07 for those that come with it
        if ($this->startsWith($phone_to_pay, "07")) {
            $pos = strpos($phone_to_pay, "07");
            if ($pos !== false) {
                $phone_to_pay = substr_replace($phone_to_pay, "2547", $pos, 2);
            }
        }
//        dd($phone_to_pay);

        //Validate the inputs
        if ($amount === "" || $phone_to_pay === "" || $user_id === "" || !$this->startsWith($phone_to_pay, "2547") ||
            $amount < 1000.0 || $amount > 70000.0 || $user_id < 1 || filter_var($user_id, FILTER_VALIDATE_FLOAT) === false ||
            filter_var($amount, FILTER_VALIDATE_FLOAT) === false) {

            return response()->json([
                "ConversationID" => "NONE",
                "OriginatorConversationID" => "NONE",
                "ResponseCode" => "3",
                "ResponseDescription" => "Wrong input"
            ], 200);

        }

        $user_loanrequestcount = LoanRequest::where('user_id', $user_id)->count();
        $repayment_cycle = RepaymentCycleInterest::where('id', $repayment_cycle_id)->first();
        $useradminsetloanlimit =AdminSetLimit::where('user_id',$user_id)->first();

//        dd($useradminsetloanlimit);
//        dd($repayment_cycle);

        $interest_amount = $amount * $repayment_cycle->interest_rate;

        if ($user_loanrequestcount <= 0) {

            $loanrequest = new LoanRequest();
            $loanrequest['user_id'] = $user_id;
            $loanrequest['repayment_cycle_interest_id'] = $repayment_cycle_id;
            $loanrequest['interest_amount'] = $interest_amount;
            $loanrequest['principal_amount'] = $amount;
            $loanrequest['loan_request_status'] = 1;
            $loanrequest->save();

            $loanrequestid = $loanrequest->id;

            //skip checking the metropol score when admin set limit for that user is 1-active

            if (!$useradminsetloanlimit == null){

                if($useradminsetloanlimit->status == true){

                    $loanrequest = LoanRequest::findOrFail($loanrequestid);
                    $loanrequest['loan_request_status'] = 2;
                    $loanrequest->save();

                    $this->transactb2c($amount, $user_id, $phone_to_pay, $loanrequest->id);

                }else{

                    if ($metropolscore->credit_score >= 401) {

                        $loanrequest = LoanRequest::findOrFail($loanrequestid);
                        $loanrequest['loan_request_status'] = 2;
                        $loanrequest->save();

                        $this->transactb2c($amount, $user_id, $phone_to_pay, $loanrequest->id);

                    } else {

                        $loanrequest = LoanRequest::findOrFail($loanrequestid);
                        $loanrequest['loan_request_status'] = 3;
                        $loanrequest->save();

                        return response()->json([
                            "ConversationID" => "NONE",
                            "OriginatorConversationID" => "NONE",
                            "ResponseCode" => "1",
                            "ResponseDescription" => "Loan Request Rejected Your Credit Score is low"
                        ], 200);

                    }

                }
            }else{

                if ($metropolscore->credit_score >= 401) {

                    $loanrequest = LoanRequest::findOrFail($loanrequestid);
                    $loanrequest['loan_request_status'] = 2;
                    $loanrequest->save();

                    $this->transactb2c($amount, $user_id, $phone_to_pay, $loanrequest->id);

                } else {

                    $loanrequest = LoanRequest::findOrFail($loanrequestid);
                    $loanrequest['loan_request_status'] = 3;
                    $loanrequest->save();

                    return response()->json([
                        "ConversationID" => "NONE",
                        "OriginatorConversationID" => "NONE",
                        "ResponseCode" => "1",
                        "ResponseDescription" => "Loan Request Rejected Your Credit Score is low"
                    ], 200);

                }

            }

        } else {

            $loanrequest = new LoanRequest();
            $loanrequest['user_id'] = $user_id;
            $loanrequest['repayment_cycle_interest_id'] = $repayment_cycle_id;
            $loanrequest['interest_amount'] = $interest_amount;
            $loanrequest['principal_amount'] = $amount;
            $loanrequest['loan_request_status'] = 1;
            $loanrequest->save();

            $loanrequestedid = $loanrequest->id;

            //skip checking the metropol score when admin set limit for that user is 1-active
            if (!$useradminsetloanlimit == null){
                if($useradminsetloanlimit->status == true){

                    $loanrequest = LoanRequest::findOrFail($loanrequestedid);
                    $loanrequest['loan_request_status'] = 2;
                    $loanrequest->save();

                    $this->transactb2c($amount, $user_id, $phone_to_pay, $loanrequest->id);

                }else{

                    if ($metropolscore->credit_score >= 401) {

                        $activeloan = Loan::where('user_id', $user_id)
                            ->where(function ($q) {
                                $q->where('loan_status', 1)
                                    ->orWhere('loan_status',3)
                                    ->orWhere('loan_status',4);
                            })
                            ->orderby('id', 'desc')
                            ->get();

                        if ($activeloan->count()) {

                            $loanrequest = LoanRequest::findOrFail($loanrequestedid);
                            $loanrequest['loan_request_status'] = 3;
                            $loanrequest->save();

                            return response()->json([
                                "ConversationID" => "NONE",
                                "OriginatorConversationID" => "NONE",
                                "ResponseCode" => "2",
                                "ResponseDescription" => "Loan Request Rejected You Have an active loan, please clear first"
                            ], 200);

                        } else {

                            $loanrequest = LoanRequest::findOrFail($loanrequest->id);
                            $loanrequest['loan_request_status'] = 2;
                            $loanrequest->save();

                            $this->transactb2c($amount, $user_id, $phone_to_pay, $loanrequest->id);

                        }

                    } else {

                        $loanrequest = LoanRequest::findOrFail($loanrequestedid);
                        $loanrequest['loan_request_status'] = 3;
                        $loanrequest->save();

                        return response()->json([
                            "ConversationID" => "NONE",
                            "OriginatorConversationID" => "NONE",
                            "ResponseCode" => "1",
                            "ResponseDescription" => "Loan Request Rejected Your Credit Score is low"
                        ], 200);

                    }

                }
            }else{

                if ($metropolscore->credit_score >= 401) {

                    $activeloan = Loan::where('user_id', $user_id)
                        ->where(function ($q) {
                            $q->where('loan_status', 1)
                                ->orWhere('loan_status',3)
                                ->orWhere('loan_status',4);
                        })
                        ->orderby('id', 'desc')
                        ->get();

                    if ($activeloan->count()) {

                        $loanrequest = LoanRequest::findOrFail($loanrequestedid);
                        $loanrequest['loan_request_status'] = 3;
                        $loanrequest->save();

                        return response()->json([
                            "ConversationID" => "NONE",
                            "OriginatorConversationID" => "NONE",
                            "ResponseCode" => "2",
                            "ResponseDescription" => "Loan Request Rejected You Have an active loan, please clear first"
                        ], 200);

                    } else {

                        $loanrequest = LoanRequest::findOrFail($loanrequest->id);
                        $loanrequest['loan_request_status'] = 2;
                        $loanrequest->save();

                        $this->transactb2c($amount, $user_id, $phone_to_pay, $loanrequest->id);

                    }

                } else {

                    $loanrequest = LoanRequest::findOrFail($loanrequestedid);
                    $loanrequest['loan_request_status'] = 3;
                    $loanrequest->save();

                    return response()->json([
                        "ConversationID" => "NONE",
                        "OriginatorConversationID" => "NONE",
                        "ResponseCode" => "1",
                        "ResponseDescription" => "Loan Request Rejected Your Credit Score is low"
                    ], 200);

                }

            }

        }

        return null;

    }


    /**
     *
     * @param $amount
     * @param $user_id
     * @param $phone_to_pay
     * @param $loan_request_id
     */
    public function transactb2c($amount, $user_id, $phone_to_pay, $loan_request_id)
    {

//        dd($amount);

//        $partya = "600136"; //B2C Shortcode/Paybill 661912(live)
        $partya = "661912"; //B2C Shortcode/Paybill 661912(live)
        $remarks = 'Issue a loan to User:' . $user_id;
        $url = 'https://api.safaricom.co.ke/mpesa/b2c/v1/paymentrequest';
        $token = $this->getToken();
        $usr = AppUser::where('app_users.id', $user_id)->first();

        $partyB = $this->getPartyB($usr->phone_number);
//        dd($partyB);


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $token)); //setting custom header

        $securityCredential = $this->generateSecurityCredential('LINQAPI', 'Th1s1sLINQ#');
//        dd($securityCredential);
        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'InitiatorName' => 'LINQAPI',
//            'SecurityCredential' => 'HTBeKeL7PGofUf4SCG/fnXBSsouY8G353JeuJevl6PGH0YbWelGB4YQYzyJhOQY/Htl35xhABzIHtlyp1Yl8lDqKVqGCefoGjpTt/+XTvsvLQZvixOCuvAS6mSgfJAR3zyLJzJ9nN7siEy4lVdHxkQXwg05uoA64sZyz/5oA94dotM5EFe3SR/u4xrMe/53Br0MoDPkyYX/oz9wl82vjCloip7aPt0Y6INPXdlWaR95CzOVuPvILOffzZxlYpIAt8wjaBdngMZ6SDI4XB6ck+ONLtZa9JR1f5yhZDKa+tL9J8D0XxT71gzpN+oG/KLUt+b3hxxpfxyxPullqGsxZOw',
            'SecurityCredential' => $securityCredential,
            'CommandID' => 'BusinessPayment',
            'Amount' => intval($amount),
            'PartyA' => $partya,
//            'PartyB' => '254708374149',//$phone_to_pay
            'PartyB' => $partyB,//$phone_to_pay
            'Remarks' => $remarks,
            'QueueTimeOutURL' => 'https://linq.mobi/api/for/b2ctimeout',
            'ResultURL' => 'https://linq.mobi/api/for/b2csuccess',
            'Occasion' => ''
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);

//        dd($curl_response);

        $json = json_decode($curl_response, true);
//        dd($json);
        $conversation_id = $json['ConversationID'];
        $originatorconversation_id = $json['OriginatorConversationID'];

        $loan_request = LoanRequest::where('id', $loan_request_id)->first();
        $loan_request['conversation_id'] = $conversation_id;
        $loan_request['originator_conversation_id'] = $originatorconversation_id;
        $loan_request->save();

        print_r($curl_response);

//        echo $curl_response;

    }

    public function timeout(Request $request)
    {

    }

    public function success(Request $request)
    {
        //register the callback
//        dd($request->all());

        MpesaCallBack::create([

            'ip' => $request->ip(),

            'caller' => "B2C_SUCCESS_RAW",

            'content' => $request->getContent(),

        ]);

        $originatorConversationID = $request->Result['OriginatorConversationID'];
        $conversationID = $request->Result['ConversationID'];

        if ($request->Result['ResultCode'] == 0) {

            $transactionamount = $request->Result['ResultParameters']['ResultParameter'][0]['Value'];
            $transactionreceipt = $request->Result['ResultParameters']['ResultParameter'][1]['Value'];
            $B2CRecipientisregisteredcustomer = $request->Result['ResultParameters']['ResultParameter'][2]['Value'];
            $B2CChargespaidaccountavailablefunds = $request->Result['ResultParameters']['ResultParameter'][3]['Value'];
            $receiverpartypublicname = $request->Result['ResultParameters']['ResultParameter'][4]['Value'];
            $transactioncompleteddatetime = $request->Result['ResultParameters']['ResultParameter'][5]['Value'];
            $B2CUtilityaccountavailablefunds = $request->Result['ResultParameters']['ResultParameter'][6]['Value'];
            $B2CWorkingaccountavailablefunds = $request->Result['ResultParameters']['ResultParameter'][7]['Value'];

            $loanrequest = LoanRequest::where('conversation_id', $conversationID)
                ->where('originator_conversation_id', $originatorConversationID)
                ->first();

            $repaymentcycleinterest = RepaymentCycleInterest::where('id', $loanrequest->repayment_cycle_interest_id)->first();
            $repaymentcycle = $repaymentcycleinterest->repayment_cycle;
            $app_user = AppUser::where('id', $loanrequest->user_id)->first();

            $today = Carbon::now();

            if ('14 days' === $repaymentcycle) {

                $duedate = Carbon::parse($today)->addDays(14);

                $loan = new Loan();
                $loan['user_id'] = $loanrequest->user_id;
                $loan['loan_request_id'] = $loanrequest->id;
                $loan['repayment_cycle_interest_id'] = $loanrequest->repayment_cycle_interest_id;
                $loan['interest_amount'] = $loanrequest->interest_amount;
                $loan['principal_amount'] = $loanrequest->principal_amount;
                $loan['loan_status'] = 1;
                $loan['start_at'] = $today;
                $loan['due_date'] = $duedate;
                $loan->save();

                $payment = new Payment();
                $payment['first_name'] = $app_user->first_name;
                $payment['middle_name'] = $app_user->surname;
                $payment['amount'] = $loan->principal_amount;
                $payment['user_id'] = $loanrequest->user_id;
                $payment['phone_number'] = $receiverpartypublicname;
                $payment['transaction_id'] = $transactionreceipt;
                $payment['transaction_time'] = $transactioncompleteddatetime;
                $payment['transaction_bill_ref_number'] = $originatorConversationID;
                $payment->save();

                $cashflow = new CashFlow();
                $cashflow['identifier'] = 'B2C_SUCCESS';
                $cashflow['amount'] = $transactionamount;
                $cashflow['paybill_balance'] = $B2CWorkingaccountavailablefunds;
                $cashflow->save();

                $phone_number = UniversalMethods::formatPhoneNumber($app_user->phone_number);

                $totalloan = intval($loan->interest_amount) + intval($loan->principal_amount);

                $message = "Hi, "
                    . $app_user->first_name . " A loan of "
                    . $transactionamount . " Ksh has been deposited to your M-pesa, Your due loan is "
                    . $totalloan . " Ksh payable on or before "
                    . $duedate;

                $updatedueonloan = Loan::findorfail($loan->id);
                $updatedueonloan['due_date'] = $duedate;
                $updatedueonloan->update();

                //send code to user
                $result = SendSMSTrait::sendSMS($message, "+254" . $phone_number);

                return response()->json([
                    'data' => $result,
                    'message' => "loan disbursed notification sent",
                ], 200);

            } elseif ('30 days' === $repaymentcycle) {

                $duedate = Carbon::parse($today)->addDays(30);

                $loan = new Loan();
                $loan['user_id'] = $loanrequest->user_id;
                $loan['loan_request_id'] = $loanrequest->id;
                $loan['repayment_cycle_interest_id'] = $loanrequest->repayment_cycle_interest_id;
                $loan['interest_amount'] = $loanrequest->interest_amount;
                $loan['principal_amount'] = $loanrequest->principal_amount;
                $loan['loan_status'] = 1;
                $loan['start_at'] = $today;
                $loan['due_date'] = $duedate;
                $loan->save();

                $payment = new Payment();
                $payment['first_name'] = $app_user->first_name;
                $payment['middle_name'] = $app_user->surname;
                $payment['amount'] = $loan->principal_amount;
                $payment['user_id'] = $loanrequest->user_id;
                $payment['phone_number'] = $receiverpartypublicname;
                $payment['transaction_id'] = $transactionreceipt;
                $payment['transaction_time'] = $transactioncompleteddatetime;
                $payment['transaction_bill_ref_number'] = $originatorConversationID;
                $payment->save();

                $cashflow = new CashFlow();
                $cashflow['identifier'] = 'B2C_SUCCESS';
                $cashflow['amount'] = $transactionamount;
                $cashflow['paybill_balance'] = $B2CWorkingaccountavailablefunds;
                $cashflow->save();

                $phone_number = UniversalMethods::formatPhoneNumber($app_user->phone_number);

                $totalloan = intval($loan->interest_amount) + intval($loan->principal_amount);

                $message = "Hi, "
                    . $app_user->first_name . " A loan of "
                    . $transactionamount . " Ksh has been deposited to your M-pesa, Your due loan is "
                    . $totalloan . " Ksh payable on or before "
                    . $duedate;

                $updatedueonloan = Loan::findorfail($loan->id);
                $updatedueonloan['due_date'] = $duedate;
                $updatedueonloan->update();

                //send code to user
                $result = SendSMSTrait::sendSMS($message, "+254" . $phone_number);

                return response()->json([
                    'data' => $result,
                    'message' => "loan disbursed notification sent",
                ], 200);
            }elseif('90 days' === $repaymentcycle){

                $duedate = Carbon::parse($today)->addDays(90);
                $month_one_start = $today;
                $month_one_end = $today->addDays(30);
                $month_two_start = $month_one_end;
                $month_two_end = $month_two_start->addDays(30);
                $month_three_start = $month_two_end;
                $month_three_end = $duedate;

                $loan = new Loan();
                $loan['user_id'] = $loanrequest->user_id;
                $loan['loan_request_id'] = $loanrequest->id;
                $loan['repayment_cycle_interest_id'] = $loanrequest->repayment_cycle_interest_id;
                $loan['interest_amount'] = $loanrequest->interest_amount;
                $loan['principal_amount'] = $loanrequest->principal_amount;
                $loan['loan_status'] = 1;
                $loan['start_at'] = $today;
                $loan['due_date'] = $duedate;
                $loan->save();

                $payment = new Payment();
                $payment['first_name'] = $app_user->first_name;
                $payment['middle_name'] = $app_user->surname;
                $payment['amount'] = $loan->principal_amount;
                $payment['user_id'] = $loanrequest->user_id;
                $payment['phone_number'] = $receiverpartypublicname;
                $payment['transaction_id'] = $transactionreceipt;
                $payment['transaction_time'] = $transactioncompleteddatetime;
                $payment['transaction_bill_ref_number'] = $originatorConversationID;
                $payment->save();

                $m1 = new MonthlyLoanTrack();
                $m1->loan_id = $loan->id;
                $m1->amount = ceil(($loan->principal_amount+$loan->interest_amount)/3);
                $m1->month = 1;
                $m1->status = 1;
                $m1->start_at = $month_one_start;
                $m1->due_date = $month_one_end;
                $m1->save();

                $m2 = new MonthlyLoanTrack();
                $m2->loan_id = $loan->id;
                $m2->amount = ceil(($loan->principal_amount+$loan->interest_amount)/3);
                $m2->month = 2;
                $m2->status = 1;
                $m2->start_at = $month_two_start;
                $m2->due_date = $month_two_end;
                $m2->save();

                $m3 = new MonthlyLoanTrack();
                $m3->loan_id = $loan->id;
                $m3->amount = ceil(($loan->principal_amount+$loan->interest_amount)/3);
                $m3->month = 3;
                $m3->status = 1;
                $m3->start_at = $month_three_start;
                $m3->due_date = $month_three_end;
                $m3->save();

                $cashflow = new CashFlow();
                $cashflow['identifier'] = 'B2C_SUCCESS';
                $cashflow['amount'] = $transactionamount;
                $cashflow['paybill_balance'] = $B2CWorkingaccountavailablefunds;
                $cashflow->save();

                $phone_number = UniversalMethods::formatPhoneNumber($app_user->phone_number);

                $totalloan = intval($loan->interest_amount) + intval($loan->principal_amount);

                $message = "Hi, "
                    . $app_user->first_name . " A loan of "
                    . $transactionamount . " Ksh has been deposited to your M-pesa, Your due loan is "
                    . $totalloan . " Ksh payable on or before "
                    . $duedate;

                $updatedueonloan = Loan::findorfail($loan->id);
                $updatedueonloan['due_date'] = $duedate;
                $updatedueonloan->update();

                //send code to user
                $result = SendSMSTrait::sendSMS($message, "+254" . $phone_number);

                return response()->json([
                    'data' => $result,
                    'message' => "loan disbursed notification sent",
                ], 200);

            }
            elseif('60 days' === $repaymentcycle){

                $duedate = Carbon::parse($today)->addDays(60);
                $month_one_start = $today;
                $month_one_end = $today->addDays(30);
                $month_two_start = $month_one_end;
                $month_two_end = $duedate;

                $loan = new Loan();
                $loan['user_id'] = $loanrequest->user_id;
                $loan['loan_request_id'] = $loanrequest->id;
                $loan['repayment_cycle_interest_id'] = $loanrequest->repayment_cycle_interest_id;
                $loan['interest_amount'] = $loanrequest->interest_amount;
                $loan['principal_amount'] = $loanrequest->principal_amount;
                $loan['loan_status'] = 1;
                $loan['start_at'] = $today;
                $loan['due_date'] = $duedate;
                $loan->save();

                $payment = new Payment();
                $payment['first_name'] = $app_user->first_name;
                $payment['middle_name'] = $app_user->surname;
                $payment['amount'] = $loan->principal_amount;
                $payment['user_id'] = $loanrequest->user_id;
                $payment['phone_number'] = $receiverpartypublicname;
                $payment['transaction_id'] = $transactionreceipt;
                $payment['transaction_time'] = $transactioncompleteddatetime;
                $payment['transaction_bill_ref_number'] = $originatorConversationID;
                $payment->save();

                $m1 = new MonthlyLoanTrack();
                $m1->loan_id = $loan->id;
                $m1->amount = ceil(($loan->principal_amount+$loan->interest_amount)/2);
                $m1->month = 1;
                $m1->status = 1;
                $m1->start_at = $month_one_start;
                $m1->due_date = $month_one_end;
                $m1->save();

                $m2 = new MonthlyLoanTrack();
                $m2->loan_id = $loan->id;
                $m2->amount = ceil(($loan->principal_amount+$loan->interest_amount)/2);
                $m2->month = 2;
                $m2->status = 1;
                $m2->start_at = $month_two_start;
                $m2->due_date = $month_two_end;
                $m2->save();

                $cashflow = new CashFlow();
                $cashflow['identifier'] = 'B2C_SUCCESS';
                $cashflow['amount'] = $transactionamount;
                $cashflow['paybill_balance'] = $B2CWorkingaccountavailablefunds;
                $cashflow->save();

                $phone_number = UniversalMethods::formatPhoneNumber($app_user->phone_number);

                $totalloan = intval($loan->interest_amount) + intval($loan->principal_amount);

                $message = "Hi, "
                    . $app_user->first_name . " A loan of "
                    . $transactionamount . " Ksh has been deposited to your M-pesa, Your due loan is "
                    . $totalloan . " Ksh payable on or before "
                    . $duedate;

                $updatedueonloan = Loan::findorfail($loan->id);
                $updatedueonloan['due_date'] = $duedate;
                $updatedueonloan->update();

                //send code to user
                $result = SendSMSTrait::sendSMS($message, "+254" . $phone_number);

                return response()->json([
                    'data' => $result,
                    'message' => "loan disbursed notification sent",
                ], 200);

            }else{

                $parts = explode(" ",$repaymentcycle);
                $daysToAdd = intval($parts[0]);

                $duedate = Carbon::parse($today)->addDays($daysToAdd);

                $loan = new Loan();
                $loan['user_id'] = $loanrequest->user_id;
                $loan['loan_request_id'] = $loanrequest->id;
                $loan['repayment_cycle_interest_id'] = $loanrequest->repayment_cycle_interest_id;
                $loan['interest_amount'] = $loanrequest->interest_amount;
                $loan['principal_amount'] = $loanrequest->principal_amount;
                $loan['loan_status'] = 1;
                $loan['start_at'] = $today;
                $loan['due_date'] = $duedate;
                $loan->save();

                $payment = new Payment();
                $payment['first_name'] = $app_user->first_name;
                $payment['middle_name'] = $app_user->surname;
                $payment['amount'] = $loan->principal_amount;
                $payment['user_id'] = $loanrequest->user_id;
                $payment['phone_number'] = $receiverpartypublicname;
                $payment['transaction_id'] = $transactionreceipt;
                $payment['transaction_time'] = $transactioncompleteddatetime;
                $payment['transaction_bill_ref_number'] = $originatorConversationID;
                $payment->save();

                $cashflow = new CashFlow();
                $cashflow['identifier'] = 'B2C_SUCCESS';
                $cashflow['amount'] = $transactionamount;
                $cashflow['paybill_balance'] = $B2CWorkingaccountavailablefunds;
                $cashflow->save();

                $phone_number = UniversalMethods::formatPhoneNumber($app_user->phone_number);

                $totalloan = intval($loan->interest_amount) + intval($loan->principal_amount);

                $message = "Hi, "
                    . $app_user->first_name . " A loan of "
                    . $transactionamount . " Ksh has been deposited to your M-pesa, Your due loan is "
                    . $totalloan . " Ksh payable on or before "
                    . $duedate;

                $updatedueonloan = Loan::findorfail($loan->id);
                $updatedueonloan['due_date'] = $duedate;
                $updatedueonloan->update();

                //send code to user
                $result = SendSMSTrait::sendSMS($message, "+254" . $phone_number);

                return response()->json([
                    'data' => $result,
                    'message' => "loan disbursed notification sent",
                ], 200);
            }

        } else {

            $loanrequest = LoanRequest::where('conversation_id', $conversationID)
                ->where('originator_conversation_id', $originatorConversationID)
                ->first();
            $app_user = AppUser::where('id', $loanrequest->user_id)->first();

            $phone_number = UniversalMethods::formatPhoneNumber($app_user->phone_number);

            $message = "Sorry " . $app_user->first_name . ", your Loan Request Failed, Unknown Error occurred please try again later";

            //send code to user
            $result = SendSMSTrait::sendSMS($message, "+254" . $phone_number);

            return response()->json([
                'data' => $result,
                'message' => "loan not disbursed notification sent",
            ], 200);

        }

    }

    public function getToken()
    {

        //Variables specific to this application

//        $consumer_key = "bGaRkyjEjG8R8HUjMTgQSdJdrZOonxjl"; //Get these two from DARAJA Platform
        $consumer_key = "OkGWiolxUAB3B8NdYBZSw4jZsALZt6jM"; //Get these two from DARAJA Platform

//        $consumer_secret = "vEIPAkXhhEUnsfev";
        $consumer_secret = "lsNXp4FfT2Cf5s6X";

        $credentials = base64_encode($consumer_key . ":" . $consumer_secret);

        //START CURL

        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $credentials));

        curl_setopt($curl, CURLOPT_HEADER, false);//Make it not return headers...true retirns header

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);//MAGIC..

        $curl_response = curl_exec($curl);

//        dd($curl_response);

        $access_token = json_decode($curl_response);

        return $access_token->access_token;

    }

    function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public function generateSecurityCredential(string $username, string $password)
    {
        $file = fopen("/etc/ssl/mpesa/mpesapublickey.cer", 'r'); // This is to be downloaded from mpesa docs and pasted here with this name
        $public_key = fread($file, 8192);
        fclose($file);

        openssl_public_encrypt($password, $encrypted_string, $public_key, OPENSSL_PKCS1_PADDING);
        return (base64_encode($encrypted_string));
    }

    public function getPartyB($phone_number)
    {
        // Replace numbers starting with 07 or +254 with 254 due to what safaricom expects
        if (substr($phone_number, 0, strlen("07")) == "07") {
            $phone_number = "2547" . substr($phone_number, strlen("07"));
        } elseif (substr($phone_number, 0, strlen("+2547")) == "+2547") {
            $phone_number = "2547" . substr($phone_number, strlen("2547"));
        }
        return $phone_number;

    }

}
