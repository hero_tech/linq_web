<?php

namespace App\Http\Controllers\Api;

use App\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FAQController extends Controller
{
    public function getFAQs()
    {
        $faqs = Faq::all();
        return response()->json($faqs);
    }
}
