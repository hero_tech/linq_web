<?php

namespace App\Http\Controllers\Api;

use App\Loan;
use App\Repayment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanHistoryController extends Controller
{
    public function getUserLoansHistory(Request $request)
    {
        $app_user_id = $request->appuser_id;

        $loanhistory = Loan::select(\DB::raw('loans.*, SUM(repayments.amount) as repayment'))
            ->leftJoin('repayments', 'repayments.loan_id', '=', 'loans.id')
            ->Join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->where('app_users.id', '=',$app_user_id)
            ->groupBy('loans.id')
            ->orderby('loans.id','desc')
            ->get();

        return response()->json($loanhistory);

    }
    public function getUserLoanDetail(Request $request)
    {
        $loanid = $request->loan_id;
        $loan = Loan::where('id',$loanid)
            ->first();

        $loanrepayments = Repayment::join('loans','repayments.loan_id','=','loans.id')
            ->where('loans.id',$loanid)
            ->select('repayments.*')
            ->orderby('repayments.id','desc')
            ->get();

        $loandetailed = ([
            'loan'=> $loan,
            'repayments' => $loanrepayments,
        ]);

        return response()->json($loandetailed);

    }
}
