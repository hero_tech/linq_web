<?php

namespace App\Http\Controllers\Api;

use App\RepaymentCycleInterest;
use App\Http\Controllers\Controller;

class RepaymentCycleController extends Controller
{
    public function getRepaymentCycles()
    {
        $repaymentcycle = RepaymentCycleInterest::where('id','!=',4)->orderby('id','desc')->get();
        return response()->json($repaymentcycle);
    }
}
