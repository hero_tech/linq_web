<?php

namespace App\Http\Controllers\Api;

use App\RefferalCode;
use App\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegionController extends Controller
{
    public function getRegions(Request $request)
    {
        $country_id = $request->country_id;
        $regions = Region::where('country_id',$country_id)->get();
        return response()->json($regions);
    }

    public function getReferralCode()
    {
        $regions = RefferalCode::all();
        return response()->json($regions);
    }

}
