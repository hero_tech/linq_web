<?php

namespace App\Http\Controllers\Api;

use App\AppUser;
use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use App\RefferalCode;
use App\SystemSetLimit;
use App\UserVarificationCode;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AppUserController extends Controller
{

    public $successStatus = 200;

    /**
     * login api
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */

    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|digits:10',
            'pin' => 'required|min:4|max:8',
        ]);

        $data = ([
            'id' => 0,
            'region_id' => 0,
            'first_name' => "NONE",
            'second_name' => "NONE",
            'surname' => "NONE",
            'id_number' => "NONE",
            'phone_number' => $request->phone_number,
            'email' => "NONE",
            'date_of_birth' => "0000-00-00 00:00:00",
            'occupation' => "NONE",
            'status' => 0,
            'created_at' => "0000-00-00 00:00:00",
            'updated_at' => "0000-00-00 00:00:00",
        ]);

        if ($validator->fails()) {

            return response()->json([
                'message' => UniversalMethods::validationErrorsToString($validator->errors()),
                'token' => "NONE",
                'data' => $data,
                'status' => 0
            ], 200);

        } else {

            if ($app_user = AppUser::where('phone_number', $request->phone_number)->first()) {
                if (Hash::check($request->get('pin'), $app_user->pin)) {
                    if ($app_user->status == 1 || $app_user->status == 3) {
                        $token = $app_user->createToken('Token')->accessToken;

                        return response()->json([
                            'message' => 'logged in successfully',
                            'token' => $token,
                            'data' => $app_user,
                            'status' => 1
                        ], 200);

                    } else {
                        return response()->json([
                            'message' => 'Your account is disabled, please contact the System administrator',
                            'token' => "NONE",
                            'data' => $data,
                            'status' => 0
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'message' => 'invalid login pin',
                        'token' => "NONE",
                        'data' => $data,
                        'status' => 0
                    ], 200);
                }

            } else {
                return response()->json([
                    'message' => 'could not find this Account!, please verify that your phone number is correct',
                    'token' => "NONE",
                    'data' => $data,
                    'status' => 0
                ], 200);
            }

        }

    }

    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'surname' => 'required',
            'region_id' => 'required',
            'phone_number' => 'required|digits:10|unique:app_users',
            'occupation' => 'required',
            'id_number' => 'required|digits_between:6,9|numeric|unique:app_users',
            'date_of_birth' => 'required',
            'email' => 'required|email|unique:app_users',
            'pin' => 'required|min:4',
            'referral_code' => 'required',
        ]);

        $data = ([
            'id' => 0,
            'region_id' => intval($request->region_id),
            'first_name' => $request->first_name,
            'second_name' => $request->second_name,
            'surname' => $request->surname,
            'referral_code' => $request->referral_code,
            'id_number' => $request->id_number,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'date_of_birth' => Carbon::parse($request['date_of_birth'])->format('Y-m-d'),
            'occupation' => $request->occupation,
            'status' => 0,
            'created_at' => "0000-00-00 00:00:00",
            'updated_at' => "0000-00-00 00:00:00",
        ]);

        $code = RefferalCode::where('code', $request->referral_code)->first();

        if ($code == null) {
            return response()->json([
                'message' => 'You have entered a wrong referral code',
                'token' => "NONE",
                'data' => $data,
                'status' => 1
            ], 200);
        } else {
            if ($validator->fails()) {

                return response()->json([
                    'message' => UniversalMethods::validationErrorsToString($validator->errors()),
                    'token' => "NONE",
                    'data' => $data,
                    'status' => 1
                ], 200);

            } else {

                $input = $request->all();
                $input['pin'] = Hash::make($input['pin']);
                $input['date_of_birth'] = Carbon::parse($input['date_of_birth']);
                $user = AppUser::create($input);
                $token = $user->createToken('Token')->accessToken;
                $app_user = AppUser::findOrFail($user->id);

                $systemsetlimit = new SystemSetLimit();
                $systemsetlimit['user_id'] = $user->id;
                $systemsetlimit['amount'] = 0;
                $systemsetlimit['status'] = 1;
                $systemsetlimit->save();

                return response()->json([
                    'message' => 'registered successfully',
                    'token' => $token,
                    'data' => $app_user,
                    'status' => 1
                ], 200);

            }
        }

    }

    public function logout(Request $request)
    {
        if (Auth::check()) {
            $request->user()->token()->revoke();
            return response()->json([
                'message' => 'Successfully logged out'
            ], 200);
        } else {
            return response()->json([
                'message' => 'You are not logged in'
            ], 200);
        }

    }

    public function requestVerification(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|digits:10',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'data' => false,
                'message' => UniversalMethods::validationErrorsToString($validator->errors()),
            ], 200);

        } else {

            try {
                $phoneNumber = $request->phone_number;
                if ($request->has('phone_number')) {

                    $gen_code = mt_rand(100000, 999999);

                    $phone_number = UniversalMethods::formatPhoneNumber($phoneNumber);

                    $new_code = new UserVarificationCode();
                    $new_code->phone_number = $phoneNumber;
                    $new_code->code = $gen_code;
                    $new_code->save();

                    $message = "<#> registration code is: " . $gen_code . " Linq Mobile Receipt : nqftCpkSBIX";
                    //$message= "Hi, please enter the following verification code to complete registration code-".$gen_code;
                    //send code to user
                    $result = SendSMSTrait::sendSMS($message, "+254" . $phone_number);

                    return response()->json([
                        'data' => $result,
                        'message' => "we have sent code to your phone number, please enter to proceed",
                    ], 200);

                } else {
                    return response()->json([
                        'data' => false,
                        'message' => 'Please provide your Phone Number',
                    ], 200);
                }
            } catch (\Exception $exception) {
                return response()->json([
                    'data' => false,
                    'message' => 'verification request failed',
                ], 200);
            }

        }

    }

    public function codeVerification(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|digits:10',
            'code' => 'required|integer|digits:6',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'message' => UniversalMethods::validationErrorsToString($validator->errors()),
            ], 200);

        } else {

            try {
                $phoneNumber = $request->phone_number;
                $code = $request->code;

                $uservarificationcode = UserVarificationCode::where('phone_number', '=', $phoneNumber)->latest()->first();

                if ($uservarificationcode->code == $code) {
                    return response()->json([
                        'message' => "code verified successfully"
                    ], 200);
                } else {
                    return response()->json([
                        'message' => 'Failed to verify, your code mismatched'
                    ], 200);
                }
            } catch (\Exception $exception) {
                return response()->json([
                    'message' => 'code verification failed'
                ], 200);
            }

        }

    }


    public function requestPinReset(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|digits:10',
        ]);

        if ($validator->fails()) {

            return response()->json([
                'data' => false,
                'message' => UniversalMethods::validationErrorsToString($validator->errors()),
            ], 200);

        } else {

            try {
                $phoneNumber = $request->phone_number;
                if ($request->has('phone_number')) {
                    if ($appuser = AppUser::where('phone_number', $phoneNumber)->first()) {

                        $gen_code = mt_rand(100000, 999999);

                        $phone_number = UniversalMethods::formatPhoneNumber($phoneNumber);

                        $new_code = new UserVarificationCode();
                        $new_code->phone_number = $phoneNumber;
                        $new_code->code = $gen_code;
                        $new_code->save();

                        $message = "<#>pin reset code is: " . $gen_code . " Linq Mobile Receipt : nqftCpkSBIX";
                        //$message= "Hi, Please use:- " . $gen_code ." to complete pin reset";
                        //send code to user
                        $result = SendSMSTrait::sendSMS($message, "+254" . $phone_number);

                        return response()->json([
                            'data' => $result,
                            'message' => 'we have sent a verification code to your phone number, please enter to proceed with pin reset',
                        ], 200);
                    } else {
                        return response()->json([
                            'data' => false,
                            'message' => 'Account does not exist. make sure provided the correct phone number',
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'data' => false,
                        'message' => 'Please provide your Phone Number',
                    ], 200);
                }
            } catch (\Exception $exception) {
                return response()->json([
                    'data' => false,
                    'message' => 'pin reset request failed',
                ], 200);
            }

        }

    }


    public function resetPin(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|digits:10',
            'pin' => 'required|min:4',
        ]);

        $data = ([
            'id' => 0,
            'region_id' => 0,
            'first_name' => "NONE",
            'second_name' => "NONE",
            'surname' => "NONE",
            'id_number' => "NONE",
            'phone_number' => $request->phone_number,
            'email' => "NONE",
            'date_of_birth' => "0000-00-00 00:00:00",
            'occupation' => "NONE",
            'status' => 0,
            'created_at' => "0000-00-00 00:00:00",
            'updated_at' => "0000-00-00 00:00:00",
        ]);

        if ($validator->fails()) {

            return response()->json([
                'message' => UniversalMethods::validationErrorsToString($validator->errors()),
                'token' => "NONE",
                'data' => $data,
                'status' => 0
            ], 200);

        } else {

            try {
                $phoneNumber = $request->phone_number;
                $pin = $request->pin;
                if ($request->has('phone_number')) {
                    if ($appuser = AppUser::where('phone_number', $phoneNumber)->first()) {
                        $token = $appuser->createToken('Token')->accessToken;
                        $appuser->pin = Hash::make($pin);
                        $appuser->status = 1;
                        $appuser->update();
                        return response()->json([
                            'message' => 'pin reset successfully',
                            'token' => $token,
                            'data' => $appuser,
                            'status' => 1
                        ], 200);
                    } else {
                        return response()->json([
                            'message' => 'Account does not exist. please contact your System administrator',
                            'token' => "NONE",
                            'data' => $data,
                            'status' => 0
                        ], 200);
                    }
                } else {
                    return response()->json([
                        'message' => 'Please provide phone Number',
                        'token' => "NONE",
                        'data' => $data,
                        'status' => 0
                    ], 200);
                }

            } catch (\Exception $exception) {
                return response()->json([
                    'message' => 'Could not reset your pin, an error occurred!',
                    'token' => "NONE",
                    'data' => $data,
                    'status' => 0
                ], 200);
            }

        }

    }

    /**
     * update user info api
     *
     * @return \Illuminate\Http\Response
     */
    public function updateuserinfo(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'appuser_id' => 'integer',
            'first_name' => 'required',
            'surname' => 'required',
            'region_id' => 'required',
            'phone_number' => 'required|digits:10',
            'occupation' => 'required',
            'id_number' => 'required|digits_between:6,9|numeric',
            'date_of_birth' => 'required',
            'email' => 'required|email',
        ]);

        $appuser = ([
            'id' => 0,
            'region_id' => intval($request->region_id),
            'first_name' => $request->first_name,
            'second_name' => $request->second_name,
            'surname' => $request->surname,
            'id_number' => $request->id_number,
            'phone_number' => $request->phone_number,
            'email' => $request->email,
            'date_of_birth' => Carbon::parse($request['date_of_birth'])->format('Y-m-d'),
            'occupation' => $request->occupation,
            'status' => 0,
            'created_at' => "0000-00-00 00:00:00",
            'updated_at' => "0000-00-00 00:00:00",
        ]);

        if ($validator->fails()) {

            return response()->json([
                'message' => UniversalMethods::validationErrorsToString($validator->errors()),
                'data' => $appuser,
                'status' => 0
            ], 200);

        } else {

            $appuser = AppUser::findOrFail(intval($request->appuser_id));

            if ($appuser) {
                $appuser['region_id'] = intval($request->input('region_id'));
                $appuser['first_name'] = $request->input('first_name');
                $appuser['second_name'] = $request->input('second_name');
                $appuser['surname'] = $request->input('surname');
                $appuser['id_number'] = $request->input('id_number');
                $appuser['email'] = $request->input('email');
                $appuser['date_of_birth'] = Carbon::parse($request->input('date_of_birth'))->format('Y-m-d');
                $appuser['occupation'] = $request->input('occupation');
                $appuser->save();

                return response()->json([
                    'message' => 'profile updated successfully',
                    'data' => $appuser,
                    'status' => 1
                ], 200);
            } else {
                return response()->json([
                    'message' => 'error!,account not found',
                    'data' => $appuser,
                    'status' => 0
                ], 200);
            }

        }

    }

}
