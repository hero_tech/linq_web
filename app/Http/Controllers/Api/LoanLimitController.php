<?php

namespace App\Http\Controllers\Api;

use App\AdminSetLimit;
use App\LoanLimit;
use App\SystemSetLimit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanLimitController extends Controller
{
    public function getLoanLimit(Request $request)
    {

        $app_user_id = $request->appuser_id;
        $adminSetLimit = AdminSetLimit::where('user_id',$app_user_id)->where('status',1)->get();
        $systemSetLimit = SystemSetLimit::where('user_id',$app_user_id)->where('status',1)->get();
        $defaltLoanLimit = LoanLimit::first();

        if($adminSetLimit->count() > 0){
            $userloanlimit = $adminSetLimit;
        } else if($systemSetLimit->count() > 0){
            $userloanlimit = $systemSetLimit;
        }

        /*else{

            $created = $defaltLoanLimit->created_at->format('Y-m-d H:i:s');
            $updated = $defaltLoanLimit->updated_at->format('Y-m-d H:i:s');

            $userloanlimit = ([
                'id'=> $defaltLoanLimit->id,
                'user_id'=>intval($app_user_id),
                'amount'=>$defaltLoanLimit->minimum_limit,
                'status' => 1,
                'created_at' => $created,
                'updated_at' => $updated,
            ]);

            $userloanlimit = array($userloanlimit);

        }*/

        return response()->json($userloanlimit);

    }

    /*public static function getValidationErrorsAsString($errorArray)
   {
       $errorArrayTemp = [];
       $error_strings = "";
       foreach ($errorArray as $index => $item) {
           $errStr = $item[0];
           array_push($errorArrayTemp, $errStr);
       }
       if (!empty($errorArrayTemp)) {
           $error_strings = implode('. ', $errorArrayTemp);
       }

       return $error_strings;
   }*/

}


