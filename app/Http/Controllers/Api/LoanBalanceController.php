<?php

namespace App\Http\Controllers\Api;

use App\Loan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LoanBalanceController extends Controller
{

    public function getUserLoanBalance(Request $request)
    {
        $app_user_id = $request->appuser_id;

        $activeloan = Loan::select(\DB::raw('loans.*, SUM(repayments.amount) as repayment'))
            ->leftJoin('repayments', 'repayments.loan_id', '=', 'loans.id')
            ->Join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->where('app_users.id', '=',$app_user_id)
            ->where( function ($q){
                $q->where('loan_status',1)
                    ->orWhere('loan_status',3)
                    ->orWhere('loan_status',4);
            })
            ->groupBy('loans.id')
            ->orderby('loans.id','desc')
            ->first();
//        $data =[
//          'id'=>$activeloan->id,
//            'user_id'=>$activeloan->user_id,
//            'loan_request_id'=>$activeloan->loan_request_id,
//            'repayment_cycle_interest_id'=>$activeloan->repayment_cycle_interest_id,
//            'interest_amount'=>$activeloan->interest_amount,
//            'principal_amount'=>$activeloan->principal_amount,
//            'loan_status'=>$activeloan->loan_status,
//            'start_at'=>$activeloan->start_at,
//            'due_date'=>$activeloan->due_date,
//            'clearance_date'=>$activeloan->clearance_date,
//            'created_at'=>$activeloan->created_at,
//            'updated_at'=>$activeloan->updated_at,
//            'repayment'=>$activeloan->repayment
//        ];

        return response()->json($activeloan);

    }

    public function getUserLoanBalanceUpdated(Request $request)
    {
        $app_user_id = $request->appuser_id;

//        SUM(monthly_loan_tracks.amount) as monthly_balance, monthly_loan_tracks.
        $activeloan = Loan::select(\DB::raw('loans.*, SUM(repayments.amount) as repayment'))
            ->leftJoin('repayments', 'repayments.loan_id', '=', 'loans.id')
            ->Join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->where('app_users.id', '=',$app_user_id)
            ->where( function ($q){
                $q->where('loan_status',1)
                    ->orWhere('loan_status',3)
                    ->orWhere('loan_status',4);
            })
            ->groupBy('loans.id')
            ->orderby('loans.id','desc')
            ->first();

        $monthly_balance =0.0;


        $monthly_duedate = $activeloan->due_date;


        if ($activeloan->cycle->repayment_cycle === '90 days' || $activeloan->cycle->repayment_cycle === '60 days'){
            $monthly_balance = $activeloan->active_monthly_installment - $activeloan->repayment;
            $monthly_duedate = $activeloan->active_monthly_due_date->due_date;
        }

//        $data =[
//            'id'=>$activeloan->id,
//            'user_id'=>$activeloan->user_id,
//            'loan_request_id'=>$activeloan->loan_request_id,
//            'repayment_cycle_interest_id'=>$activeloan->repayment_cycle_interest_id,
//            'interest_amount'=>$activeloan->interest_amount,
//            'principal_amount'=>$activeloan->principal_amount,
//            'loan_status'=>$activeloan->loan_status,
//            'start_at'=>$activeloan->start_at,
//            'due_date'=>$activeloan->due_date,
//            'clearance_date'=>$activeloan->clearance_date,
//            'created_at'=>$activeloan->created_at,
//            'updated_at'=>$activeloan->updated_at,
//            'monthly_balance'=>$monthly_balance,
//            'monthly_due_date'=>$activeloan->created_at,
//            'repayment'=>$activeloan->repayment
//        ];


        $data =[
            'id'=>$activeloan->id,
            'user_id'=>$activeloan->user_id,
            'loan_request_id'=>$activeloan->loan_request_id,
            'repayment_cycle_interest_id'=>$activeloan->repayment_cycle_interest_id,
            'interest_amount'=>$activeloan->interest_amount,
            'principal_amount'=>$activeloan->principal_amount,
            'loan_status'=>$activeloan->loan_status,
            'start_at'=>$activeloan->start_at,
            'due_date'=>$activeloan->due_date,
            'clearance_date'=>$activeloan->clearance_date,
            'created_at'=>$activeloan->created_at->toDateTimeString(),
            'updated_at'=>$activeloan->updated_at->toDateTimeString(),
            'monthly_balance'=>$monthly_balance,
            'monthly_due_date'=>$monthly_duedate,
            'repayment'=>$activeloan->repayment,
            'cycle'=>$activeloan->cycle->repayment_cycle,
        ];

        return response()->json((object) $data);
    }

}
