<?php

namespace App\Http\Controllers;

use App\MinimumVersion;
use Illuminate\Http\Request;

class MinimumAppVersionController extends Controller
{
    public function index() {
        $minimumversion = MinimumVersion::orderby('id','desc')->get();
        return view('minimumversion.index', ['minimumversions' => $minimumversion]);
    }

    public function create(){
        return view('minimumversion.create');
    }

    public function store(Request $request) {
        $this->validate($request,
            [
                'min_version' => 'required|integer',
            ]
        );

        $minimumversion = new MinimumVersion();
        $minimumversion->min_version = $request->input('min_version');
        $minimumversion->save();

        return redirect()->route('minimumversions')->with('success', 'Minimum Version has been successfully created!');

    }

    public function edit($id){
        $minimumversion = MinimumVersion::findOrFail($id);
        return view('minimumversion.edit', ['minimumversion' => $minimumversion]);

    }

    public function update(Request $request, $id){

        $this->validate($request,
            [
                'min_version' => 'required|integer',
            ]
        );

        $minimumversion = MinimumVersion::findOrFail($id);
        $minimumversion['min_version']=$request->input('min_version');

        if($minimumversion->save()){

            return redirect()->route('minimumversions')->with('success' , 'Minimum Version updated successfully');

        }else{

            return back()->withInput()->with('errors', 'unable to update Minimum Version');

        }

    }

    public function destroy($id){
        $minimumversion = MinimumVersion::findOrFail($id);

        if($minimumversion->delete()){

            return redirect()->route('minimumversions')->with('success' , 'Minimum Version deleted successfully');

        }else{

            return back()->withInput()->with('errors', 'unable to delete Minimum Version');

        }
    }
}
