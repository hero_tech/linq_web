<?php

namespace App\Http\Controllers;

use App\AdminSetLimit;
use App\AppUser;
use App\Contact;
use App\Country;
use App\Http\Traits\UniversalMethods;
use App\Loan;
use App\MetropolDetail;
use App\MpesaVerification;
use App\Region;
use App\Reminder;
use App\Repayment;
use App\Role;
use App\StkPushPaymentRequest;
use App\User;
use App\VerificationStkPushRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Khill\Lavacharts\Exceptions\InvalidCellCount;
use Khill\Lavacharts\Exceptions\InvalidColumnType;
use Khill\Lavacharts\Exceptions\InvalidConfigValue;
use Khill\Lavacharts\Exceptions\InvalidLabel;
use Khill\Lavacharts\Exceptions\InvalidRowDefinition;
use Khill\Lavacharts\Exceptions\InvalidRowProperty;
use Khill\Lavacharts\Lavacharts;

class AdminController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

//    function to admin dashboard home
    public function index()
    {

        $usr = Auth::user()->roles;
        return view('home');
    }

//    listing all the admins
    public function admins()
    {



        $admins = User::with('roles')->get();
//        dd($admins);
        $context = [
            'admins' => $admins
        ];
        return view('admins.admins', $context);

    }

    public function add_admin_form()
    {
        return view('admins.add_admin');
    }

    public function create_admin(Request $request)
    {

        $validator = Validator::make($request->toArray(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ]);
        $pass = str_random(8);

        $normal_admin = Role::where('name', 'Normal Admin')->first();

        $user = User::create([
            'first_name' => $request['first_name'],
            'last_name' => $request['last_name'],
            'email' => $request['email'],
            'password' => bcrypt($pass),
        ]);
        $user->roles()->attach($normal_admin);


        $context = [
            'user' => $user,
            'email' => $user->email,
            'password' => $pass
        ];

        Mail::send('mails.new_admin', $context, function ($message) use ($context) {
            $message->from('odhiamboherons12@gmail.com');
            $message->to($context['email']);
            $message->subject('Registration credentials');
        });

        return redirect()->route('admins');
    }

//    all the users of the app
    public function app_users()
    {

//        $test_users = AppUser::join('system_set_limits', 'app_users.id', '=', 'system_set_limits.user_id')
//            ->join('admin_set_limits', 'app_users.id', '=', 'admin_set_limits.user_id')
//            ->join('regions', 'app_users.region_id', '=', 'regions.id')
//            ->select('app_users.*', 'system_set_limits.amount as system_loan_limit', 'regions.name as region',
//                'admin_set_limits.amount as admin_loan_limit','admin_set_limits.status as admin_set_status',
//                'system_set_limits.status as system_set_status')
//            ->get();
//        dd($test_users);



        $app_users = AppUser::join('system_set_limits', 'app_users.id', '=', 'system_set_limits.user_id')
            ->join('regions', 'app_users.region_id', '=', 'regions.id')
            ->join('admin_user_verifications','app_users.id','=','admin_user_verifications.app_user_id')
            ->select('app_users.*', 'system_set_limits.amount as loan_limit', 'regions.name as region')
            ->where('app_users.status',1)
            ->where('admin_user_verifications.admin_verification_status',2)
            ->orderby('app_users.created_at','desc')
            ->get();
//        dd($app_users);
        $context = [

            'app_users' => $app_users

        ];
        return view('users.all_users', $context);
    }
    public function active_users()
    {
        $active_users = AppUser::join('system_set_limits', 'app_users.id', '=', 'system_set_limits.user_id')
            ->join('regions', 'app_users.region_id', '=', 'regions.id')
            ->join('admin_user_verifications','app_users.id','=','admin_user_verifications.app_user_id')
            ->select('app_users.*', 'system_set_limits.amount as loan_limit', 'regions.name as region')
            ->where('app_users.status', AppUser::ACTIVE_USER)
            ->where('admin_user_verifications.admin_verification_status',2)
            ->get();
//        dd($active_users);
        $context = [
            'active_users' => $active_users
        ];

        return view('users.active', $context);
    }

    public function user_details($id)
    {
        $user = AppUser::findOrFail($id);
//        dd($user);
        $context = [
            'user' => $user
        ];
//        dd($user);
        return view('layouts.user.details', $context);
    }

    public function user_profile($id)
    {
        $user = AppUser::findOrFail($id);
//        dd($user->first_name);

        $app_user = AppUser::join('regions', 'app_users.region_id', '=', 'regions.id')
            ->join('countries', 'regions.country_id', '=', 'countries.id')
            ->select('app_users.*', 'regions.name as region_name', 'countries.name as country')
            ->where('app_users.id', $user->id)
            ->first();
//        dd($app_user->first_name);
        $context = [
            'user' => $app_user
        ];


        return view('users.user_profile', $context);
    }

    public function user_loans($id)
    {

        $user = AppUser::findOrFail($id);

        $loans = DB::table('loans')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'repayment_cycle_interests.repayment_cycle as repayment_cycle')
            ->where('user_id', $user->id)
            ->get();

//        dd($loans);

        $context = [
            'user' => $user,
            'loans' => $loans
        ];

        return view('users.user_loans', $context);
    }

    public function user_repayments($id)
    {

        $user = AppUser::findOrFail($id);

        $loans = DB::table('loans')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'repayment_cycle_interests.repayment_cycle as repayment_cycle')
            ->where('user_id', $user->id)
            ->get();

//        dd($loans);

        //TODO: check nullability

        $repayments = DB::table('repayments')
            ->join('loans', 'repayments.loan_id', '=', 'loans.id')
            ->select('repayments.*', 'loans.principal_amount as principal', 'loans.interest_amount as interest','loans.due_date as due_date', 'loans.clearance_date as due')
            ->where('loans.user_id', $user->id)
            ->get();
//        dd(count($repayments) == 0);
        $repayment_array = $repayments->toArray();
//        dd($repayment_array);
        $total = 0;
        for ($i = 0; $i < count($repayment_array); $i++) {
            $total += $repayment_array[$i]->amount;
        }

        if (count($repayments) == 0){
            $balance = null;
        }else {
            $balance = ($repayment_array[0]->principal + $repayment_array[0]->interest) - $total;
        }

//        dd($balance);
        $context = [
            'user' => $user,
            'loans' => $loans,
            'repayments' => $repayments,
            'balance' => $balance
        ];

        return view('users.user_repayments', $context);
    }

//    countries

//list of countries
    public function countries()
    {
        $countries = Country::all();

//        $loanee = Loan::join('app_users','loans.user_id','=','app_users.id')
//            ->select('loans.*','app_users.phone_number as phone number')
//            ->get();
//        dd($loanee);

        return view('admins.countries', ['countries' => $countries]);
    }

//    to the create country form
    public function create_contry()
    {
        return view('admins.create_country');
    }

//    storing the country created
    public function store_country(Request $request)
    {

        $validator = $request->validate([
            'name' => 'required'
        ]);

        $country = Country::create(
            [
                'name' => Input::get('name')
            ]
        );

        return redirect()->route('countries');

    }

//    to edit country form
    public function edit_country($country_id)
    {
        $country = Country::find($country_id);

        return view('admins.country_edit', ['country' => $country]);

    }

//    updating the country
    public function update_country(Request $request, $counrty_id)
    {
        $country = Country::find($counrty_id);

        $country->name = $request->name;
        $country->save();

        return redirect()->route('countries')->with("success", "Country Updated successfully");
    }

//    to show country details and statistics
    public function country_details($country_id)
    {
        $country = Country::find($country_id);
        $regions = Region::join('countries', 'regions.country_id', '=', 'countries.id')
            ->select('regions.*')
            ->where('regions.country_id', $country_id)
            ->get();
        $context = [
            'regions' => $regions,
            'country' => $country
        ];
        return view('admins.country_details', $context);
    }

//    regions

    public function create_region($id)
    {
        $country = Country::findOrFail($id);
//        dd($country);
        return view('admins.add_region', ['country' => $country]);
    }

//    storing the country created
    public function store_region(Request $request)
    {
//        dd($request);

//        $country_id= Country::findOrFail($country_id)->id;

        $validator = $request->validate([
            'name' => 'required',
            'country_id' => 'required',
        ]);

        Region::create(
            [
                'name' => Input::get('name'),
                'country_id' => Input::get('country_id')
            ]
        );

        return redirect()->route('country_details',['id'=>$request->country_id])->with('success','Region added successfully');

    }

    public function edit_region($id)
    {
        $region = Region::findOrFail($id);
        return view('admins.region_edit', ['region' => $region]);
    }

    public function update_region(Request $request, $id)
    {
        $region = Region::find($id);

        $region->name = $request->name;
        $region->save();

        return redirect()->route('countries')->with("success", "Region Updated successfully");
    }


//    reminders

    public function reminders()
    {

        $reminders = Reminder::join('loans', 'reminders.loan_id', '=', 'loans.id')
            ->join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->select('reminders.*', 'loans.clearance_date as due_date', 'app_users.first_name as fname',
                'app_users.second_name as sname', 'app_users.surname as surname')
            ->get();
//        dd($reminders);
//    $loan = Loan::where('loans.id',1)->first();
//    $user = DB::table('app_users')->join('loans','app_users.id','=','loans.user_id')
//        ->select('app_users.*','loans.interest_amount as interest','loans.principal_amount as principal')
//        ->where('loans.id',1)
//        ->first();
//    dd($user);
        $context = [
            'reminders' => $reminders
        ];

        return view('users.reminders', $context);
    }

//repayments

    public function repayments($id)
    {
        $loan = Loan::findOrFail($id);

        $repayments = Repayment::join('mpesa_repaymnts', 'repayments.mpesa_repayment_id', '=', 'mpesa_repaymnts.id')
            ->join('loans', 'repayments.loan_id', '=', 'loans.id')
            ->join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->select('repayments.*', 'loans.principal_amount as principal', 'loans.interest_amount as interest',
                'app_users.first_name as first_name', 'app_users.second_name as second_name', 'app_users.surname as surname', 'app_users.id as user')
            ->where('loans.id',$loan->id)
            ->orderByDesc('created_at')
            ->get();
//        dd($repayments);
        $repayment_array = $repayments->toArray();

//        dd($repayment_array);

        $context = [
            'repayments' => $repayment_array,
        ];

        return view('users.repayments', $context);
    }


//    Reports

    public function reports()
    {


//        data

//        $users = AppUser::join('regions', 'app_users.region_id', '=', 'regions.id')
//            ->join('loans', 'app_users.id', '=', 'loans.user_id')
//            ->join('repayments', 'loans.id', '=', 'repayments.loan_id')
//            ->select('app_users.*', 'date_of_birth as dob', 'regions.name as region', 'loans.principal_amount as principal',
//                'loans.interest_amount as interest','repayments.balance as balance','repayments.created_at as date')
//            ->where('app_users.status', 1)
//            ->where('loans.loan_status', '!=', 2)
//            ->orderby('date','desc')
//            ->get()->toArray();
        $all_users = AppUser::all();

        $users = AppUser::join('regions', 'app_users.region_id', '=', 'regions.id')
            ->join('loans', 'app_users.id', '=', 'loans.user_id')
            ->select('app_users.*', 'date_of_birth as dob', 'regions.name as region', 'loans.principal_amount as principal',
                'loans.interest_amount as interest')
            ->where('app_users.status', 1)
            ->where('loans.loan_status', '=', 1)
            ->get()->toArray();

        $users_pending = AppUser::join('regions', 'app_users.region_id', '=', 'regions.id')
            ->join('loans', 'app_users.id', '=', 'loans.user_id')
            ->select('app_users.*', 'date_of_birth as dob', 'regions.name as region', 'loans.principal_amount as principal',
                'loans.interest_amount as interest')
            ->where('app_users.status', 1)
            ->where('loans.loan_status', '=', 3)
            ->get()->toArray();

        $loans = Loan::join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'repayment_cycle_interests.interest_rate as rate')
            ->where('loan_status', '!=', 2)
            ->get();

//        dd($users_pending);

//        constants & variables
        $today = date('Y');
        $youths = 0;
        $youths_pending=0;
        $mid_adults_pending = 0;
        $old_pending = 0;
        $mid_adults = 0;
        $two_weeks = 0;
        $old = 0;
        $monthly = 0;
        for ($i = 0; $i < count($users); $i++) {

            $year = date('Y', strtotime($users[$i]['dob']));
            $age = (int)$today - (int)$year;
            $user_amount  = $users[$i]['principal']+ $users[$i]['interest'];
            if ($age < 36) {
                $youths += $user_amount;
            } else if ($age < 70) {
                $mid_adults += $user_amount;
            } else {
                $old += $user_amount;
            }
        }

        for ($m = 0; $m < count($users_pending); $m++) {

            $year_pending = date('Y', strtotime($users_pending[$m]['dob']));
            $age_pending = (int)$today - (int)$year_pending;
            $user_pending_amount  = $users_pending[$m]['principal']+ $users_pending[$m]['interest'];
            if ($age_pending < 36) {
                $youths_pending += $user_pending_amount;
            } else if ($age_pending < 70) {
                $mid_adults_pending += $user_pending_amount;
            } else {
                $old_pending += $user_pending_amount;
            }
        }

        for ($j = 0; $j < count($loans); $j++) {
            $amount = $loans[$j]->principal_amount + $loans[$j]->interest_amount;
            if ($loans[$j]->rate == 0.15) {
                $monthly += $amount;
            } elseif($loans[$j]->rate == 0.1) {
                $two_weeks += $amount;
            }else{

            }
        }
//        dd($monthly);

//    Lavacharts
        $lava = new Lavacharts();

        $regions = $lava->DataTable();
        $classes = $lava->DataTable();
        $interests = $lava->DataTable();
        $pending_stats = $lava->DataTable();

        try {
            $regions->addStringColumn('Regions')
                ->addNumberColumn('Users')
                ->addRow(['Kenya', count($all_users)]);
        } catch (InvalidColumnType $e) {
        } catch (InvalidLabel $e) {
        } catch (InvalidRowDefinition $e) {
        } catch (InvalidCellCount $e) {
        } catch (InvalidRowProperty $e) {
        }

        try {
            $classes->addStringColumn("Age Group")
                ->addNumberColumn("Amount")
                ->addRow(['Youths (18 - 35)', $youths])
                ->addRow(['Middle (36 - 69)', $mid_adults])
                ->addRow(['Aged (Above 69)', $old]);
        } catch (InvalidCellCount $e) {
        } catch (InvalidColumnType $e) {
        } catch (InvalidLabel $e) {
        } catch (InvalidRowDefinition $e) {
        } catch (InvalidRowProperty $e) {
        }

        try {
            $pending_stats->addStringColumn("Age Group")
                ->addNumberColumn("Amount")
                ->addRow(['Youths (18 - 35)', $youths_pending])
                ->addRow(['Middle (36 - 69)', $mid_adults_pending])
                ->addRow(['Aged (Above 69)', $old_pending]);
        } catch (InvalidCellCount $e) {
        } catch (InvalidColumnType $e) {
        } catch (InvalidLabel $e) {
        } catch (InvalidRowDefinition $e) {
        } catch (InvalidRowProperty $e) {
        }

        try {
            $interests->addStringColumn('Loan Cycle')
                ->addNumberColumn('Loan Requests')
                ->addRow(['30 Day', $monthly])
                ->addRow(['14 Day', $two_weeks]);
        } catch (InvalidCellCount $e) {
        } catch (InvalidColumnType $e) {
        } catch (InvalidLabel $e) {
        } catch (InvalidRowDefinition $e) {
        } catch (InvalidRowProperty $e) {
        }

        $lava->GeoChart('Population', $regions, [
            'title' => 'User Locations'
        ]);

        $lava->PieChart('UserGroups', $classes, [
            'title' => 'Active Loans By Age Groups',
            'is3D' => true,
            'slices' => [
                ['offset' => 0.1],
                ['offset' => 0.15],
                ['offset' => 0.2]
            ]
        ]);

        $lava->PieChart('UserGroupsPending', $pending_stats, [
            'title' => 'Defaulted Loans By Age Groups',
            'is3D' => true,
            'slices' => [
                ['offset' => 0.02],
                ['offset' => 0.03],
                ['offset' => 0.05]
            ]
        ]);

        $lava->DonutChart('Loan Cycles', $interests, [

            'title' => 'Pending Loans By Repayment Cycles',
            'color' => ['#012B43', '#E55025'],
            'fontSize' => 14

        ]);

        $context = [
            'lava_chart' => $lava
        ];

        return view('admins.reports', $context);
    }

    public function enquiries(){
        $enquiries = Contact::all();
//        dd($enquiries->isEmpty());

        $user = AppUser::all();
//        dd($user == null);

        return view('admins.enquiries',['enquiries'=>$enquiries,'user'=>$user]);
    }

    public function select_users_table(){

        $app_users = AppUser::join('system_set_limits', 'app_users.id', '=', 'system_set_limits.user_id')
            ->join('regions', 'app_users.region_id', '=', 'regions.id')
            ->select('app_users.*', 'system_set_limits.amount as loan_limit', 'regions.name as region')
            ->orderby('app_users.created_at','desc')
            ->get();
//        dd($app_users);
        $context = [

            'app_users' => $app_users

        ];
        return view('users.all_users_select', $context);

    }

    //users with bad debt
    public function app_users_with_bad_debt()
    {
        $app_users = AppUser::join('system_set_limits', 'app_users.id', '=', 'system_set_limits.user_id')
            ->join('regions', 'app_users.region_id', '=', 'regions.id')
            ->join('loans','app_users.id','=','loans.user_id')
            ->select('app_users.*', 'system_set_limits.amount as loan_limit', 'regions.name as region')
            ->where('loans.loan_status',4)
            ->orderby('app_users.created_at','desc')
            ->get();

        $context = [
            'app_users' => $app_users
        ];

        return view('users.bad_debt_users', $context);
    }

    public function blocked_users(){
        $users = AppUser::where('status',2)->get();
        $context = [
          'users'=>$users
        ];
        return view('users.blocked',$context);
    }
    public function flagged_users(){
        $users = AppUser::join('regions','regions.id','=','region_id')
            ->select('app_users.*','regions.name as region')
            ->where('status',3)->get();
        $context = [
          'users'=>$users
        ];
        return view('users.flagged',$context);
    }

    public function flagged_user_reset($user_id){
        $user = AppUser::findOrFail($user_id);
        $loan = Loan::where('user_id',$user_id)->first();

        if ($loan != null){
            return redirect()->back()->with("success","You can not reset a user who has a loan history. Please edit details instead");
        }else{
            $user->delete();
            return redirect()->route('flagged_users')->with('success','user account reset successfully');
        }
//        return view('users.edit',['user'=>$user]);
    }

    public function edit_user_profile($user_id)
    {
        $user = AppUser::findOrFail($user_id);
        return view('users.edit',['user'=>$user]);
    }

    public function flagged_user_update(Request $request,$user_id)
    {


        Validator::make($request->all(),[
            'f_name'=>'required',
            'm_name'=>'required',
            'o_name'=>'required',
            'phone'=>'required',
            'id'=>'required',
        ])->validate();
//        dd($request);

        $user = AppUser::findOrFail($user_id);
        $verification = VerificationStkPushRequest::where('user_id',$user_id)->get();
        $mpesa_verification = MpesaVerification::where('user_id',$user_id)->first();
        $metropol_detail = MetropolDetail::where('user_id',$user_id)->first();

            if ($verification != null){
                foreach ($verification as $ver){
                    $ver->status = 2;
                    $ver->save();
                }
            }

            if ($mpesa_verification != null){

                $mpesa_verification->verification_status = 1;
                $mpesa_verification->save();

            }

            if (Input::get('id') != $metropol_detail->identity_number){

                $metropol_detail->delete();

            }

            $user->first_name = Input::get('f_name');
            $user->second_name = Input::get('m_name');
            $user->surname = Input::get('o_name');
            $user->phone_number = Input::get('phone');
            $user->id_number = Input::get('id');
            $user->status = 1;
            $user->save();

        return redirect()->route('users')->with('success','User details updated successfully');

    }

    public function block_user($user_id){
        $user = AppUser::findOrFail($user_id);

        $user->status = 2;
        $user->save();
        return redirect()->route('users')->with('success','User blocked successfully');
    }
    public function unblock_user($user_id){
        $user = AppUser::findOrFail($user_id);

        $user->status = 1;
        $user->save();
        return redirect()->route('users')->with('success','User unblocked successfully');
    }

    public function deactivate_admin($admin_id){

        $admin = User::findOrFail($admin_id);

        $admin->status = 2;
        $admin->save();

        return redirect()->route('admins')->with('success','Admin deactivated successfully');

    }
    public function activate_admin($admin_id){

        $admin = User::findOrFail($admin_id);

        $admin->status = 1;
        $admin->save();

        return redirect()->route('admins')->with('success','Admin activated successfully');

    }
}
