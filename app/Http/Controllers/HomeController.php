<?php

namespace App\Http\Controllers;

use App\AppUser;
use App\Contact;
use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use App\Loan;
use App\LoanRequest;
use App\Payment;
use App\Repayment;
use Carbon\Carbon;
use Dotenv\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Khill\Lavacharts\Exceptions\InvalidCellCount;
use Khill\Lavacharts\Exceptions\InvalidColumnType;
use Khill\Lavacharts\Exceptions\InvalidDateTimeFormat;
use Khill\Lavacharts\Exceptions\InvalidLabel;
use Khill\Lavacharts\Exceptions\InvalidRowDefinition;
use Khill\Lavacharts\Exceptions\InvalidRowProperty;
use Khill\Lavacharts\Lavacharts;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $role = Auth::user()->hasAnyRole('Normal Admin');
//        dd($role);

        $lava = new Lavacharts();


//        $s = "HERONS";
//        $l = "Heroins";
//        if (strcasecmp($s,$l) == 0){
//            $t=2;
//        }else{
//            $t = 1;
//        }
//        dd($t);


//        dd(Carbon::parse(now()));
//        $due = Carbon::parse('2019-01-23 08:53:05.0');
//        $now = Carbon::parse('2019-01-30 08:53:05.0');
//        if ($due->addDays(7)->equalTo($now)) {
//            dd('true');
//        } else {
//            dd('false');
//        }

        $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
            ->where('loans.id',1)
            ->first();
//        $defaulted = UniversalMethods::checkIfDefaulted($loans->id);
//        dd($defaulted);



        $loans = Loan::join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->join('repayment_cycle_interests', 'loans.repayment_cycle_interest_id', '=', 'repayment_cycle_interests.id')
            ->select('loans.*', 'app_users.phone_number as phone', 'app_users.first_name as name', 'repayment_cycle_interests.repayment_cycle as cycle')
            ->get();

        foreach ($loans as $loan){
            $defaulted = UniversalMethods::seventhDayDefaulted($loan->id);
            $p = UniversalMethods::formatPhoneForAT($loan->phone);
            if ($defaulted){
//                SendSMSTrait::sendSMS('hello',$p);
//                dd('true');
            }else{
//                dd('false');
            }
        }

//        $phone_numbers = implode(",",$p);
//        SendSMSTrait::sendSMS($mesge,$phone_numbers);
//        dd($p);


        $cleared_loans = DB::table('loans')
            ->where('loan_status', 2)
            ->get();
        $loan_requests = LoanRequest::where('loan_request_status', 2)->get();
        $total_loan_requests = count($loan_requests);
        $total_cleared_loans = count($cleared_loans);

        $defaulted_loans = DB::table('loans')
            ->where('loan_status', 3)->get();
        $total_defaulted_loans = count($defaulted_loans);

        $active_loans = DB::table('loans')
            ->where('loan_status', 1)->get();
//        $month = date('m', strtotime($active_loans[0]->created_at));
//        dd($month);
        $total_active_loans = count($active_loans);

        $all_users = DB::table('app_users')
            ->where('status', 1)
            ->count();

        $january = 0;
        $february = 0;
        $march = 0;
        $april = 0;
        $may = 0;
        $june = 0;
        $july = 0;
        $august = 0;
        $september = 0;
        $october = 0;
        $november = 0;
        $december = 0;
        $jan = 0;
        $feb = 0;
        $mar = 0;
        $apr = 0;
        $my = 0;
        $jun = 0;
        $jul = 0;
        $aug = 0;
        $sept = 0;
        $oct = 0;
        $nov = 0;
        $dec = 0;

        $repayments = Repayment::all();
        $payments = Loan::all();
//        dd($payments);

        $current_year = date("Y");

        for ($j = 0; $j < count($repayments); $j++) {

            $month = date('m', strtotime($repayments[$j]->created_at));
            $year = date('Y', strtotime($repayments[$j]->created_at));

            if ($month === "01" && $year === $current_year) {
                $january += $repayments[$j]->amount;
            } else if ($month === "02" && $year === $current_year) {
                $february += $repayments[$j]->amount;
            } else if ($month === "03" && $year === $current_year) {
                $march += $repayments[$j]->amount;
            } else if ($month === "04" && $year === $current_year) {
                $april += $repayments[$j]->amount;
            } else if ($month === "05" && $year === $current_year) {
                $may += $repayments[$j]->amount;
            } else if ($month === "06" && $year === $current_year) {
                $june += $repayments[$j]->amount;
            } else if ($month === "07" && $year === $current_year) {
                $july += $repayments[$j]->amount;
            } else if ($month === "08" && $year === $current_year) {
                $august += $repayments[$j]->amount;
            } else if ($month === "09" && $year === $current_year) {
                $september += $repayments[$j]->amount;
            } else if ($month === "10" && $year === $current_year) {
                $october += $repayments[$j]->amount;
            } else if ($month === "11" && $year === $current_year) {
                $november += $repayments[$j]->amount;
            } else if ($month === "12" && $year === $current_year) {
                $december += $repayments[$j]->amount;
            }
        }

        for ($i = 0; $i < count($payments); $i++) {

            $mo = date('m', strtotime($payments[$i]->created_at));
            $yr = date('Y', strtotime($payments[$i]->created_at));

            if ($mo === "01" && $yr === $current_year) {
                $jan += $payments[$i]->principal_amount;
            } else if ($mo === "02" && $yr === $current_year) {
                $feb += $payments[$i]->principal_amount;
            } else if ($mo === "03" && $yr === $current_year) {
                $mar += $payments[$i]->principal_amount;
            } else if ($mo === "04" && $yr === $current_year) {
                $apr += $payments[$i]->principal_amount;
            } else if ($mo === "05" && $yr === $current_year) {
                $my += $payments[$i]->principal_amount;
            } else if ($mo === "06" && $yr === $current_year) {
                $jun += $payments[$i]->principal_amount;
            } else if ($mo === "07" && $yr === $current_year) {
                $jul += $payments[$i]->principal_amount;
            } else if ($mo === "08" && $yr === $current_year) {
                $aug += $payments[$i]->principal_amount;
            } else if ($mo === "09" && $yr === $current_year) {
                $sept += $payments[$i]->principal_amount;
            } else if ($mo === "10" && $yr === $current_year) {
                $oct += $payments[$i]->principal_amount;
            } else if ($mo === "11" && $yr === $current_year) {
                $nov += $payments[$i]->principal_amount;
            } else if ($mo === "12" && $yr === $current_year) {
                $dec += $payments[$i]->principal_amount;
            }
        }

//        dd($nov);

        $total_active_loans_amount = 0.0;
        for ($i = 0; $i < $total_active_loans; $i++) {
            $total_active_loans_amount += ($active_loans[$i]->interest_amount + $active_loans[$i]->principal_amount);
        }
        $total_defaulted_loans_amount = 0.0;
        for ($i = 0; $i < $total_defaulted_loans; $i++) {
            $total_defaulted_loans_amount += ($defaulted_loans[$i]->interest_amount + $defaulted_loans[$i]->principal_amount);
        }

        $total_cleared_loans_amount = 0.0;
        for ($i = 0; $i < $total_cleared_loans; $i++) {
            $total_cleared_loans_amount += ($cleared_loans[$i]->interest_amount + $cleared_loans[$i]->principal_amount);
        }

        $loans_donut_stats = $lava->DataTable();

        try {
            $loans_donut_stats
                ->addStringColumn('Loan Status')
                ->addNumberColumn('Amount')
                ->addRow(['Cleared Loans', $total_cleared_loans_amount])
                ->addRow(['Active Loans', $total_active_loans_amount])
                ->addRow(['Defaulted Loans', $total_defaulted_loans_amount]);
        } catch (InvalidCellCount $e) {
        } catch (InvalidColumnType $e) {
        } catch (InvalidLabel $e) {
        } catch (InvalidRowDefinition $e) {
        } catch (InvalidRowProperty $e) {
        }

        $loans_per_month = $lava->DataTable();

        try {
            $loans_per_month->addStringColumn('Month')
                ->addNumberColumn('Amount')
                ->addRow(['January', $january])
                ->addRow(['February', $february])
                ->addRow(['March', $march])
                ->addRow(['April', $april])
                ->addRow(['May', $may])
                ->addRow(['June', $june])
                ->addRow(['July', $july])
                ->addRow(['August', $august])
                ->addRow(['September', $september])
                ->addRow(['October', $october])
                ->addRow(['November', $november])
                ->addRow(['December', $december]);
        } catch (InvalidCellCount $e) {
        } catch (InvalidColumnType $e) {
        } catch (InvalidLabel $e) {
        } catch (InvalidRowDefinition $e) {
        } catch (InvalidRowProperty $e) {
        }

        $loans_comparison_yearly = $lava->DataTable();

        try {
            $loans_comparison_yearly->addDateColumn('Month')
                ->addNumberColumn('Disbursements')
                ->addNumberColumn('Repayments')
                ->setDateTimeFormat('M')
                ->addRow(['January', $jan, $january])
                ->addRow(['February', $feb, $february])
                ->addRow(['March', $mar, $march])
                ->addRow(['April', $apr, $april])
                ->addRow(['May', $my, $may])
                ->addRow(['June', $jun, $june])
                ->addRow(['July', $jul, $july])
                ->addRow(['August', $aug, $august])
                ->addRow(['September', $sept, $september])
                ->addRow(['October', $oct, $october])
                ->addRow(['November', $nov, $november])
                ->addRow(['December', $dec, $december]);
        } catch (InvalidCellCount $e) {
        } catch (InvalidColumnType $e) {
        } catch (InvalidDateTimeFormat $e) {
        } catch (InvalidLabel $e) {
        } catch (InvalidRowDefinition $e) {
        } catch (InvalidRowProperty $e) {
        }


        $lava->ColumnChart('Loans', $loans_comparison_yearly, [
            'title' => 'Monthly Loan Disbursements Against Repayments',
            'titleTextStyle' => [
                'color' => ['#012B43', '#E55025'],
                'fontSize' => 14
            ]
        ]);
        $lava->BarChart('Loans', $loans_per_month, [
            'title' => 'Monthly Repayments',
            'titleTextStyle' => [
                'color' => '#012B43',
                'fontSize' => 14
            ]
        ]);
        $lava->DonutChart('Loans', $loans_donut_stats, [
            'title' => 'Loan Performance',
            'titleTextStyle' => [
                'color' => '#012B43',
                'fontSize' => 14
            ]
        ]);

        $context = [
            'lava' => $lava,
            'active' => $total_active_loans,
            'cleared' => $total_cleared_loans,
            'defaulted' => $total_defaulted_loans,
            'users' => $all_users
        ];

        return view('admins.home', $context);
    }

}
