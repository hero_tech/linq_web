<?php

namespace App\Http\Controllers;

use App\RefferalCode;
use Illuminate\Http\Request;

class CodeController extends Controller
{
    //
    public function index() {
        $codes = RefferalCode::all();
        return view('code.index', ['codes' => $codes]);
    }
    public function create(){
        return view('code.create',['code'=>RefferalCode::find(1)]);
    }
    public function store(Request $request) {

        $this->validate($request,
            [
                'code' => 'required|unique:refferal_codes,code',
            ]
        );
//        dd(dd(RefferalCode::find(1)));
        if (RefferalCode::find(1) == null)   {
            $code = new RefferalCode();
            $code->code = $request->input('code');
            $code->save();
            $message = 'Referral Code has been successfully created!';
        }   else{
            $referral_code = RefferalCode::findOrFail(1);

            $referral_code->code = $request->input('code');
            $referral_code->save();
            $message = 'Referral Code has been successfully updated!';
        }
        return redirect()->route('code')->with('success', $message);

    }
}
