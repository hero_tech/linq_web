<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{

    public function index() {
        $faqs = Faq::all();
        return view('faq.faqs', ['faqs' => $faqs]);
    }

    public function create()
    {
        return view('faq.faq_create');
    }

    public function store(Request $request) {

        $this->validate($request,
            [
                'question' => 'required',
                'content' => 'required',
            ]
        );

        $faq = new Faq();
        $faq->question = $request->input('question');
        $faq->content = $request->input('content');
        $faq->save();

        return redirect()->route('faq-all')->with('success', 'FAQ has been successfully created!');

    }

    public function edit($id)
    {
        $faq = Faq::findOrFail($id);
        return view('faq.faq_edit', ['faq' => $faq]);
    }

    public function update(Request $request, $id)
    {
        $faq = Faq::findOrFail($id);
        $faq['question']=$request->input('question');
        $faq['content']=$request->input('content');

        if($faq->save()){

            return redirect()->route('faq-all')->with('success' , 'FAQ updated successfully');

        }else{

            return back()->withInput()->with('errors', 'unable to update FAQ');

        }

    }

    public function show($id)
    {
        $faq = Faq::findOrFail($id);
        return view('faq.faq_show',['faq'=>$faq]);
    }

    public function destroy($id)
    {
        $faq = Faq::findOrFail($id);

        if($faq->delete()){

            return redirect()->route('faq-all')->with('success' , 'FAQ deleted successfully');

        }else{

            return back()->withInput()->with('errors', 'unable to delete FAQ');

        }
    }

}
