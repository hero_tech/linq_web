<?php

namespace App\Http\Controllers;

use App\MetropolScoreRequest;
use Illuminate\Http\Request;

class ScoreReportController extends Controller
{
    //
    public function index()
    {
        return view('admins.score_reports',['reports'=>MetropolScoreRequest::all()]);
    }

}
