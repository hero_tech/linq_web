<?php

namespace App\Http\Controllers;

use App\RepaymentCycleInterest;
use Illuminate\Http\Request;

class RepayCycleController extends Controller
{
    public function index() {
        $repaymentcycle = RepaymentCycleInterest::all();
        return view('repaymentcycle.index', ['repaymentcycles' => $repaymentcycle]);
    }

    public function create(){
        return view('repaymentcycle.create');
    }

    public function store(Request $request) {

        $this->validate($request,
            [
                'repayment_cycle' => 'required|unique:repayment_cycle_interests,repayment_cycle',
                'interest_rate' => 'required|regex:/^\d+(\.\d{1,2})?$/|unique:repayment_cycle_interests,interest_rate',
            ]
        );

        $repaymentcycle = new RepaymentCycleInterest();
        $repaymentcycle->repayment_cycle = $request->input('repayment_cycle');
        $repaymentcycle->interest_rate = $request->input('interest_rate');
        $repaymentcycle->save();

        return redirect()->route('repaymentcycles')->with('success', 'Repayment Cycle has been successfully created!');

    }

    public function edit($id){
        $repaymentcycle = RepaymentCycleInterest::findOrFail($id);
        return view('repaymentcycle.edit', ['repaymentcycle' => $repaymentcycle]);
    }

    public function update(Request $request, $id){
        $repaymentcycle = RepaymentCycleInterest::findOrFail($id);
        $repaymentcycle['repayment_cycle']=$request->input('repayment_cycle');
//        $repaymentcycle['interest_rate']=$request->input('interest_rate');

        if($repaymentcycle->save()){

            return redirect()->route('repaymentcycles')->with('success' , 'Repayment Cycle updated successfully');

        }else{

            return back()->withInput()->with('errors', 'unable to update Repayment Cycle');

        }

    }

    public function destroy($id){
        $repaymentcycle = RepaymentCycleInterest::findOrFail($id);

        if($repaymentcycle->delete()){

            return redirect()->route('repaymentcycles')->with('success' , 'Repayment Cycle deleted successfully');

        }else{

            return back()->withInput()->with('errors', 'unable to delete Repayment Cycle');

        }
    }
}
