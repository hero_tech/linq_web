<?php

namespace App\Http\Controllers;

use App\AdminSetLimit;
use App\AppUser;
use App\Contact;
use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use App\Loan;
use App\LoanRequest;
use App\MetropolCrb;
use App\Payment;
use App\Policy;
use App\SystemSetLimit;
use App\Terms;
use App\TermsAndCondition;
use Carbon\Carbon;
use Dotenv\Validator;
use function GuzzleHttp\Promise\all;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;

class LoanController extends Controller
{
    //
    public function index()
    {
        $all_loans = DB::table('loans')->join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->select('loans.*', 'app_users.first_name as user_first_name',
                'app_users.second_name as user_second_name', 'app_users.surname as user_surname', 'app_users.phone_number as user_phone_number')
            ->orderBy('loans.updated_at', 'desc')
            ->get();
//        dd($all_loans);

//        dd($all_loans->pluck('loan_status'));
        $context = [
            'loans' => $all_loans
        ];
        return view('loans.loans', $context);
    }

    public function active_loans()
    {
        $active_loans = DB::table('loans')->join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->select('loans.*', 'app_users.first_name as user_first_name',
                'app_users.second_name as user_second_name', 'app_users.surname as user_surname', 'app_users.phone_number as user_phone_number')
            ->where('loan_status', 1)
            ->get();
        $context = [
            'active_loans' => $active_loans
        ];

        return view('loans.active', $context);
    }

    public function cleared_loans()
    {
        $cleared_loans = DB::table('loans')->join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->select('loans.*', 'app_users.first_name as user_first_name',
                'app_users.second_name as user_second_name', 'app_users.surname as user_surname', 'app_users.phone_number as user_phone_number')
            ->where('loan_status', 2)
            ->get();
        $context = [
            'cleared_loans' => $cleared_loans
        ];

        return view('loans.cleared', $context);
    }

    public function defaulted_loans()
    {
        $defaulted_loans = DB::table('loans')->join('app_users', 'loans.user_id', '=', 'app_users.id')
            ->select('loans.*', 'app_users.first_name as user_first_name',
                'app_users.second_name as user_second_name', 'app_users.surname as user_surname', 'app_users.phone_number as user_phone_number')
            ->where('loan_status', 3)
            ->get();

        $context = [
            'defaulted_loans' => $defaulted_loans
        ];

        return view('loans.defaulted', $context);
    }

    //method to set or unset loan as bad debt
    public function set_or_unset_bad_debt($id,$val)
    {
        $loan = Loan::findOrFail($id);

        if($val == 1){
            if($loan->due_date < $now = Carbon::parse(now())->format('Y-m-d')){
                $loan['loan_status']=3;
            }else{
                $loan['loan_status']=$val;
            }
        }else{
            $loan['loan_status']=$val;
        }

        $loanamount = $loan->interest_amount + $loan->principal_amount;

        if($loan->save()){
            if($val == 1){
                return redirect()->back()->with('success' , $loan->loanuser->first_name.' Loan of '.$loanamount.' Ksh set to bad debt successfully');
            }else{
                return redirect()->back()->with('success' ,  $loan->loanuser->first_name.' Loan of '.$loanamount.' Ksh unset to bad debt successfully');
            }
        }else{
            return back()->withInput()->with('errors', ['unable to act on Loan']);
        }
    }

    public function loan_limits()
    {

    }

    public function admin_set_limits()
    {

        $users = AppUser::all();
//        dd($users);

        $admin_set_limits = AdminSetLimit::join('app_users', 'admin_set_limits.user_id', '=', 'app_users.id')
            ->select('admin_set_limits.*', 'app_users.status as user_status', 'app_users.first_name as first_name',
                'app_users.second_name as second_name', 'app_users.phone_number as phone')
            ->where('admin_set_limits.status', 1)
            ->orderBy('admin_set_limits.created_at', 'desc')
            ->get();
//        dd($admin_set_limits);
        $context = [
            'limits' => $admin_set_limits,
            'users' => $users
        ];
        return view('loans.admin_set_limits', $context);
    }

    public function system_set_limits()
    {

        $system_set_limits = SystemSetLimit::join('app_users', 'system_set_limits.user_id', '=', 'app_users.id')
            ->select('system_set_limits.*', 'app_users.status as user_status', 'app_users.first_name as first_name',
                'app_users.second_name as second_name', 'app_users.phone_number as phone')
            ->get();
//        dd($system_set_limits);
        $context = [
            'limits' => $system_set_limits
        ];

        return view('loans.system_set_limits', $context);
    }

    public function set_loan_limit_form($id)
    {

        $app_user = AppUser::findOrFail($id);
//        dd($app_user);
        $phone = $app_user->phone_number;
//        dd($phone);

        $user = AppUser::where('phone_number', $phone)->first();
        if ($user === null) {
            return redirect()->back()->with("failure", "No user was selected");
        }
//        dd($user);

        return view('loans.set_limit', ['user' => $user]);
    }

    public function set_loan_limit(Request $request, $user_id)
    {

        $user = AppUser::findOrFail($user_id);

//        $metropol = MetropolCrb::where('user_id', '=', $user_id)->first();
//        $score = $metropol->credit_score;
//        if ($score <= 400) {
//
//            return redirect()->back()->with('success', 'The user does not have enough score to get a loan limit');
//
//        } else {

//        dd($request);

            $this->validate($request, [
                'new_limit' => 'required'
            ]);
//        dd($request);
            if ($request->new_limit < 1000 || $request->new_limit > 70000) {
                return redirect()->back()->with('success', 'The loan limit must be between 1000 and 70000');
            } else {
                if (!UniversalMethods::hasActiveLoan($user->id)) {

                    $loan_limits = AdminSetLimit::where('user_id', $user->id)->get();

                    if ($loan_limits->isEmpty()) {
                        AdminSetLimit::create([
                            'user_id' => $user->id,
                            'amount' => Input::get('new_limit'),
                            'status' => 1
                        ]);
                    } else {
                        foreach ($loan_limits as $limit) {
                            $limit->status = 2;
                            $limit->save();
                        }
                        AdminSetLimit::create([
                            'user_id' => $user->id,
                            'amount' => Input::get('new_limit'),
                            'status' => 1
                        ]);
                    }

                    return redirect()->route('admin_set_limits')->with("success", "Limit set successfully");
                } else {
                    return redirect()->route('admin_set_limits')->with("failure", "Sorry, You can't set a loan limit to a user with an active loan.");
                }
            }
//        }

    }

    public function payments()
    {
        $payments = Payment::all();

        $context = [
            'payments' => $payments
        ];

        return view('loans.payments', $context);
    }

    /**
     * Reset admin_set loan limits
     */
    public function reset_admin_set_limit($id)
    {
        $limit = AdminSetLimit::where('id', $id)->first();
        $limit->status = 2;
        $limit->save();
        return redirect()->back()->with("success", "Limit deactivated successful");

    }

    public function activate_admin_set_limit($id)
    {
        $limit = AdminSetLimit::where('id', $id)->first();
        $limit->status = 1;
        $limit->save();
        return redirect()->back()->with("success", "Limit activated successful");

    }

    public function limit_update_form($id)
    {
        $limit = AdminSetLimit::where('id', $id)->first();
        return view('loans.edit_admin_set_limit', ['limit' => $limit]);
    }

    public function limit_update($id)
    {
        $limit = AdminSetLimit::where('id', $id)->first();

        $limit->amount = Input::get('limit');
        $limit->save();
        return redirect()->route('admin_set_limits');
    }

    public function contact_message(Request $request)
    {

        $this->validate($request, [
            'phone' => ['required', 'regex:/^(7|07|\+2547|2547)(\d){8}/'],
            'email' => 'required',
            'message' => 'required'
        ]);

        if ($request->name == null) {

            Contact::create([
                'phone' => Input::get('phone'),
                'email' => Input::get('email'),
                'message' => Input::get('message')
            ]);
            $context = [
                'email' => $request->email,
                'phone' => $request->phone,
                'message' => $request->message

            ];

            Mail::send('mails.contact_mail', $context, function ($message) use ($context) {
                $message->from('linq.mobi@gmail.com');
                $message->to($context['email']);
                $message->subject('Message Received');
            });

            Mail::send('mails.notify_mail', $context, function ($message) use ($context) {
                $message->from('linq.mobi@gmail.com');
                $message->to('linq.mobi@gmail.com');
                $message->subject('Message Sent');
            });
        }
        return redirect()->back();
    }

    public function tocs()
    {
        return view('landing_pages.t_and_cs');
    }

    public function update_tac_form()
    {
        $tac = Terms::where('id', 1)->first();
//        dd($tac);
        $context = ['tac' => $tac];
        return view('admins.update_toc', $context);
    }

    public function update_privacy_policy_form()
    {
        $pol = Policy::where('id', 1)->first();
//        dd($tac);
        $context = ['pol' => $pol];
        return view('admins.update_policy', $context);
    }


    public function create_tac(Request $request)
    {
        $this->validate($request,
            [
                'terms_conditions' => 'required',
            ]
        );

        $terms = Terms::where('id', 1)->first();

        if (!$terms == null) {
            $terms->terms_conditions = $request->input('terms_conditions');
            $terms->save();

        } else {

            $tac = new Terms();
            $tac->terms_conditions = $request->input('terms_conditions');
            $tac->save();
        }

        return redirect()->route('super_admin_home')->with('success', 'Terms and condition has been successfully created!');

    }

    public function create_policy(Request $request)
    {
        $this->validate($request,
            [
                'policy' => 'required',
            ]
        );

        $policy = Policy::where('id', 1)->first();

        if (!$policy == null) {
            $policy->policy = $request->input('policy');
            $policy->save();

        } else {

            $policy = new Policy();
            $policy->policy = $request->input('policy');
            $policy->save();
        }

        return redirect()->route('super_admin_home')->with('success', 'Privacy Policy has been successfully created!');

    }

    public function privacy()
    {
        return view('landing_pages.privacy');
    }


    public function loan_requests()
    {
        $loan_request = LoanRequest::where('loan_request_status', 2)->get();

//        dd($loan_request);
        $context = [
            'loan_requests' => $loan_request
        ];
        return view('admins.loan_requests', $context);

    }


    public function reject_request($request_id)
    {
        $loan_request = LoanRequest::where('id', $request_id)->first();

        $loan_request->loan_request_status = 5;
        $loan_request->save();

        $p = UniversalMethods::formatPhoneForAT($loan_request->user->phone_number);
        $mesge = 'Hello ' . $loan_request->user->first_name . ', Your loan request of KSh. '.$loan_request->principal_amount.' has been rejected. Please contact us on info@linq.mobi  for any enquiry';

        SendSMSTrait::sendSMS($mesge, $p);

        return redirect()->route('loan_requests')->with("success", "Loan request has been rejected");

    }

    public function accept_request($request_id)
    {
        $loan_request = LoanRequest::where('id', $request_id)->first();

        $loan_request->loan_request_status = 4;
        $loan_request->save();

        $user = AppUser::where('id', $loan_request->user_id)->first();

        $user_id = $loan_request->user_id;
        $amount = $loan_request->principal_amount;
        $loan_request_id = $loan_request->id;
        $phone_to_pay = $user->id;

        if ($this->startsWith($phone_to_pay, "07")) {
            $pos = strpos($phone_to_pay, "07");
            if ($pos !== false) {
                $phone_to_pay = substr_replace($phone_to_pay, "2547", $pos, 2);
            }
        }

        $this->transactb2c($amount, $user_id, $phone_to_pay, $loan_request_id);

        return redirect()->route('loan_requests')->with("success", "Loan request accepted successfully");
    }

    public function transactb2c($amount, $user_id, $phone_to_pay, $loan_request_id)
    {

//        dd($amount);

//        $partya = "600136"; //B2C Shortcode/Paybill 661912(live)
        $partya = "661912"; //B2C Shortcode/Paybill 661912(live)
        $remarks = 'Issue a loan to User:' . $user_id;
        $url = 'https://api.safaricom.co.ke/mpesa/b2c/v1/paymentrequest';
        $token = $this->getToken();
        $usr = AppUser::where('app_users.id', $user_id)->first();

        $partyB = $this->getPartyB($usr->phone_number);
//        dd($partyB);


        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type:application/json', 'Authorization:Bearer ' . $token)); //setting custom header

        $securityCredential = $this->generateSecurityCredential('LINQAPI', 'Th1s1sLINQ#');
//        dd($securityCredential);
        $curl_post_data = array(
            //Fill in the request parameters with valid values
            'InitiatorName' => 'LINQAPI',
//            'SecurityCredential' => 'HTBeKeL7PGofUf4SCG/fnXBSsouY8G353JeuJevl6PGH0YbWelGB4YQYzyJhOQY/Htl35xhABzIHtlyp1Yl8lDqKVqGCefoGjpTt/+XTvsvLQZvixOCuvAS6mSgfJAR3zyLJzJ9nN7siEy4lVdHxkQXwg05uoA64sZyz/5oA94dotM5EFe3SR/u4xrMe/53Br0MoDPkyYX/oz9wl82vjCloip7aPt0Y6INPXdlWaR95CzOVuPvILOffzZxlYpIAt8wjaBdngMZ6SDI4XB6ck+ONLtZa9JR1f5yhZDKa+tL9J8D0XxT71gzpN+oG/KLUt+b3hxxpfxyxPullqGsxZOw',
            'SecurityCredential' => $securityCredential,
            'CommandID' => 'BusinessPayment',
            'Amount' => intval($amount),
            'PartyA' => $partya,
//            'PartyB' => '254708374149',//$phone_to_pay
            'PartyB' => $partyB,//$phone_to_pay
            'Remarks' => $remarks,
            'QueueTimeOutURL' => 'https://linq.mobi/api/for/b2ctimeout',
            'ResultURL' => 'https://linq.mobi/api/for/b2csuccess',
            'Occasion' => ''
        );

        $data_string = json_encode($curl_post_data);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

        $curl_response = curl_exec($curl);

//        dd($curl_response);

        $json = json_decode($curl_response, true);
//        dd($json);
        $conversation_id = $json['ConversationID'];
        $originatorconversation_id = $json['OriginatorConversationID'];

        $loan_request = LoanRequest::where('id', $loan_request_id)->first();
        $loan_request['conversation_id'] = $conversation_id;
        $loan_request['originator_conversation_id'] = $originatorconversation_id;
        $loan_request->save();

        print_r($curl_response);

//        echo $curl_response;

    }


    public function getToken()
    {

        //Variables specific to this application

//        $consumer_key = "bGaRkyjEjG8R8HUjMTgQSdJdrZOonxjl"; //Get these two from DARAJA Platform
        $consumer_key = "OkGWiolxUAB3B8NdYBZSw4jZsALZt6jM"; //Get these two from DARAJA Platform

//        $consumer_secret = "vEIPAkXhhEUnsfev";
        $consumer_secret = "lsNXp4FfT2Cf5s6X";

        $credentials = base64_encode($consumer_key . ":" . $consumer_secret);

        //START CURL

        $url = 'https://api.safaricom.co.ke/oauth/v1/generate?grant_type=client_credentials';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, $url);

        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Authorization: Basic ' . $credentials));

        curl_setopt($curl, CURLOPT_HEADER, false);//Make it not return headers...true retirns header

        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);//MAGIC..

        $curl_response = curl_exec($curl);

//        dd($curl_response);

        $access_token = json_decode($curl_response);

        return $access_token->access_token;

    }

    function startsWith($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public function generateSecurityCredential(string $username, string $password)
    {
        $file = fopen("/etc/ssl/mpesa/mpesapublickey.cer", 'r'); // This is to be downloaded from mpesa docs and pasted here with this name
        $public_key = fread($file, 8192);
        fclose($file);

        openssl_public_encrypt($password, $encrypted_string, $public_key, OPENSSL_PKCS1_PADDING);
        return (base64_encode($encrypted_string));
    }

    public function getPartyB($phone_number)
    {
        // Replace numbers starting with 07 or +254 with 254 due to what safaricom expects
        if (substr($phone_number, 0, strlen("07")) == "07") {
            $phone_number = "2547" . substr($phone_number, strlen("07"));
        } elseif (substr($phone_number, 0, strlen("+2547")) == "+2547") {
            $phone_number = "2547" . substr($phone_number, strlen("2547"));
        }
        return $phone_number;

    }

}
