<?php

namespace App\Http\Controllers;

use App\Policy;
use App\Terms;
use Illuminate\Http\Request;

class LandingController extends Controller
{
    //
    public  function view_tac(){
        $tac = Terms::where('id',1)->first();
//        dd($tac);
        $context=['tac'=>$tac];
        return view('landing_pages.t_and_cs',$context);
    }

    public function view_policy()
    {
        $policy = Policy::where('id',1)->first();
//        dd($tac);
        $context=['pol'=>$policy];
        return view('landing_pages.privacy',$context);
    }
}
