<?php

namespace App\Http\Controllers;

use App\AdminUserVerification;
use App\AppUser;
use App\Http\Traits\SendSMSTrait;
use App\Http\Traits\UniversalMethods;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminVerificationController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function unverified_users()
    {

        $unverified_users = AdminUserVerification::where('admin_verification_status',1)->get();

        $users = AppUser::where('status',1)->get();

        $context = [
            'app_users'=>$users
        ];

        return view('users.unverified',$context);

    }

    public function verify_user($user_id)
    {
        $unverified = AdminUserVerification::where('app_user_id',$user_id)->where('admin_verification_status',1)->first();

        if ($unverified == null){
            $unverified = new AdminUserVerification();
            $unverified->app_user_id = $user_id;
            $unverified->user_id = Auth::user()->id;
            $unverified->admin_verification_status = 2;
            $unverified->save();
        }else{

            $unverified->admin_verification_status = 2;
            $unverified->user_id = Auth::user()->id;
            $unverified->save();

        }

        $p = UniversalMethods::formatPhoneForAT($unverified->user->phone_number);
        $mesge = 'Hello ' . $unverified->user->first_name . ', your Linq Mobile account has been verified. You can now login to LinqMobile app and apply for a loan';
        SendSMSTrait::sendSMS($mesge, $p);



        return redirect()->route('users')->with("message","user verified successfully");
    }

    public function reject_user($user_id)
    {
        $unverified = AdminUserVerification::where('app_user_id',$user_id)->where('admin_verification_status',1)->first();

        if ($unverified == null){
            $unverified = new AdminUserVerification();
            $unverified->app_user_id = $user_id;
            $unverified->user_id = Auth::user()->id;
            $unverified->admin_verification_status = 3;
            $unverified->save();
        }else{

            $unverified->admin_verification_status = 3;
            $unverified->user_id = Auth::user()->id;
            $unverified->save();

        }

        $p = UniversalMethods::formatPhoneForAT($unverified->user->phone_number);
        $mesge = 'Hello ' . $unverified->user->first_name . ', your Linq Mobile account has been rejected. Please contact us in case of any enquiry';
        SendSMSTrait::sendSMS($mesge, $p);

        return redirect()->back()->with("message","user account has been rejected successfully");
    }

}
