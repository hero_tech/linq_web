<?php

namespace App\Http\Controllers;

use App\MpesaVerification;
use Illuminate\Http\Request;

class MpesaVerificationController extends Controller
{
    //

    public function mpesa_verifications()
    {

        $verification = MpesaVerification::all();

        $context = [
          'verifications'=>$verification
        ];

        return view('admins.mpesa_verifications',$context);

    }

}
