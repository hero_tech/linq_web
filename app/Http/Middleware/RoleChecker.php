<?php

namespace App\Http\Middleware;

use Closure;

class RoleChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
//        dd($request);
//        dd($request->user());
        if ($request->user()===null){
            return redirect()->back();
        }
        $action = $request->route()->getAction();
//        dd($action);
        $roles = isset($action['roles']) ? $action['roles'] : null;
//        dd($roles);
        if ($request->user()->hasAnyRole($roles)){
            return $next($request);
        }
        return redirect()->back();
    }
}
