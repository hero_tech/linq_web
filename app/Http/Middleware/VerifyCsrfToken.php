<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'https://d75a6dfd.ngrok.io/api/for/b2ctimeout',
        'https://d75a6dfd.ngrok.io/api/for/b2csuccess',
        'https://d75a6dfd.ngrok.io/api/for/loanrequest',
        'https://linq.mobi/api/for/loanrequest',
        'https://d75a6dfd.ngrok.io/api/callback',
        'https://23e87f38.ngrok.io/api/confirmation',
        'https://23e87f38.ngrok.io/api/validation',

        'https://linq.mobi/api/for/b2csuccess',
        'https://linq.mobi/api/for/b2ctimeout',

        "https://linq.mobi/api/verification/callback",

        'https://linq.mobi/api/callback',
        'https://linq.mobi/api/confirmation',
        'https://linq.mobi/api/validation',
        '127.0.0.1:8000/api/metropol/report/callback',
        'https://11181410.ngrok.io/api/metropol/report/callback',
        'https://linq.mobi/api/metropol/report/callback',
        'https://acc84c6c.ngrok.io/api/metropol/report/callback',
    ];
}
