<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetropolScoreRequest extends Model
{
    //

    protected $fillable =[
        'user_id','identity_number','identity_type','credit_score'
    ];

    public function getUserAttribute()
    {
        return AppUser::where('id',$this->user_id)->first();
    }
}
