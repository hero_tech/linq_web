<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanRequest extends Model
{
    //
    protected  $fillable =[
        'user_id',
        'repayment_cycle_interest_id',
        'interest_amount',
        'principal_amount',
        'loan_request_status',
        'conversation_id',
        'originator_conversation_id'
    ];

    public function getUserAttribute()
    {
        $user = AppUser::where('id',$this->user_id)->first();

        return $user;
    }

    public function getCycleAttribute()
    {
        $cycle = RepaymentCycleInterest::where('id',$this->repayment_cycle_interest_id)->first();

        return $cycle;
    }
}
