<?php

namespace App;

use App\Notifications\PassResetNotification;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements CanResetPassword
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new PassResetNotification($token));
    }

    public function roles()
    {
        return $this->belongsToMany('App\Role', 'user_role', 'user_id', 'role_id');
    }
    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        if ($this->roles()->where('name', $role)->first()) {
            return true;
        }
        return false;
    }

//    public function getRole() {
//        if ($this->hasRole('Super Admin')){
//            return 'Super Admin';
//        } elseif ($this->hasRole('Normal Admin')) {
//            return 'Normal Admin';
//        }
//    }

//    public function checkRole(){
//        if ($this->hasRole('Super Admin')){
//            return 'Super Admin';
//        }elseif ($this->hasRole('Normal Admin')){
//            return 'Normal Admin';
//        }
//    }


}
