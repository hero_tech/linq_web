<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserMetropolScore extends Model
{
    protected  $fillable =[
        'user_id','score'
    ];
}
