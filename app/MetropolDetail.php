<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MetropolDetail extends Model
{
    //
    protected $fillable =[
        'user_id','identity_number','identity_type','first_name','last_name','other_name','dob'
    ];
}
