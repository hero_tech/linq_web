<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminUserVerification extends Model
{
    //

    protected $fillable =[
        'app_user_id','user_id','admin_verification_status'
    ];

    public function getUserAttribute()
    {
        $user = AppUser::where('id',$this->app_user_id)->first();

        return $user;
    }
}
