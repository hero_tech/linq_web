<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UpdateLoanIncrement extends Model
{
    //
    protected $fillable=[
      'loan_id','increment_no'
    ];
}
