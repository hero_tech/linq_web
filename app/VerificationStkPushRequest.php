<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VerificationStkPushRequest extends Model
{

    //

    protected $fillable=[
        'user_id','amount','status','merchant_request_id','checkout_request_id'
    ];

}
