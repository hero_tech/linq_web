<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StkPushPaymentRequest extends Model
{
    //
    protected $fillable=[
        'loan_id','amount','status','merchant_request_id','checkout_request_id'
    ];
}