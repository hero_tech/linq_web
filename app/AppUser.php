<?php

namespace App;


use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class AppUser extends Authenticatable
{
    use HasApiTokens, Notifiable;

    const ACTIVE_LOAN = 1;
    const CLEARED_LOAN = 2;
    const DEFAULTED_LOAN = 3;
    const ACTIVE_USER = 1;
    const BLOCKED_USER = 2;


//    protected $table="app_users";
//
//    public function findForPassport($username) {
//        return $this->where('phone_number', $username)->first();
//    }

    protected $fillable = [

        'first_name', 'second_name', 'surname', 'region_id', 'phone_number', 'pin', 'occupation', 'id_number', 'date_of_birth', 'email', 'status'

    ];

    protected $hidden = [
        'pin', 'remember_token',
    ];

    public function loans()
    {
        return $this->hasMany('App\Loan', 'user_id');
    }

    public function active_loan()
    {
        return $this->loans()->where('loan_status', '=', $this::ACTIVE_LOAN);
    }

    public function defaulted_loan()
    {
        return $this->loans()->where('loan_status', '=', $this::DEFAULTED_LOAN);
    }
    public function cleared_loan()
    {
        return $this->loans()->where('loan_status', '=', $this::CLEARED_LOAN);
    }

    public function getAdminVerificationAttribute()
    {

        $verification = AdminUserVerification::where('app_user_id',$this->id)->first();

        return $verification;

    }

    public function getRegionAttribute()
    {

        $region = Region::where('id',$this->region_id)->first();

        return $region->name;

    }

    public function getGenderAttribute()
    {
        $details = MetropolDetail::where('user_id',$this->id)->first();

        return $details->gender;
    }
}
