<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoanRepaymentRequest extends Model
{
    //
    protected $fillable=[
        'loan_id','amount','status'
    ];
}
