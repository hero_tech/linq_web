<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Repayment extends Model
{
    //
    protected  $fillable=[
        'loan_id','user_id','amount'
    ];

    public function loan(){
        return $this->belongsTo('App\Loan', 'loan_id');
    }
}
