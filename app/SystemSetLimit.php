<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemSetLimit extends Model
{
    //
    protected  $fillable=[

        'user_id','amount','status','incremented_at'

    ];


}
