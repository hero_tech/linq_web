<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AdminSetLimit extends Model
{
    //
    protected $fillable=[
      'user_id','amount','status'
    ];
}
