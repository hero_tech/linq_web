<?php


namespace App\Classes;


use Carbon\Carbon;
use GuzzleHttp\Client;
use Psr\Log\LoggerInterface;

class MetropolDataSubmission
{

    /**

     * @var mixed

     */

    private $publicApiKey;

    /**

     * @var mixed

     */

    private $privateApiKey;



    /**

     * @var string

     */

    private $baseEndpoint = "https://api.metropol.co.ke";



    /**

     * Always clarify the port with MCRB before making any connection

     * @var mixed

     */

//    private $port = 22225;
    private $port = 5558;

    /**

     * @var Client

     */

    private $http;



    /**

     * The Metropol API version

     * @var string

     */

    private $version = 'v1';



    /**

     * @var LoggerInterface

     */

    private $logger = null;



    /**

     * Metropol constructor.

     * @param $publicApiKey

     * @param $privateApiKey

     */

    public function __construct($publicApiKey, $privateApiKey)

    {

        $this->publicApiKey = $publicApiKey;

        $this->privateApiKey = $privateApiKey;



        $this->http = new Client([

            'timeout'         => 120,

            'allow_redirects' => true,

            'http_errors'     => true, //let users handle errors

            'verify'          => false,

        ]);

    }



    /**
 * @param $version
 * @return MetropolDataSubmission
 */

    public function withVersion($version)

    {

        $this->version = $version;

        return $this;

    }



    /**

     * @return null|string

     */

    public function getVersion()

    {

        if (!is_null($this->version)) {

            return '/' . $this->version;

        }



        return null;

    }



    /**

     * @return string

     */

    public function getBaseUrl()

    {

        return $this->baseEndpoint . ":" . $this->port;

    }



    /**
 * @param $publicApiKey
 * @return MetropolDataSubmission
 */

    public function withPublicApiKey($publicApiKey)

    {

        $this->publicApiKey = $publicApiKey;

        return $this;

    }



    /**
 * @param $privateApiKey
 * @return MetropolDataSubmission
 */

    public function withPrivateApiKey($privateApiKey)

    {

        $this->privateApiKey = $privateApiKey;

        return $this;

    }



    /**
 * @param $baseEndpoint
 * @return MetropolDataSubmission
 */

    public function withBaseEndpoint($baseEndpoint)

    {

        $this->baseEndpoint = $baseEndpoint;

        return $this;

    }



    /**
 * @param $port
 * @return MetropolDataSubmission
 */

    public function withPort($port)

    {

        $this->port = $port;

        return $this;

    }



    /**
 * @param Client $http
 * @return MetropolDataSubmission
 */

    public function withHttp(Client $http)

    {

        $this->http = $http;

        return $this;

    }



    /**
 * @param LoggerInterface $logger
 * @return MetropolDataSubmission
 */

    public function withLogger(LoggerInterface $logger)

    {

        $this->logger = $logger;

        return $this;

    }



    /**

     * @param $payload

     * @return array

     */

    private function setHeaders($payload)

    {

        $now = Carbon::now('UTC');



        $apiTimestamp = $now->format('Y-m-d-H-i-s-u');

        $apiTimestamp = str_replace('-', '', $apiTimestamp);



        //calculate the rest api hash as required

        $apiHash = $this->calculateHash($payload, $apiTimestamp);





        $headers = [

            "X-METROPOL-REST-API-KEY" => $this->publicApiKey,

            "X-METROPOL-REST-API-HASH" =>  $apiHash,

            "X-METROPOL-REST-API-TIMESTAMP" => $apiTimestamp,

            "Content-Type" => "application/json"

        ];









        $this->log("Metropol API Headers:", $headers);



        return array_values($headers);

    }



    /**

     * @param $payload

     * @param $apiTimestamp

     * @return string

     */

    private function calculateHash($payload, $apiTimestamp)

    {

        $string = $this->privateApiKey . trim(json_encode($payload)) . $this->publicApiKey . $apiTimestamp;



        return hash('sha256', $string);

    }



    /**

     * @param $endpoint

     * @param $payload

     * @return array

     */

    public function httpPost($endpoint, $payload)

    {



        $now = Carbon::now('UTC');



        $apiTimestamp = $now->format('Y-m-d-H-i-s-u');

        $apiTimestamp = str_replace('-', '', $apiTimestamp);



        //calculate the rest api hash as required

        $apiHash = $this->calculateHash($payload, $apiTimestamp);





        $headers = [

            "X-METROPOL-REST-API-KEY" => $this->publicApiKey,

            "X-METROPOL-REST-API-HASH" =>  $apiHash,

            "X-METROPOL-REST-API-TIMESTAMP" => $apiTimestamp,

            "Content-Type" => "application/json"

        ];







        $url = $this->getBaseUrl() . $this->getVersion() . $endpoint;

//        dd($url);

        $this->log("Metropol API URL:" . $url);

        $this->log("Metropol API Payload:", $payload);





        $response = $this->http->request('POST', $url, [

            'json'    => $payload,

            'headers' => [

                "X-METROPOL-REST-API-KEY" => $this->publicApiKey,

                "X-METROPOL-REST-API-HASH" =>  $apiHash,

                "X-METROPOL-REST-API-TIMESTAMP" => $apiTimestamp,

                "Content-Type" => "application/json"

            ],

        ]);





        $contents = $response->getBody()->getContents();



        return json_decode($contents);

    }



    /**

     * @param $message

     * @param array $context

     */

    public function log($message, $context = [])

    {

        if ($this->logger) {

            $this->logger->log("info", $message, $context);

        }

    }



    /**

     * @param $id_number

     * @return array

     */

    public function identityVerification($id_number)

    {

        $endpoint = '/identity/verify';



        $payload = [

            "report_type"     => 1,

            "identity_number" => (string) $id_number,

            "identity_type"   => "001",

        ];



        return $this->httpPost($endpoint, $payload);

    }



    /**

     * @param $id_number

     * @param $loan_amount

     * @return array

     */

    public function deliquencyStatus($id_number, $loan_amount)

    {

        $endpoint = '/deliquency/status';



        $payload = [

            "report_type"     => 2,

            "identity_number" => (string) $id_number,

            "identity_type"   => "001",

            "loan_amount"     => $loan_amount,

        ];



        return $this->httpPost($endpoint, $payload);

    }



    /**

     * @param $id_number

     * @param $loan_amount

     * @return array

     */

    public function creditInfo($id_number, $loan_amount)

    {

        $endpoint = '/report/credit_info';



        $payload = [

            "report_type"     => 8,

            "identity_number" => (string) $id_number,

            "identity_type"   => "001",

            "loan_amount"     => $loan_amount,

            "report_reason"   => 1,

        ];



        return $this->httpPost($endpoint, $payload);

    }



    /**

     * @param $id_number

     * @return array

     */

    public function consumerScore($id_number)

    {



        $endpoint = '/score/consumer';



        $payload = [

            "report_type"     => 3,

            "identity_number" => (string) $id_number,

            "identity_type"   => "001",

        ];





        return $this->httpPost($endpoint, $payload);

    }

    public function submitJSON($loan_id)
    {

    }


}
