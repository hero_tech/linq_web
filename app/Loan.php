<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected  $fillable =[
        'user_id',
        'loan_request_id',
        'repayment_cycle_interest_id',
        'interest_amount',
        'principal_amount',
        'loan_status',
        'start_at',
        'due_date',
        'clearance_date'
    ];

    public function appUser(){
        $this->belongsTo('App\AppUser');
    }

    public function loanuser(){
        return $this->belongsTo('App\AppUser','user_id');
    }

    public function repayment(){
        return $this->hasMany('App\Repayment', 'loan_id');
    }

    public function getAppUserAttribute()
    {
        $app_user = AppUser::where('id',$this->user_id)->first();

        return $app_user;
    }

    public function getCycleAttribute()
    {
        $cycle = RepaymentCycleInterest::where('id',$this->repayment_cycle_interest_id)->first();

        return $cycle;
    }

    public function getBalanceAttribute()
    {
        $balance = 0;
        if (count(Repayment::where('loan_id',$this->id)->get()) != null){
            $repayment = Repayment::where('loan_id',$this->id)->orderBy('created_at', 'DESC')->first();
            $balance = $repayment->balance;
        }else{
            $balance = $this->principal_amount + $this->interest_amount;
        }
        return $balance;
    }

    public function getLatestRepaymentAttribute(){
        $repayment = Repayment::where('loan_id',$this->id)->orderBy('created_at', 'DESC')->first();

        return $repayment;
    }

    public function getIncrementAttribute()
    {
        return UpdateLoanIncrement::where('loan_id',$this->id)->first();
    }

    public function getLastIncrementAttribute()
    {
        return LoanIncrement::where('loan_id',$this->id)->first();
    }

    public function getFirstMonthPaymentAttribute()
    {
        $track = MonthlyLoanTrack::where('loan_id',$this->id)->where('month',1)->first();

        return $track;
    }

    public function getSecondMonthPaymentAttribute()
    {
        $track = MonthlyLoanTrack::where('loan_id',$this->id)->where('month',2)->first();

        return $track;
    }

    public function getThirdMonthPaymentAttribute()
    {
        $track = MonthlyLoanTrack::where('loan_id',$this->id)->where('month',3)->first();

        return $track;
    }

    public function getActiveMonthlyInstallmentAttribute()
    {
        $amount = 0.0;
        $tracks = MonthlyLoanTrack::where('loan_id',$this->id)->where('status',1)->orWhere('status',3)->get();
        foreach ($tracks as $track){
            $amount += $track->amount;
        }
        return $amount;
    }

    public function getActiveMonthlyDueDateAttribute()
    {
        $tracks = MonthlyLoanTrack::where('loan_id',$this->id)->where('status',1)->orWhere('status',3)->orderby('due_date','desc')->first();
        return $tracks;
    }
}
