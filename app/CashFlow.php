<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashFlow extends Model
{
    //
    protected  $fillable=[
        'identifier','amount','transaction_cost','paybill_balance'
    ];
}
